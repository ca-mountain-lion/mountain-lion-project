/*
  Pin Definitions:

  Motor:
  - const byte MOTOR_IN1 = D7;

  Speaker:
  - const int I2S_DOUT = 22;
  - const int I2S_LRC = 25;
  - const int I2S_BCLK = 26;

  SD Card:
  - const int SD_CS = 4;
  - const int SPI_SCK = 18;
  - const int SPI_MISO = 19;
  - const int SPI_MOSI = 23;
 */

#include "SD_Speaker_Lib.h"
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncTCP.h>

// REPLACE WITH YOUR NETWORK CREDENTIALS
const char* ssid = "Mountain-Lion-Web";
const char* password = "calionpass";

const char* web_ui_username = "mountainlion";
const char* web_ui_password = "calion";

const char* PARAM_INPUT_1 = "state";

const int output = 2;

// HTML web page
const char index_html[] PROGMEM = R"rawliteral(
  <!DOCTYPE html>
  <html>
  <head>
      <title>California Mountain Lion Project</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style>
      body {font-family: Verdana; text-align: center; margin:0px auto; padding-top: 30px;}

      /***** Start of Push Button Section *****/
      .button {
      padding: 10px 20px;
      font-size: 16px;
      text-align: center;
      outline: none;
      color: #fff;
      background-color: #228B22;
      border: none;
      border-radius: 20px;
      box-shadow: 0 0px #999;
      cursor: pointer;
      margin-left:20px;
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
      }
      .button:hover {
      background-color: #355E3B
      }

      .button:active {
      background-color: #355E3B;
      box-shadow: 0 4px #666;
      transform: translateY(2px);
      }
      /***** End of Push Button Section *****/

      /***** Start of Tab Section *****/
      /* Style the tab */
      .tab {
      overflow: hidden;
      border: 1px solid #ccc;
      background-color: #f1f1f1;
      }

      /* Style the buttons inside the tab */
      .tab button {
      background-color: inherit;
      float: center;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      transition: 0.3s;
      font-size: 17px;
      }

      /* Change background color of buttons on hover */
      .tab button:hover {
      background-color: #ddd;
      }

      /* Create an active/current tablink class */
      .tab button.active {
      background-color: #ccc;
      }

      /* Style the tab content */
      .tabcontent {
      display: none;
      padding: 6px 12px;
      border: 1px solid #ccc;
      border-top: none;
      }

      /* Style the close button */
      .topright {
      float: right;
      cursor: pointer;
      font-size: 28px;
      }

      .topright:hover {color: red;}
      /***** End of Tab Section *****/

      /***** Start of Select Drop-Down Section *****/
      /*the container must be positioned relative:*/
      .custom-select {
      display: inline-block;
      position: relative;
      font-family: Arial;
      }

      .custom-select select {
      display: none; /*hide original SELECT element:*/
      }

      .select-selected {
      background-color: DodgerBlue;
      }

      /*style the arrow inside the select element:*/
      .select-selected:after {
      position: absolute;
      content: "";
      top: 14px;
      right: 10px;
      width: 0;
      height: 0;
      border: 6px solid transparent;
      border-color: #fff transparent transparent transparent;
      }

      /*point the arrow upwards when the select box is open (active):*/
      .select-selected.select-arrow-active:after {
      border-color: transparent transparent #fff transparent;
      top: 7px;
      }

      /*style the items (options), including the selected item:*/
      .select-items div,.select-selected {
      color: #ffffff;
      padding: 8px 16px;
      border: 1px solid transparent;
      border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
      cursor: pointer;
      user-select: none;
      }

      /*style items (options):*/
      .select-items {
      position: absolute;
      background-color: DodgerBlue;
      top: 100%;
      left: 0;
      right: 0;
      z-index: 99;
      }

      /*hide the items when the select box is closed:*/
      .select-hide {
      display: none;
      }

      .select-items div:hover, .same-as-selected {
      background-color: rgba(0, 0, 0, 0.1);
      }
      /***** End of Select Drop-Down Section *****/

      /***** Start of Slider Section *****/
      .slider {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      border-radius: 34px;
      }
      .slider:before {
      position: absolute;
      content: "";
      height: 52px;
      width: 52px;
      left: 8px;
      bottom: 8px;
      background-color: #fff;
      -webkit-transition: .4s;
      transition: .4s;
      border-radius: 68px
      }

      .slider:hover {
        opacity: 1;
      }
      input:checked+.slider {
      background-color: #2196F3
      }
      input:checked+.slider:before {
      -webkit-transform: translateX(52px);
      -ms-transform: translateX(52px);
      transform: translateX(52px)
      }

      .slider2 {
      -webkit-appearance: none;
      margin: 14px;
      width: 300px;
      height: 20px;
      background: #ccc;
      outline: none;
      -webkit-transition: .2s;
      transition: opacity .2s;
      border-radius: 50px;
      }

      .slider2::-webkit-slider-thumb {
      -webkit-appearance: none;
      appearance: none;
      width: 35px;
      height: 35px;
      background: #385d9c;
      cursor: pointer;
      border-radius: 50%;
      }
      .slider2::-moz-range-thumb {
      width: 30px;
      height: 30px;
      background: #2f4468;
      cursor: pointer;
      border-radius: 50%;
      }
      /***** End of Slider Section *****/

      /***** Start of Switch Section *****/
      .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
      }
      .switch input {
        display: none;
      }

      .slider_for_mode {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
      border-radius: 20px;
      }

      .slider_for_mode:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
      border-radius: 20px;
      }
      input:checked + .slider_for_mode {
      background-color: #2196F3;
      }

      input:focus + .slider_for_mode {
      box-shadow: 0 0 1px #2196F3;
      }

      input:checked + .slider_for_mode:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
      }
      /***** End of Switch Section *****/

      /***** Start of Input Timer Section *****/
      form {
      margin: 25px;
      display: inline-block;
      }

      .input-field {
      position: relative;
      width: 250px;
      height: 10px;
      line-height: 50px;
      }

      label {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      color: #d3d3d3;
      transition: 0.2s all;
      cursor: text;
      }

      input {
      width: 100%;
      border: 0;
      outline: 0;
      padding: 0.5rem 0;
      border-bottom: 2px solid #d3d3d3;
      box-shadow: none;
      color: #111;
      }

      input:invalid {
        outline: 0;
      }

      input:focus,
      input:valid {
      border-color: #333533;
      }
      input:focus~label,
      input:valid~label {
      font-size: 14px;
      top: -24px;
      color: #060a07;
      }
      /***** End of Input Timer Section *****/

  </style>
  </head>
  <body>

  <h2>Predator Deterrent Device Control Web UI</h2>
  <p>Select a tab to control the device:</p>

  <div class="tab">
    <button class="tablinks" onclick="open_tab(event, 'Alarm Enable')" id="defaultOpen">Alarm Options</button>
    <button class="tablinks" onclick="open_tab(event, 'LED Strip Control')">LED Strip Control</button>
    <button class="tablinks" onclick="open_tab(event, 'Motor Control')">Motor Control</button>
    <button class="tablinks" onclick="open_tab(event, 'Sound Control')">Sound Control</button>
    <button class="tablinks" onclick="open_tab(event, 'About Page')">About</button>
  </div>

  <div id="Alarm Enable" class="tabcontent">
    <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
    <h3>Alarm System Control Menu</h3>
    <p>This section controls the device's alarm system which consists of the LED strip, the motor, and the speaker.</p>

    <h4>Automatic Trigger Mode</h4>
    <label class="switch">
      <input id="automatic-trigger-switch" type="checkbox">
      <div class="slider_for_mode"></div>
    </label>

    <h4>Timer Trigger Mode</h4>
    <label class="switch">
      <input id="timer-trigger-switch" type="checkbox">
      <div class="slider_for_mode"></div>
    </label>

    <h4>Manual Control Mode</h4>
    <label class="switch">
      <input id="manual-trigger-switch" type="checkbox">
      <div class="slider_for_mode"></div>
    </label>

    <h4>Device Trigger Mode Status: </h4>
    <p id="return mode status"></p>

    <h4>Set Timer Interval (in minutes):</h4>
    <form action="">
      <div class="input-field">
        <input type="number" id="timer_interval" min="1" max="10080"/>
        <label for="timer_value">Timer Interval:</label>
        <button class="button" style="display: inline-block; height: 30px; width: 150px; margin: 0; padding: 0; vertical-align:top; font-size: 16px; border-radius:20px;" onclick="set_input_timer_interval('initialize_timer');">Initialize Timer</button>
      </div>
    </form>
  </div>

  <div id="LED Strip Control" class="tabcontent">
    <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
    <h3>LED Strip Control Menu</h3>
    <p>This menu controls the device's LED strip. Select the desired LED pattern and hit "LED Enable" button.</p>
    <h3>LED Strip Pattern Menu</h3>
      <select class="led-strip" name="led-strip" style="text-align: center;">
          <option disabled hidden selected value>Choose LED Pattern</option>
          <option value="Red">Red</option>
          <option value="Orange">Orange</option>
          <option value="Yellow">Yellow</option>
          <option value="Green">Green</option>
          <option value="Blue">Blue</option>
          <option value="Purple">Purple</option>
          <option value="White">White</option>
          <option value="Theater Chase">Theater Chase</option>
          <option value="Rainbow Pattern">Rainbow Pattern</option>
          <option value="Random Pattern">Random Pattern</option>
          <option value="Flash Red">Flash Red</option>
          <option value="Flash Orange">Flash Orange</option>
          <option value="Flash Yellow">Flash Yellow</option>
          <option value="Flash Green">Flash Green</option>
          <option value="Flash Blue">Flash Blue</option>
          <option value="Flash Purple">Flash Purple</option>
          <option value="Flash White">Flash White</option>
      </select>
      <h4>Current Selected LED Pattern: </h4>
      <p id="return led pattern"></p>
      <button class="button" onmousedown="led_strip_on_handler('led_on');" onmouseup="led_strip_off_handler('led_random_off');" ontouchend="led_strip_off_handler('led_random_off');">LED Strip On</button>
      <button class="button" style="background-color: #971b1b;" onmousedown="led_strip_off_handler('led_off');">LED Strip Off</button>
  </div>

  <div id="Motor Control" class="tabcontent">
    <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
    <h3>Motor Control Menu</h3>
    <p>This menu controls the device's motor. Press the buttons below to run the motor in a clockwise or counter-clockwise direction.</p>
      <button class="button" onmousedown="toggle_push_button('motor_clockwise_on');" ontouchstart="toggle_push_button('motor_clockwise_on');" onmouseup="toggle_push_button('motor_clockwise_on');" ontouchend="toggle_push_button('motor_clockwise_on');">Run Clockwise</button>
      <button class="button" onmousedown="toggle_push_button('motor_counter_clockwise_on');" ontouchstart="toggle_push_button('motor_counter_clockwise_on');" onmouseup="toggle_push_button('motor_counter_clockwise_on');" ontouchend="toggle_push_button('motor_counter_clockwise_on');">Run Counter-Clockwise</button>
      <button class="button" style="background-color: #971b1b;" onmousedown="toggle_push_button('motor_off');" ontouchstart="toggle_push_button('motor_off');" onmouseup="toggle_push_button('motor_off');" ontouchend="toggle_push_button('motor_off');">Turn Motor Off</button>
  </div>

  <div id="Sound Control" class="tabcontent">
      <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
      <h3>Sound Control Menu</h3>
      <p>This menu contains the device's available sound files. Press the drop-down menu to view the list of sounds files for each category.</p>
      <p>Then, press the "Play Sound" button to play the selected option.</p>

      <select class="speaker-device1" name="speaker-device1" style="text-align: center;">
        <optgroup label="Deer Sounds"></optgroup>
          <option disabled hidden selected>Deer Sounds</option>
          <option value="Deer 1">Deer 1</option>
          <option value="Deer 2">Deer 2</option>
          <option value="Deer 3">Deer 3</option>
          <option value="Deer 4">Deer 4</option>
          <option value="Deer 5">Deer 5</option>
          <option value="Deer 6">Deer 6</option>
          <option value="Deer 7">Deer 7</option>
          <option value="Deer 8">Deer 8</option>
          <option value="Deer 9">Deer 9</option>
          <option value="Deer 10">Deer 10</option>
        </optgroup>

        <optgroup label="Rabbit Sounds"></optgroup>
          <option value="Rabbit 1">Rabbit 1</option>
          <option value="Rabbit 2">Rabbit 2</option>
          <option value="Rabbit 3">Rabbit 3</option>
          <option value="Rabbit 4">Rabbit 4</option>
          <option value="Rabbit 5">Rabbit 5</option>
          <option value="Rabbit 6">Rabbit 6</option>
          <option value="Rabbit 7">Rabbit 7</option>
          <option value="Rabbit 8">Rabbit 8</option>
          <option value="Rabbit 9">Rabbit 9</option>
          <option value="Rabbit 10">Rabbit 10</option>
        </optgroup>

        <optgroup label="Human Voice Sounds"></optgroup>
          <option value="Human Voice 1">Human Voice 1</option>
          <option value="Human Voice 2">Human Voice 2</option>
          <option value="Human Voice 3">Human Voice 3</option>
          <option value="Human Voice 4">Human Voice 4</option>
          <option value="Human Voice 5">Human Voice 5</option>
          <option value="Human Voice 6">Human Voice 6</option>
          <option value="Human Voice 7">Human Voice 7</option>
          <option value="Human Voice 8">Human Voice 8</option>
          <option value="Human Voice 9">Human Voice 9</option>
          <option value="Human Voice 10">Human Voice 10</option>
        </optgroup>

        <optgroup label="Miscellaneous Alarm Sounds"></optgroup>
          <option value="Alarm 1">Alarm 1</option>
          <option value="Alarm 2">Alarm 2</option>
          <option value="Alarm 3">Alarm 3</option>
          <option value="Alarm 4">Alarm 4</option>
          <option value="Alarm 5">Alarm 5</option>
          <option value="Alarm 6">Alarm 6</option>
          <option value="Alarm 7">Alarm 7</option>
          <option value="Alarm 8">Alarm 8</option>
          <option value="Alarm 9">Alarm 9</option>
          <option value="Alarm 10">Alarm 10</option>
        </optgroup>
      </select>

      <h4>Current Selected Sound: </h4>
      <p id="return selected sound"></p>
      <button class="button" onmousedown="speaker_sound_on_handler('speaker_sound_on');">Play Sound</button>
      <button class="button" style="background-color: #971b1b;" onmousedown="speaker_sound_off_handler('speaker_sound_off');">Stop Sound</button>

      <h4>Play A Random Sound</h4>
      <button class="button" onmousedown="speaker_random_sound_on_handler('speaker_random_sound_on');">Play Random Sound</button>
      <button class="button" style="background-color: #971b1b;" onmousedown="speaker_sound_off_handler('speaker_sound_off');">Stop Random Sound</button>

      <h4>Volume Level: </h4>
      <p><span id="volume_level_value">Set Volume Level: </span> %</p>
      <p><input type="range" onchange="update_volume_level(this)" id="volume_level_slider" min="1" max="100" value="%TIMERVALUE%" step="1" value="50" class="slider2"></p>
    </div>

  <div id="About Page" class="tabcontent">
      <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
      <h3>Predator Deterrent Device Description</h3>
      <p>This section contains the description for the Predator Deterrent Device.</p>

      <h4>Timer</h4>
      <p>The timer interval can be set between 1 minute to 10,080 minutes (a maximum of a week). The default value is set to 10 minutes.</p>
  </div>

  <script>
  /***** Start of Drop-Down Menu Section *****/

  // LED Strip Event Listener
  let selected_led_element = document.querySelector('.led-strip');
  let led_result = document.getElementById('return led pattern');

  selected_led_element.addEventListener('change', (event) => {
    led_result.textContent = `${event.target.value}`;
  });

  // Sound Event Listener
  const selected_sound_element = document.querySelector(".speaker-device1");
  const sound_result = document.getElementById('return selected sound');

  selected_sound_element.addEventListener('change', (event) => {
    sound_result.textContent = `${event.target.value}`;
  });

  // Get the element with id="defaultOpen" and click on it
  document.getElementById("defaultOpen").click();

  /***** End of Drop-Down Menu Section *****/

  /***** Start of Button Section *****/
  function led_strip_on_handler(input_from_pushbutton)
  {
      var http_request_data = new XMLHttpRequest();
      http_request_data.open("GET", "/" + input_from_pushbutton + "_" + led_result.textContent, true);
      http_request_data.send();
  }

  function led_strip_random_off_handler(input_from_pushbutton)
  {
      var http_request_data = new XMLHttpRequest();
      http_request_data.open("GET", "/" + input_from_pushbutton + "_" + led_result.textContent, true);
      http_request_data.send();
  }

  function led_strip_off_handler(input_from_pushbutton)
  {
      var http_request_data = new XMLHttpRequest();
      http_request_data.open("GET", "/" + input_from_pushbutton, true);
      http_request_data.send();
  }

  function speaker_sound_on_handler(input_from_pushbutton)
  {
    var http_request_data = new XMLHttpRequest();
    http_request_data.open("GET", "/" + input_from_pushbutton + "_" + sound_result.textContent, true);
    http_request_data.send();
  }

  function speaker_random_sound_on_handler(input_from_pushbutton)
  {
    var http_request_data = new XMLHttpRequest();
    http_request_data.open("GET", "/" + input_from_pushbutton, true);
    http_request_data.send();
  }

  function speaker_sound_off_handler(input_from_pushbutton)
  {
    var http_request_data = new XMLHttpRequest();
    http_request_data.open("GET", "/" + input_from_pushbutton, true);
    http_request_data.send();
  }

  function set_input_timer_interval(input_from_timer_button)
  {
    var http_request_data = new XMLHttpRequest();
    http_request_data.open("GET", "/" + input_from_timer_button, true);
    http_request_data.send();
  }
  /***** End of Button Section *****/

  /***** Start of Slider Section *****/
  function toggleCheckbox(element) {
    var sliderValue = document.getElementById("volume_level_slider").value;
    var xhr = new XMLHttpRequest();
    if(element.checked){ xhr.open("GET", "/update?state=1", true); xhr.send();
      var count = sliderValue, timer = setInterval(function() {
        count--; document.getElementById("volume_level_value").innerHTML = count;
        if(count == 0){ clearInterval(timer); document.getElementById("volume_level_value").innerHTML = document.getElementById("volume_level_slider").value; }
      }, 1000);
      sliderValue = sliderValue*1000;
      setTimeout(function(){ xhr.open("GET", "/update?state=0", true);
      document.getElementById(element.id).checked = false; xhr.send(); }, sliderValue);
    }
  }
  function update_volume_level(element) {
    var sliderValue = document.getElementById("volume_level_slider").value;
    document.getElementById("volume_level_value").innerHTML = sliderValue;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/slider?value="+sliderValue, true);
    xhr.send();
  }
  /***** End of Slider Section *****/

  /***** Start of Functions for Tab Section *****/
  function open_tab(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
  /***** End of Functions for Tab Section *****/

  /***** Start of Slider Switch Section *****/
  let automatic_mode_status = false;
  let manual_mode_status = false;
  let timer_mode_status = false;
  let device_mode_status = document.getElementById('return mode status');
  device_mode_status.textContent = `No Trigger Mode Set.`;

  function read_switch_mode(event)
  {
    var switch_element = event.currentTarget

    if (switch_element.id == "automatic-trigger-switch")
    {
      if (switch_element.checked)
      {
        automatic_mode_status = true;
        console.log("Automatic Mode Status:", automatic_mode_status);
        device_mode_status.textContent = `Automatic Trigger Status: ${automatic_mode_status}`;
      }
      else if (!switch_element.checked && (manual_mode_status == false) && (timer_mode_status == false))
      {
        automatic_mode_status = false;
        device_mode_status.textContent = `No Trigger Mode Set.`;
      }
      else
      {
        automatic_mode_status = false;
        console.log("Automatic Mode Status:", automatic_mode_status);
        device_mode_status.textContent = `${automatic_mode_status}`;
        device_mode_status.textContent = `Automatic Trigger Status: ${automatic_mode_status}`;
      }
    }
    else if (switch_element.id == "manual-trigger-switch")
    {
      if (switch_element.checked)
      {
        manual_mode_status = true;
        console.log("Manual Mode Status:", manual_mode_status);
        device_mode_status.textContent = `Manual Trigger Status: ${manual_mode_status}`;
      }
      else if (!switch_element.checked && (automatic_mode_status == false) && (timer_mode_status == false))
      {
        manual_mode_status = false;
        device_mode_status.textContent = `No Trigger Mode Set.`;
      }
      else
      {
        manual_mode_status = false;
        console.log("Manual Mode Status:", manual_mode_status);
        device_mode_status.textContent = `Manual Trigger Status: ${manual_mode_status}`;
      }
    }
    else if (switch_element.id == "timer-trigger-switch")
    {
      if (switch_element.checked)
      {
        timer_mode_status = true;
        console.log("Timer Mode Status:", timer_mode_status);
        device_mode_status.textContent = `Timer Trigger Status: ${timer_mode_status}`;
      }
      else if (!switch_element.checked && (automatic_mode_status == false) && (manual_mode_status == false))
      {
        timer_mode_status = false;
        device_mode_status.textContent = `No Trigger Mode Set.`;
      }
      else
      {
        timer_mode_status = false;
        console.log("Timer Mode Status:", manual_mode_status);
        device_mode_status.textContent = `Timer Trigger Status: ${timer_mode_status}`;
      }
    }

    if ((automatic_mode_status && manual_mode_status) || (automatic_mode_status && timer_mode_status) || (manual_mode_status && timer_mode_status))
    {
      automatic_mode_status = false;
      manual_mode_status = false;
      timer_mode_status = false;
      document.getElementById("automatic-trigger-switch").checked = false;
      document.getElementById("timer-trigger-switch").checked = false;
      document.getElementById("manual-trigger-switch").checked = false;
      console.log("Automatic Mode Status:", automatic_mode_status);
      console.log("Timer Mode Status:", manual_mode_status);
      console.log("Manual Mode Status:", manual_mode_status);
      device_mode_status.textContent = `Error. Cannot set two trigger modes at the same time.`;
    }
  }
  document.getElementById("automatic-trigger-switch").addEventListener("change", read_switch_mode);
  document.getElementById("timer-trigger-switch").addEventListener("change", read_switch_mode);
  document.getElementById("manual-trigger-switch").addEventListener("change", read_switch_mode);
  /***** End of Slider Switch Section *****/

  const inputs = document.querySelectorAll('input');

  inputs.forEach(el => {
    el.addEventListener('blur', e => {
      if(e.target.value) {
        e.target.classList.add('dirty');
      } else {
        e.target.classList.remove('dirty');
      }
    })
  })
  </script>
  </body>
  </html>)rawliteral";

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

AsyncWebServer server(80);

void handle_led_strip_web_server_requests()
{
  // Send the web page to the client
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html);
  });

  // Receive an HTTP GET "OFF" Request for the LED Strips
  server.on("/led_off", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_off_flag = 1;
    led_strip_off_theater_chase_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Red", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_on_red();
    led_strip_on_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Red", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_on_red();
    led_strip_on_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Orange", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_on_orange();
    led_strip_on_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Yellow", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_on_yellow();
    led_strip_on_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Green", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_on_green();
    led_strip_on_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Blue", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_on_blue();
    led_strip_on_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Purple", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_on_purple();
    led_strip_on_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_White", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_on_white();
    led_strip_on_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Theater Chase", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_on_theater_chase_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Rainbow Pattern", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_on_rainbow_pattern();
    led_strip_on_flag = 1;
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Random Pattern", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_random_on_flag = 1;
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_random_off_Random Pattern", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_random_on_flag = 0;
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Flash Red", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_on_flash_flag = 1;
    led_flash_pattern_index = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Flash Orange", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_on_flash_flag = 1;
    led_flash_pattern_index = 2;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Flash Yellow", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_on_flash_flag = 1;
    led_flash_pattern_index = 3;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Flash Green", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_on_flash_flag = 1;
    led_flash_pattern_index = 4;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Flash Blue", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_on_flash_flag = 1;
    led_flash_pattern_index = 5;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Flash Purple", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_on_flash_flag = 1;
    led_flash_pattern_index = 6;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "ON" Request for the LED Strips
  server.on("/led_on_Flash White", HTTP_GET, [] (AsyncWebServerRequest *request) {
    led_strip_on_flash_flag = 1;
    led_flash_pattern_index = 7;
    request->send(200, "text/plain", "ok");
  });
}

void handle_speaker_web_server_requests()
{
  // Send the web page to the client
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html);
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_random_sound_on", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_random_playing = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Spoeaker
  server.on("/speaker_sound_off", HTTP_GET, [] (AsyncWebServerRequest *request) {
    turn_speaker_off_flag = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 1", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 0;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 2", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 1;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 3", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 2;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 4", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 3;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 5", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 4;
    request->send(200, "text/plain", "ok");
  });


  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 6", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 5;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 7", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 6;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 8", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 7;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 9", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 8;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Deer 10", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 9;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 1", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 10;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 2", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 11;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 3", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 12;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 4", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 13;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 5", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 14;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 6", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 15;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 7", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 16;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 8", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 17;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 9", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 18;
    request->send(200, "text/plain", "ok");
  });

  // Receive an HTTP GET "OFF" Request for the Speaker
  server.on("/speaker_sound_on_Rabbit 10", HTTP_GET, [] (AsyncWebServerRequest *request) {
    is_speaker_playing = 1;
    soundIndex = 19;
    request->send(200, "text/plain", "ok");
  });

}

void setup()
{
  Serial.begin(115200);
  motor_setup();
  SD_Speaker_setup();
  LED_Strip_Setup();
  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.println();
  Serial.print("ESP IP Address: http://");
  Serial.println(IP);

  pinMode(output, OUTPUT);
  digitalWrite(output, LOW);

  handle_led_strip_web_server_requests();
  handle_speaker_web_server_requests();

  server.onNotFound(notFound);
  server.begin();
}

void loop()
{
  check_led_strip_active_status();
  check_led_strip_active_theater_chase();
  turn_on_random_pattern();
  led_flash_handler(led_flash_pattern_index);
  play_specific_sound_loop(soundIndex);
  play_random_sound_loop();
  turn_speaker_off();
}
