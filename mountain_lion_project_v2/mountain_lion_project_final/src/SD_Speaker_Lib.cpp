#include "SD_Speaker_Lib.h"

AudioGeneratorMP3 *mp3;
AudioFileSourceSD *file;
AudioOutputI2S *out;
File SD_Card_Directory;

volatile bool is_speaker_playing = 0;
volatile bool is_speaker_random_playing = 0;
volatile bool turn_speaker_off_flag = 0;
int soundIndex;
int numSoundFiles; // Variable to keep track of the number of sound files in a directory
int numAllSoundFiles; // Variable to keep track of the number of all sound files (should be 20)
int dirStringLen;
String *soundList;
String *newSoundList;
String directoryName;
String deerDir = "/Deer_Sounds";
String rabbitDir = "/Rabbit_Sounds";
String deerRabbitDir = "/DeerRabbit_Sounds";
String randomSoundFile;

void SD_Speaker_setup()
{
  out = new AudioOutputI2S();
  mp3 = new AudioGeneratorMP3();

  Serial.print("Initializing SD card...");
  SD.begin(SD_CS);
  if (!SD.begin(SD_CS))
  {
    Serial.println("SD Card Initialization Failed!");
    return;
  }
  Serial.println("SD Card Initialization Done!");
  delay(100);
}

float set_volume(float volume_level)
{
  Serial.print("The volume level is: ");
  Serial.println(volume_level);

  return volume_level;
}

void play_specific_sound_loop(int soundIndex)
{
  if (is_speaker_playing == 1)
  {
    if(mp3->isRunning())
    {
      mp3->stop();
    }

    float volume_level = set_volume(0.30);

    // Deer Sounds
    if (soundIndex == 0) file = new AudioFileSourceSD("/Deer_Sounds/1_Deer.mp3");
    if (soundIndex == 1) file = new AudioFileSourceSD("/Deer_Sounds/2_Deer.mp3");
    if (soundIndex == 2) file = new AudioFileSourceSD("/Deer_Sounds/3_Deer.mp3");
    if (soundIndex == 3) file = new AudioFileSourceSD("/Deer_Sounds/4_Deer.mp3");
    if (soundIndex == 4) file = new AudioFileSourceSD("/Deer_Sounds/5_Deer.mp3");
    if (soundIndex == 5) file = new AudioFileSourceSD("/Deer_Sounds/6_Deer.mp3");
    if (soundIndex == 6) file = new AudioFileSourceSD("/Deer_Sounds/7_Deer.mp3");
    if (soundIndex == 7) file = new AudioFileSourceSD("/Deer_Sounds/8_Deer.mp3");
    if (soundIndex == 8) file = new AudioFileSourceSD("/Deer_Sounds/9_Deer.mp3");
    if (soundIndex == 9) file = new AudioFileSourceSD("/Deer_Sounds/10_Deer.mp3");

    // Rabbit Sounds
    if (soundIndex == 10) file = new AudioFileSourceSD("/Rabbit_Sounds/1_Rabbit.mp3");
    if (soundIndex == 11) file = new AudioFileSourceSD("/Rabbit_Sounds/2_Rabbit.mp3");
    if (soundIndex == 12) file = new AudioFileSourceSD("/Rabbit_Sounds/3_Rabbit.mp3");
    if (soundIndex == 13) file = new AudioFileSourceSD("/Rabbit_Sounds/4_Rabbit.mp3");
    if (soundIndex == 14) file = new AudioFileSourceSD("/Rabbit_Sounds/5_Rabbit.mp3");
    if (soundIndex == 15) file = new AudioFileSourceSD("/Rabbit_Sounds/6_Rabbit.mp3");
    if (soundIndex == 16) file = new AudioFileSourceSD("/Rabbit_Sounds/7_Rabbit.mp3");
    if (soundIndex == 17) file = new AudioFileSourceSD("/Rabbit_Sounds/8_Rabbit.mp3");
    if (soundIndex == 18) file = new AudioFileSourceSD("/Rabbit_Sounds/9_Rabbit.mp3");
    if (soundIndex == 19) file = new AudioFileSourceSD("/Rabbit_Sounds/10_Rabbit.mp3");

    out -> SetGain(volume_level);
    mp3 -> begin(file, out);
    is_speaker_playing = 0;
  }
  if(mp3->isRunning())
  {
    if (!mp3->loop())
    {
      mp3->stop();
      is_speaker_playing = 0;
      Serial.println("Sound has stopped playing!");
    }
  }
}

void play_random_sound_loop()
{
  if (is_speaker_random_playing == 1)
  {
    if(mp3->isRunning())
    {
      mp3->stop();
    }

    float volume_level = set_volume(0.30);
    file = new AudioFileSourceSD(return_random_sound_file(deerRabbitDir).c_str());
    out -> SetGain(volume_level);
    mp3 -> begin(file, out);
    is_speaker_random_playing = 0;
  }
  if(mp3->isRunning())
  {
    if (!mp3->loop())
    {
      mp3->stop();
      is_speaker_random_playing = 0;
      Serial.println("Sound has stopped playing!");
    }
  }
}

void turn_speaker_off()
{
  if (turn_speaker_off_flag == 1)
  {
    if(mp3->isRunning())
    {
      mp3->stop();
    }
    turn_speaker_off_flag = 0;
    Serial.println("Sound has stopped playing!");
  }
}

int get_string_length(String directoryName)
{
  dirStringLen = directoryName.length() + 2;

  return dirStringLen;
}

int return_num_sound_files(File folderName)
{
  numSoundFiles = 0;

  while (true)
  {
    File entry =  folderName.openNextFile();
    if (!entry) {
      folderName.rewindDirectory();
      break;
    } else {
      numSoundFiles++;
    }
    entry.close();
  }
  return numSoundFiles;
}

String* list_sound_files(File folderName, String directoryName)
{
    numSoundFiles = return_num_sound_files(folderName);
    Serial.print("Number of Sound Files in Directory: ");
    Serial.println(numSoundFiles);
    if (directoryName == deerRabbitDir)
    {
      numAllSoundFiles = numSoundFiles;
    }

    soundList = new String[numSoundFiles];
    newSoundList = new String[numSoundFiles];

    for (int i = 0; i < numSoundFiles; i++)
    {
      File entry = folderName.openNextFile();
      soundList[i] = entry.name();
      entry.close();
      Serial.println(soundList[i]);
    }

    if (directoryName == deerDir || directoryName == rabbitDir)
    {
      for (int i = 0; i < numSoundFiles; i++)
      {
        if (i == numSoundFiles - 1)
        {
          newSoundList[numSoundFiles - 1] = String(String(i+1) + soundList[0].substring(get_string_length(directoryName) + 1));
          if (directoryName == deerDir) newSoundList[numSoundFiles - 1].remove(7, 10);
          else if (directoryName == rabbitDir) newSoundList[numSoundFiles - 1].remove(9, 12);
        }
        else
        {
          newSoundList[i] = String(String(i+1) + soundList[i + 1].substring(get_string_length(directoryName)));
          if (directoryName == deerDir) newSoundList[i].remove(6, 10);
          else if (directoryName == rabbitDir) newSoundList[i].remove(8, 12);
        }
      }
    }
    else if (directoryName == deerRabbitDir)
    {
      for (int i = 0; i < numSoundFiles; i++)
      {
        newSoundList[i] = soundList[i];
      }
    }

    return newSoundList;
}

String* print_sound_files(String directoryName)
{
  SD_Card_Directory = SD.open(directoryName);
  Serial.println("Listing Directory's Sounds");
  newSoundList = list_sound_files(SD_Card_Directory, directoryName);

  return newSoundList;
}

/*
    This function will open the specified directory in the
    SD card, and then return the number of files located
    inside that directory.
    The sound files will get stored in an array, and then
    the function will return a random string from that array
    This return value gets used in the Play_Sound_Loop function
 */

String return_random_sound_file(String directoryName)
{
  int soundArrayIndex;
  SD_Card_Directory = SD.open(directoryName);
  newSoundList = list_sound_files(SD_Card_Directory, directoryName);
  soundArrayIndex = random(numAllSoundFiles);
  randomSoundFile = newSoundList[soundArrayIndex];
  Serial.println(randomSoundFile);

  return randomSoundFile;
}
