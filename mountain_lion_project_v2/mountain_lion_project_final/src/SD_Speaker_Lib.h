#ifndef __SD_SPEAKER_LIB_H__
#define __SD_SPEAKER_LIB_H__

#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"
#include "AudioFileSourceSD.h"
#include "driver/i2s.h"
#include "SD.h"
#include "SPIFFS.h"
#include "HTTPClient.h"
#include "Motor_Lib.h"
#include "LED_Strip_Lib.h"

const int I2S_DOUT = 22;
const int I2S_LRC = 25;
const int I2S_BCLK = 26;

const int SD_CS = 4;
const int SPI_SCK = 18;
const int SPI_MISO = 19;
const int SPI_MOSI = 23;

extern AudioGeneratorMP3 *mp3;
extern AudioFileSourceSD *file;
extern AudioOutputI2S *out;

extern File SD_Card_Directory;

extern volatile bool is_speaker_playing;
extern volatile bool is_speaker_random_playing;
extern volatile bool turn_speaker_off_flag;
extern int soundIndex;

// SD Card and Sound File Variables
extern int numSoundFiles;
extern int numAllSoundFiles;
extern int dirStringLen;
extern String *soundList;
extern String *newSoundList;
extern String directoryName;
extern String deerDir;
extern String rabbitDir;
extern String deerRabbitDir;
extern String randomSoundFile;

void SD_Speaker_setup();
float set_volume(float volume_level);
void play_random_sound_loop();
void play_specific_sound_loop(int soundIndex);
void turn_speaker_off();
int get_string_length(String directoryName);
int return_num_sound_files(File folderName);
String* list_sound_files(File folderName, String directoryName);
String* print_sound_files(String directoryName);
String return_random_sound_file(String directoryName);

#endif
