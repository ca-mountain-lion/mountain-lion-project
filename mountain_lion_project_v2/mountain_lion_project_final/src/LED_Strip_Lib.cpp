#include <LED_Strip_Lib.h>

Adafruit_NeoPixel led_strip(LED_STRIP_COUNT, LED_STRIP_PIN, NEO_GRB + NEO_KHZ800);

int pixel_interval = 50; // 50 ms
int pixel_queue = 0;
int pixel_cycle = 0;
int led_pattern_index = 0;
int led_flash_pattern_index = 0;
volatile bool led_strip_on_flag = 0;
volatile bool led_strip_random_on_flag = 0;
volatile bool led_strip_on_theater_chase_flag = 0;
volatile bool led_strip_off_flag = 0;
volatile bool led_strip_off_theater_chase_flag = 0;
volatile bool led_strip_on_flash_flag = 0;

void LED_Strip_Setup()
{
  led_strip.begin();
  led_strip.show();
  led_strip.setBrightness(50);
}

void turn_off_LEDs()
{
  Serial.println("Turning OFF LED Strip");
  led_strip.clear();
  led_strip.show();
}

void turn_on_red()
{
  Serial.println("Turning on Red");
  turn_off_LEDs();
  for (int i = 0; i < LED_STRIP_COUNT; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(255, 0, 0));
    led_strip.show();
    delay(0.005);
  }
}

void turn_on_orange()
{
  Serial.println("Turning on Orange");
  turn_off_LEDs();
  for (int i = 0; i < LED_STRIP_COUNT; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(255, 69, 0));
    led_strip.show();
    delay(0.005);
  }
}

void turn_on_yellow()
{
  Serial.println("Turning on Yellow");
  turn_off_LEDs();
  for (int i = 0; i < LED_STRIP_COUNT; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(220, 220, 0));
    led_strip.show();
    delay(0.005);
  }
}

void turn_on_green()
{
  Serial.println("Turning on Green");
  turn_off_LEDs();
  for (int i = 0; i < LED_STRIP_COUNT; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(0, 255, 0));
    led_strip.show();
    delay(0.005);
  }
}

void turn_on_blue()
{
  Serial.println("Turning on Blue");
  turn_off_LEDs();
  for (int i = 0; i < LED_STRIP_COUNT; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(0, 0, 255));
    led_strip.show();
    delay(0.005);
  }
}

void turn_on_purple()
{
  Serial.println("Turning on Purple");
  turn_off_LEDs();
  for (int i = 0; i < LED_STRIP_COUNT; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(128, 0, 128));
    led_strip.show();
    delay(0.005);
  }
}

void turn_on_white()
{
  Serial.println("Turning on Purple");
  turn_off_LEDs();
  for (int i = 0; i < LED_STRIP_COUNT; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(255, 255, 255));
    led_strip.show();
    delay(0.005);
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return led_strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return led_strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return led_strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

//Theatre-style crawling lights with rainbow effect
void theater_chase_rainbow(int wait_time)
{
  if(pixel_interval != wait_time)
  {
    pixel_interval = wait_time; // Update the delay time
  }

    for (int i = 0; i < 200; i++)
    {
      for(int i = 0; i < LED_STRIP_COUNT; i+=3)
      {
        led_strip.setPixelColor(i + pixel_queue, Wheel((i + pixel_cycle) % 255)); // Update the delay time
      }
      led_strip.show();
      for(int i = 0; i < LED_STRIP_COUNT; i+=3)
      {
        led_strip.setPixelColor(i + pixel_queue, led_strip.Color(0, 0, 0)); // Update the delay time
      }
      pixel_queue++;          // Advance the current color queue
      pixel_cycle++;          // Advance the current color cycle
      if(pixel_queue >= 3)
        pixel_queue = 0;      // Loop
      if(pixel_cycle >= 256)
        pixel_cycle = 0;      // Loop
      if (led_strip_off_theater_chase_flag == 1)
      {
        led_strip_off_theater_chase_flag = 0;
        turn_off_LEDs();
        break;
      }
    delay(50);
  }
}

void turn_on_theater_chase_rainbow()
{
  Serial.println("Starting Theater Chase Rainbow Pattern");
  turn_off_LEDs();
  theater_chase_rainbow(1000);
  turn_off_LEDs();
  Serial.println("End of Theater Chase Rainbow Pattern");
}

void check_led_strip_active_status()
{
  if (led_strip_on_flag == 1)
  {
    led_strip_on_flag = 0;
  }
  else if (led_strip_off_flag == 1)
  {
    led_strip_off_flag = 0;
    led_strip_on_flag = 0;
    turn_off_LEDs();
  }
}

void check_led_strip_active_theater_chase()
{
  if (led_strip_on_theater_chase_flag == 1)
  {
    led_strip_on_theater_chase_flag = 0;
    turn_on_theater_chase_rainbow();
  }
}

void turn_on_rainbow_pattern()
{
  turn_off_LEDs();
  Serial.println("Turning on Rainbow Pattern");
  // Turn on Red pixels
  for(int i = 0; i < 5; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(255, 0, 0));
    led_strip.show();
    delay(0.005);
  }

  // Turn on Orange pixels
  for(int i = 5; i < 10; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(255, 69, 0));
    led_strip.show();
    delay(0.005);
  }

  // Turn on Yellow pixels
  for(int i = 10; i < 15; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(220, 220, 0));
    led_strip.show();
    delay(0.005);
  }

  // Turn on Green pixels
  for(int i = 15; i < 20; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(0, 255, 0));
    led_strip.show();
    delay(0.005);
  }

  // Turn on Blue pixels
  for(int i = 20; i < 25; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(0, 0, 255));
    led_strip.show();
    delay(0.005);
  }

  // Turn on Purple pixels
  for(int i = 25; i < 31; i++)
  {
    led_strip.setPixelColor(i, led_strip.Color(128, 0, 128));
    led_strip.show();
    delay(0.005);
  }
}

void turn_on_red_flash()
{
  turn_on_red();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_red();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_red();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
}

void turn_on_orange_flash()
{
  turn_on_orange();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_orange();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_orange();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
}

void turn_on_yellow_flash()
{
  turn_on_yellow();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_yellow();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_yellow();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
}

void turn_on_green_flash()
{
  turn_on_green();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_green();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_green();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
}

void turn_on_blue_flash()
{
  turn_on_blue();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_blue();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_blue();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
}

void turn_on_purple_flash()
{
  turn_on_purple();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_purple();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_purple();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
}

void turn_on_white_flash()
{
  turn_on_white();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_white();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
  turn_on_white();
  delay(1000);
  turn_off_LEDs();
  delay(1000);
}

void led_flash_handler(int led_flash_pattern_index)
{
  if (led_strip_on_flash_flag == 1)
  {
    led_strip_on_flash_flag = 0;
    if (led_flash_pattern_index == 1) turn_on_red_flash();
    if (led_flash_pattern_index == 2) turn_on_orange_flash();
    if (led_flash_pattern_index == 3) turn_on_yellow_flash();
    if (led_flash_pattern_index == 4) turn_on_green_flash();
    if (led_flash_pattern_index == 5) turn_on_blue_flash();
    if (led_flash_pattern_index == 6) turn_on_purple_flash();
    if (led_flash_pattern_index == 7) turn_on_white_flash();
  }
}

void turn_on_random_pattern()
{
  if (led_strip_random_on_flag == 1)
  {
    Serial.println("Random Pattern is Enabled");
    led_strip_random_on_flag = 0;
    int random_pattern_num = random(16);

    Serial.println("Pattern Number: ");
    Serial.print(random_pattern_num);
    Serial.println("");

    if (random_pattern_num == 0) turn_on_red();
    if (random_pattern_num == 1) turn_on_orange();
    if (random_pattern_num == 2) turn_on_yellow();
    if (random_pattern_num == 3) turn_on_green();
    if (random_pattern_num == 4) turn_on_blue();
    if (random_pattern_num == 5) turn_on_purple();
    if (random_pattern_num == 6) turn_on_white();
    if (random_pattern_num == 7) turn_on_theater_chase_rainbow();
    if (random_pattern_num == 8) turn_on_rainbow_pattern();
    if (random_pattern_num == 9) turn_on_red_flash();
    if (random_pattern_num == 10) turn_on_orange_flash();
    if (random_pattern_num == 11) turn_on_yellow_flash();
    if (random_pattern_num == 12) turn_on_green_flash();
    if (random_pattern_num == 13) turn_on_blue_flash();
    if (random_pattern_num == 14) turn_on_purple_flash();
    if (random_pattern_num == 15) turn_on_white_flash();
  }
}
