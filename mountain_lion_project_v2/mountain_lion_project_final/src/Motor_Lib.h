#ifndef __MOTOR_LIB_H__
#define __MOTOR_LIB_H__
#include "Arduino.h"

const int MOTOR_IN1 = 5;
const int PWM1_Ch = 0;
const int PWM1_Res = 8;
const int PWM1_Freq = 500;

extern volatile bool motor_active_state;
extern int motor_state_counter;
extern unsigned long previous_motor_run_time;
extern long motor_run_duration;
extern int duty_cycle_clockwise;
extern int duty_cycle_counter_clockwise;

void motor_setup();
void stop_motor();
void handle_motor_duty_cycle(int duty_cycle, volatile byte motor_active_state);
void run_motor_clockwise_and_counter_clockwise();
void run_motor_clockwise(int duty_cycle, long motor_run_duration);
void run_motor_counter_clockwise(int duty_cycle, long motor_run_duration);
void update_motor_clockwise_state(long motor_run_duration);

#endif
