#include "Motor_Lib.h"

volatile bool motor_active_state = LOW;
int motor_state_counter = 0;
unsigned long previous_motor_run_time = 0;
long motor_run_duration = 3000;
int duty_cycle_clockwise = 135;
int duty_cycle_counter_clockwise = 250;

void motor_setup()
{
  ledcAttachPin(MOTOR_IN1, PWM1_Ch);
  ledcSetup(PWM1_Ch, PWM1_Freq, PWM1_Res);
}

void stop_motor()
{
  ledcWrite(PWM1_Ch, 0);
}

void handle_motor_duty_cycle(int duty_cycle, volatile byte motor_active_state)
{
  if (motor_active_state == HIGH)
  {
    if (duty_cycle <= 180 && duty_cycle >= 120)
    {
      Serial.print("Moving Clockwise | Duty Cycle ");
      Serial.println(duty_cycle);
      ledcWrite(PWM1_Ch, duty_cycle);
    }
    else if (duty_cycle <= 255 && duty_cycle >= 200)
    {
      Serial.print("Moving Counter-Clockwise | Duty Cycle ");
      Serial.println(duty_cycle);
      ledcWrite(PWM1_Ch, duty_cycle);
    }
    else if (duty_cycle == 0)
    {
      Serial.print("Motor Has Stopped | Duty Cycle ");
      Serial.println(duty_cycle);
      stop_motor();
    }
    else
    {
      Serial.println("Invalid Duty Cycle Value. Stopping Motor.");
      stop_motor();
    }
  }
  else
  {
    Serial.println("Motor Currently Not Active.");
    stop_motor();
  }
}

void run_motor_clockwise_and_counter_clockwise()
{
  if (motor_state_counter == 0)
  {
    stop_motor();
  }
  else if (motor_state_counter == 1)
  {
    handle_motor_duty_cycle(duty_cycle_clockwise, motor_active_state);
  }
  else if (motor_state_counter == 2)
  {
    stop_motor();
  }
  else if (motor_state_counter == 3)
  {
    handle_motor_duty_cycle(duty_cycle_counter_clockwise, motor_active_state);
  }
}

void run_motor_clockwise(int duty_cycle, long motor_run_duration)
{
  //stop_motor();
  if (duty_cycle <= 180 && duty_cycle >= 120)
  {
    Serial.print("Moving Clockwise | Duty Cycle ");
    Serial.println(duty_cycle);
    ledcWrite(PWM1_Ch, duty_cycle);
  }
  else if (duty_cycle == 0)
  {
    Serial.print("Motor Has Stopped | Duty Cycle ");
    Serial.println(duty_cycle);
    stop_motor();
  }
  else
  {
    Serial.println("Invalid Duty Cycle Value. Stopping Motor.");
    stop_motor();
  }
}

void run_motor_counter_clockwise(int duty_cycle, long motor_run_duration)
{
  stop_motor();
  if (duty_cycle <= 255 && duty_cycle >= 200)
  {
    Serial.print("Moving Counter-Clockwise | Duty Cycle ");
    Serial.println(duty_cycle);
    ledcWrite(PWM1_Ch, duty_cycle);
  }
  else if (duty_cycle == 0)
  {
    Serial.print("Motor Has Stopped | Duty Cycle ");
    Serial.println(duty_cycle);
    stop_motor();
  }
  else
  {
    Serial.println("Invalid Duty Cycle Value. Stopping Motor.");
    stop_motor();
  }
  delay(motor_run_duration);
  stop_motor();
}

void update_motor_clockwise_state(long motor_run_duration)
{
  unsigned long current_motor_run_time_millis = millis();
  stop_motor();
  if (current_motor_run_time_millis - previous_motor_run_time >= motor_run_duration)
  {
    previous_motor_run_time = current_motor_run_time_millis;

    if (motor_active_state == LOW)
    {
      motor_active_state = HIGH;
      Serial.print("Motor State: ");
      Serial.println(motor_active_state);
    }
    else
    {
      motor_active_state = LOW;
      Serial.print("Motor State: ");
      Serial.println(motor_active_state);
    }

  }
}

void update_motor_state(long motor_run_duration)
{
  unsigned long current_motor_run_time_millis = millis();

  if (current_motor_run_time_millis - previous_motor_run_time >= motor_run_duration)
  {
    previous_motor_run_time = current_motor_run_time_millis;
    if (motor_state_counter == 3)
    {
      motor_state_counter = 0;
    }
    else
    {
      motor_state_counter += 1;
    }

    Serial.print("Motor State Counter: ");
    Serial.println(motor_state_counter);

    if (motor_active_state == LOW)
    {
      motor_active_state = HIGH;
      Serial.print("Motor State: ");
      Serial.println(motor_active_state);
    }
    else
    {
      motor_active_state = LOW;
      Serial.print("Motor State: ");
      Serial.println(motor_active_state);
    }
    handle_motor_duty_cycle(150, motor_active_state);
  }
}
