#ifndef __LED_STRIP_LIB_H__
#define __LED_STRIP_LIB_H__

#include <Adafruit_NeoPixel.h>

const int LED_STRIP_PIN = 17;
const int LED_STRIP_COUNT = 30;
extern volatile bool led_strip_on_flag;
extern volatile bool led_strip_random_on_flag;
extern volatile bool led_strip_on_theater_chase_flag;
extern volatile bool led_strip_off_flag;
extern volatile bool led_strip_off_theater_chase_flag;
extern volatile bool led_strip_on_flash_flag;
extern int pixel_interval;
extern int pixel_queue;
extern int pixel_cycle;
extern int led_pattern_index;
extern int led_flash_pattern_index;

extern Adafruit_NeoPixel led_strip;

void LED_Strip_Setup();
void turn_off_LEDs();
void turn_on_red();
void turn_on_orange();
void turn_on_yellow();
void turn_on_green();
void turn_on_blue();
void turn_on_purple();
void turn_on_white();
uint32_t Wheel(byte WheelPos);
void theater_chase_rainbow(int wait_time);
void turn_on_theater_chase_rainbow();
void check_led_strip_active_status();
void check_led_strip_active_theater_chase();
void turn_on_rainbow_pattern();
void turn_on_random_pattern();
void turn_on_red_flash();
void turn_on_orange_flash();
void turn_on_yellow_flash();
void turn_on_green_flash();
void turn_on_blue_flash();
void turn_on_purple_flash();
void turn_on_white_flash();
void led_flash_handler(int led_flash_pattern_index);

#endif
