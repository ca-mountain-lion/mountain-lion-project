FROM ubuntu:20.04

RUN : \
    && apt-get update \
    && apt-get install -y \
    apt-utils \
    bison \
    ca-certificates \
    ccache \
    check \
    curl \
    flex \
    git \
    gperf \
    lcov \
    libffi-dev \
    libncurses-dev \
    libpython2.7 \
    libusb-1.0-0-dev \
    make \
    ninja-build \
    python3 \
    python3-pip \
    unzip \
    wget \
    xz-utils \
    zip \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 10 \
    && python -m pip install --upgrade \
    pip \
    virtualenv \
    && :

RUN mkdir project
ADD ca_mountain_lion/ project/ca_mountain_lion
ADD mountain_lion_project_v2/mountain_lion_project_final project/mountain_lion_project_final

RUN apt update -y \
    && apt install nano vim tree -y

WORKDIR project/mountain_lion_project_final

RUN pip install -U platformio \
    && pio run
