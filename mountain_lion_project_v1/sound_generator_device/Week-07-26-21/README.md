# Updates
First prototype is finally finished. The device's circuitry is housed in a smaller enclosure box, and it is contained in a larger, hinged enclosure box with a transparent lid. Also, the code has been cleaned up a bit and added the GPIO and motion sensor to a separate file.

Next week, another video demo will be made to show all of the full features of the device.

Summary:
* After powering it on, the LED blinks and indicates when it is finished setting up
* The OLED menu can allow the user to input a frequency value, list the sound files from the SD card, and test if each peripheral device is working
* Selecting "Start Device" allows the user to manually let the device play a random sound file
   * Will modify this to a better feature next week
* The power switch is connected to the batteries
* The buck-boost converter outputs a stable 3.3V to the ESP32 microcontroller. Four AA lithium batteries are used so that the device can last for weeks or months
  * How long the device can stay on needs to be tested
* An enable switch allows the user to turn on or off the OLED menu - it can still work without the OLED menu being active
* The red button acts as a reset button - this will make the device boot up from start
* Speaker will be changed to a smaller size

* **Bugs to fix:**
   * Deep sleep mode not being activated after the sound file is done playing
   * Tone generator doesn't play any sound after inputting frequency value

* Problems:
   * Outer enclosure's lid is affecting the motion sensor's sensitivity. It would be better to mount the motion sensor onto the transparent lid instead of within the smaller enclosure box
   * More small holes need to be added so the sound from the speaker is not too muffled

# Materials Used
| Part | QTY | Price ($) | Link |
| --- | --- | --- | --- |
| FireBeetle ESP32 Microcontroller | 1 | 6.90 per unit | [Product Link](https://www.dfrobot.com/product-1590.html)
| Adafruit MicroSD Card SPI Module | 1 | 2.95 per unit | [Product Link](https://www.mouser.com/ProductDetail/adafruit/4682/?qs=hWgE7mdIu5TtvwzYJhYD8g%3D%3D&countrycode=US&currencycode=USD)
| MAX98357 I2S Amplifier | 1 | 5.33 per unit | [Product Link](https://www.amazon.com/gp/product/B0912CWB7Z/)
| 3W 4-Ohm Speaker | 1 | 5.50 per unit | [Product Link](https://www.amazon.com/gp/product/B08JCHK7GR/)
| OLED Module (SSD1306) | 1 | 3.50 per unit | [Product Link](https://www.amazon.com/SSD1306-Self-Luminous-Display-Compatible-Raspberry/dp/B08FD643VZ/)
| Rotary Encoder Module | 1 | 2.00 per unit | [Product Link](https://www.amazon.com/gp/product/B07T3672VK/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
| Momentary Push Button Switch | 2 | 0.66 per unit | [Product Link](https://www.amazon.com/gp/product/B07F24Y1TB/)
| 2 Position DPDT Toggle Switch | 2 | 0.75 per unit | [Product Link](https://www.amazon.com/gp/product/B013DZB6CO/)
| AM312 Mini PIR Motion Sensor | 1 | 2.50 per unit | [Product Link](https://www.amazon.com/gp/product/B07NPKMH58)
| 4.5 x 3.5 x 2.2 Electronic Enclosure Box | 1 | 7.25 per unit | [Product Link](https://www.amazon.com/gp/product/B083H9FNRT/)
| 5.9 x 5.9 x 3.5 Hinged Shell Enclosure Box | 1 | 19.99 per unit | [Product Link](https://www.amazon.com/gp/product/B07PK84N5D/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)
| 100K Potentiometer | 1 | -- | [Product Link](https://www.amazon.com/gp/product/B07ZKJVZ53/)
| 5mm Green LED | 1 | -- | [Product Link](https://www.amazon.com/gp/product/B073QMYKDM/)
| 5mm LED Holder | 1 | -- | [Product Link](https://www.amazon.com/gp/product/B07WNMNS9P/ref=ppx_yo_dt_b_asin_title_o05_s00?ie=UTF8&psc=1)

# Video Demo
Click the image below to view the video demo:

[![Video Demo for ESP32 OLED Menu 2](https://github.com/aaron-nanas/MountainLion_Senior_Project/blob/main/Screenshots/DevicePhoto-07-28-21.JPG?raw=true)](https://youtu.be/kOQOH4HT9Ik)

# Development Tools
* Microcontroller: ESP32 (FireBeetle ESP32 IoT Microcontroller)
* Text Editor: Atom
* Environment: PlatformIO
* Languages: C++, C
