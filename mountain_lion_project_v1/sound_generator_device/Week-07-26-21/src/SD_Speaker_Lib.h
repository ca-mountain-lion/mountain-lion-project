#include "Arduino.h"
#include "ESP32-audioI2S/ESP32-audioI2S-master/src/Audio.h"
#include "SPI.h"
#include "SD.h"
#include "FS.h"
#include "HTTPClient.h"

//SD Card
const int SD_CS = 5;
const int SPI_MOSI = 23;
const int SPI_MISO = 19;
const int SPI_SCK = 18;

//Digital I/O used
const int I2S_DOUT = 27;
const int I2S_BCLK = 26;
const int I2S_LRC = 25;

extern Audio audio;

struct Music_info
{
    String name;
    int length;
    int runtime;
    int volume;
    int status;
    int mute_volume;
};
extern struct Music_info music_info;

extern String file_list[20];
extern int file_num;
extern int file_index;
extern uint run_time;
extern uint button_time;

void Speaker_SD_Setup();
void returnSoundFiles(String directoryName);
int get_music_list(fs::FS &fs, const char *dirname, uint8_t levels, String wavlist[30]);
void print_song_time();
void print_song_info();
void open_new_song(String filename);
