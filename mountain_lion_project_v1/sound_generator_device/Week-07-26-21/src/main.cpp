/*
  Description: This is the main.cpp file for the Sound Generator project.
  It will wait when motion is detected to play either a rabbit or deer
  distress sound file. Currently, the sound files are in mp3 format.

  ESP32 Pin Definitions:
    const int SD_CS = 5;
    const int I2C_SDA = 9;
    const int I2C_SCL = 10;
    #define SWITCH_ENABLE_PIN GPIO_NUM_12
    const int DT_PIN = 13;  // DT of Rotary Encoder
    const int CLK_PIN = 14;  // CLK of Rotary Encoder
    const int ENC_BUTTON_PIN = 15; // Encoder Push Button
    const int SPI_SCK = 18;
    const int SPI_MISO = 19;
    const int BUTTON_PIN = 22;
    const int SPI_MOSI = 23;
    const int I2S_LRC = 25;
    const int I2S_BCLK = 26;
    const int I2S_DOUT = 27;
    const int MOTION_SENSOR_PIN = 35;

  To-do:
    -- Add motion sensor and push button as external wake-up sources
    -- Fix Tone Generator feature
    -- Initialize device to be in deep-sleep
    -- Add another switch for menu control?
      -- This way, the menu doesn't have to be active when switch enable is active (less power consumption)
    -- Add battery level indicator
 */
#include "OLED_RotaryEnc_Lib.h"
#include "SD_Speaker_Lib.h"
#include "MotionSensor_Lib.h"

void setup() {
  Serial.begin(115200);
  Serial.println("Starting Menu");

  GPIO_Setup();

  WiFi.mode(WIFI_OFF);
  btStop();

  RotaryEncoder_Setup();
	Speaker_SD_Setup();
  OLED_Screen_Setup();
  touch_pad_intr_disable();

  splashScreen(); // Initial Splash Screen Display
  delay(1000); // Displays splash screen for one second
  displayBabyLion();
  delay(1000);

  mainMenu(); // Starts the menu

  blinkLED(); // To indicate that it has finished setting up
}

void loop() {
		audio.loop();

    if (millis() - run_time > 100)
    {
      run_time = millis();
      readEnableSwitch = digitalRead(SWITCH_ENABLE_PIN);
      if (menuTitle != "" && readEnableSwitch == 1) { // Start if menu is active
        menuCheck(); // Checks if encoder button is pressed
        menuItemSelection(); // Checks for change in highlighted items
        staticMenu(); // Displays the menu
        menuItemActions(); // Processes item selection
      }
      else if (readEnableSwitch == 0)
      {
        Serial.println("Switch OFF");
        menuItemClicked=100;
        //menuTitle = ""; // Turning Menu OFF
        display.clearDisplay();
        display.display();
      //goToSleep();
      }
    }
    playSong();
}
