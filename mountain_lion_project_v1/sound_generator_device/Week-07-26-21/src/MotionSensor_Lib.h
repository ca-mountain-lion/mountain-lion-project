#ifndef __MOTIONSENSOR_LIB_H__
#define __MOTIONSENSOR_LIB_H__

#include "Arduino.h"
#define SWITCH_ENABLE_PIN GPIO_NUM_12

const int sleepTimer = 2000;
const int MOTION_SENSOR_PIN = 35;
const int BUTTON_PIN = 22;
const int LED_PIN = 17;

extern volatile bool isMotionDetected;
extern volatile bool readEnableSwitch;
extern volatile bool isPlaying;
extern volatile bool isStarted;
extern volatile bool isSleepEnabled;

void GPIO_Setup();
void turnOnLED();
void turnOffLED();
void blinkLED();
IRAM_ATTR void testButton();
IRAM_ATTR void handleMotionSensor();
void goToSleep();

#endif
