#ifndef __OLED_ROTARYENC_LIB_H__
#define __OLED_ROTARYENC_LIB_H__

#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_I2CDevice.h>
#include "BabyLionBitmap.h"

extern bool serialDebug;
extern int itemTrigger;

// OLED Definitions
#define OLED_ADDR 0x3C
const int I2C_SDA = 9;
const int I2C_SCL = 10;
const int DT_PIN = 13;  // DT of Rotary Encoder
const int CLK_PIN = 14;  // CLK of Rotary Encoder
const int ENC_BUTTON_PIN = 15; // Encoder Push Button
const int POT_VOLUME_PIN = 34;

const int SCREEN_WIDTH = 128;                  // OLED display width, in pixels
const int SCREEN_HEIGHT = 64;                  // OLED display height, in pixels
const int OLED_RESET = -1;                     // Reset pin # (or -1 if sharing Arduino reset pin)
extern Adafruit_SSD1306 display;

// OLED Menu Variables
extern const byte menuMax;
extern const byte lineSpace1;
extern const byte lineSpace2;
extern String menuOption[];
extern byte menuCount;
extern String menuTitle;
extern byte menuItemClicked;
extern uint32_t lastEncoderStatus;
extern int OLEDDisplayTimeout;

// Rotary Encoder Variables
extern volatile int16_t CLK_Position;
extern volatile bool CLK_prevPosition;
extern volatile bool DT_prevPosition;
extern bool buttonState;
extern uint32_t reButtonTimer;
extern int reButtonMinTime;

extern int potValue;
extern int speakerVolume;

extern volatile bool isDeerSelected;
extern volatile bool isRabbitSelected;

// Function Prototypes
void turnOn();
void turnOff();
IRAM_ATTR void readEncoderValues();
void exitMenu();
//bool confirmActionRequired();
void staticMenu();
bool menuCheck();
void waitForButtonPress(int timeout);
void setMenu(byte inum, String iname);
void menuItemSelection();
int enterValue(String title, int start, int stepSize, int low, int high);
int chooseFromList(byte noOfElements, String listTitle, String list[]);
void mainMenu();
void goBackMainMenu(String titleName, int numItemClicked);
void startDeviceMenu();
void loadSongMenu();
void subMenu();
void testPeripheralMenu();
void handleStartDeviceMenu();
void handleLoadSongMenu();
void handleTestPeripheralsMenu();
void handleSubMenu();
int returnFrequencyValue();
void menuItemActions();
void splashScreen();
void RotaryEncoder_Setup();
void OLED_Screen_Setup();
void displayBabyLion();
void playSong();
int volumeMap(int x, int in_min, int in_max, int out_min, int out_max);
int adjustVolume();

#endif
