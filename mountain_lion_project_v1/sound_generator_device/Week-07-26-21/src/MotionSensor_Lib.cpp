/*
    This contains all the pin definitions for the LED indicator,
    the motion sensor, and the push button.

    It will also contain ISRs for the push button and the motion sensor,
    and functions for ESP32's sleep feature.
 */
#include "MotionSensor_Lib.h"

volatile bool isMotionDetected;
volatile bool readEnableSwitch;
volatile bool isPlaying;
volatile bool isStarted = 0;
volatile bool isSleepEnabled;

void GPIO_Setup()
{
  pinMode(MOTION_SENSOR_PIN, INPUT_PULLUP);
  pinMode(SWITCH_ENABLE_PIN, INPUT_PULLUP);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(MOTION_SENSOR_PIN), handleMotionSensor, RISING);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), testButton, FALLING);
}

void turnOnLED()
{
  digitalWrite(LED_PIN, HIGH);
}

void turnOffLED()
{
  digitalWrite(LED_PIN, LOW);
}

void blinkLED()
{
  turnOffLED();
  delay(200);
  turnOnLED();
  delay(200);
  turnOffLED();
  delay(200);
  turnOffLED();
  delay(200);
  turnOnLED();
  delay(200);
  turnOffLED();
}

IRAM_ATTR void testButton()
{
  Serial.println("Button is working!");
}

IRAM_ATTR void handleMotionSensor()
{
  unsigned long motionInterruptTime = millis();
  unsigned long lastMotionInterruptTime = 0;
  isMotionDetected = digitalRead(MOTION_SENSOR_PIN);
  readEnableSwitch = digitalRead(SWITCH_ENABLE_PIN);

  if (motionInterruptTime - lastMotionInterruptTime > 200)
  {
    if (isMotionDetected == 1)
    {
      Serial.println("Motion Detected!");
      isPlaying = 1;
      isMotionDetected = 0;
      //isSleepEnabled = 0;
      digitalWrite(LED_PIN, HIGH);
    }
    // else if ((isMotionDetected == 1) && (readEnableSwitch == 0))
    // {
    //   Serial.println("Switch not enabled");
    //   //isPlaying = 0;
    //   //isMotionDetected = 0;
    //   digitalWrite(LED_PIN, LOW);
    // }
  }
  lastMotionInterruptTime = motionInterruptTime;
  Serial.println(isMotionDetected);
}

void goToSleep()
{
  //unsigned long sleepTimer = millis();
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_35, HIGH);
  delay(sleepTimer);
  Serial.println("Entering Deep Sleep Mode");
  esp_deep_sleep_start();
}
