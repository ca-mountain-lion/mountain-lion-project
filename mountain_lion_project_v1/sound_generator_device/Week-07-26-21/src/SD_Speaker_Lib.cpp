#include "SD_Speaker_Lib.h"

Audio audio;

struct Music_info music_info = {.name = "", .length = 0, .runtime = 0, .volume = 0, .status = 0, .mute_volume = 0};

String file_list[20];
int file_num = 0;
int file_index = 0;
uint run_time = 0;
uint button_time = 0;

void returnSoundFiles(String directoryName)
{
  file_num = get_music_list(SD, directoryName.c_str(), 0, file_list);
  Serial.print("Number of Sound Files: ");
  Serial.println(file_num);
  Serial.println("All Sounds List: ");
  for (int i = 0; i < file_num; i++)
  {
      Serial.println(file_list[i]);
  }
}

void Speaker_SD_Setup()
{
  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, HIGH);
  SPI.begin(SPI_SCK, SPI_MISO, SPI_MOSI);
  SPI.setFrequency(1000000);
  if (!SD.begin(SD_CS, SPI))
  {
      Serial.println("SD Card Initialization Failed");
      while (1);
  }

  returnSoundFiles("/Deer_Sounds");
  audio.setPinout(I2S_BCLK, I2S_LRC, I2S_DOUT); // Initialize the pin definitions for the I2S Speaker
}

int get_music_list(fs::FS &fs, const char *dirname, uint8_t levels, String wavlist[30])
{
    Serial.printf("Listing directory: %s\n", dirname);
    int i = 0;

    File root = fs.open(dirname);
    if (!root)
    {
        Serial.println("Failed to open directory!");
        return i;
    }
    if (!root.isDirectory())
    {
        Serial.println("This is not a directory!");
        return i;
    }

    File file = root.openNextFile();
    while (file)
    {
        if (file.isDirectory()) // Continues if it is a directory
        {
        }
        else
        {
            String temp = file.name();
            if (temp.endsWith(".wav"))
            {
                wavlist[i] = temp;
                i++;
            }
            else if (temp.endsWith(".mp3"))
            {
                wavlist[i] = temp;
                i++;
            }
        }
        file = root.openNextFile();
    }
    return i;
}
/*
    Optional Functions:
      -- void open_new_song(String filename)
      -- void print_song_info()
      -- void print_song_time()
 */
void open_new_song(String filename)
{
    music_info.name = filename.substring(1, filename.indexOf("."));
    audio.connecttoFS(SD, filename.c_str());
    music_info.runtime = audio.getAudioCurrentTime();
    music_info.length = audio.getAudioFileDuration();
    music_info.volume = audio.getVolume();
    music_info.status = 1;
    Serial.println("Start a new sound");
}

void print_song_info()
{
    Serial.println("***********************************");
    Serial.println(audio.getFileSize());
    Serial.println(audio.getFilePos());
    Serial.println(audio.getSampleRate());
    Serial.println(audio.getBitsPerSample());
    Serial.println(audio.getChannels());
    Serial.println(audio.getVolume());
    Serial.println("***********************************");
}

void print_song_time()
{
    music_info.runtime = audio.getAudioCurrentTime();
    music_info.length = audio.getAudioFileDuration();
    music_info.volume = audio.getVolume();
}
