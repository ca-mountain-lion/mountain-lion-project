#include <Arduino.h>
#include "WiFi.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"
#include "AudioFileSourceSD.h"
#include "driver/i2s.h"
#include "SD.h"

// Trigger Pins
//#define TRIGGER2 21
#define LED       2 

// SD Card Pins
#define SD_CS          5
#define SPI_MOSI      23
#define SPI_MISO      19
#define SPI_SCK       18
#define deerDir       "/Deer_Sounds"
#define rabbitDir     "/Rabbit_Sounds"

// Potentiometer Pin
#define potPin        34

RTC_DATA_ATTR int bootCount = 0;

File SD_Card_Directory;
float potValue = 0.0;
float speakerVolume;
int numSoundFiles; // Variable to keep track of number of sound files in a directory
String *soundList; // String array that will hold the sound file names

//Initialize ESP8266 Audio Library classes
AudioGeneratorMP3 *mp3;
AudioFileSourceSD *file;
AudioOutputI2S *out;

volatile bool playing = 0;
volatile byte loadTrack = 0;

volatile byte state = 0;

#define GPIO_PIN_WAKEUP GPIO_NUM_27

void goToSleep() {
  digitalWrite(LED, HIGH);
  esp_sleep_enable_ext0_wakeup(GPIO_PIN_WAKEUP, HIGH);
  Serial.println("Entering deep sleep\n");
  delay(2000);
  digitalWrite(LED, LOW);
  WiFi.mode(WIFI_OFF);
  btStop();
  esp_deep_sleep_start(); 
}

// Function to map potentiometer values to volume (from 0.0 to max of 4.0)
float volumeMap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


//External Interrupt function with software switch debounce
void IRAM_ATTR handleInterrupt()
{
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  state = 1;
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 200) 
  {
    //Figure out which switch was triggered, and which track to play
//    if (!digitalRead(TRIGGER2)) {
//      loadTrack = 3;
//      digitalWrite(LED, LOW);
//    }
    if (digitalRead(GPIO_PIN_WAKEUP)) {
      //state = HIGH;
      loadTrack = 4;
      digitalWrite(LED, HIGH);
    }
    
    playing = 1;
  }
  last_interrupt_time = interrupt_time;
}

void ListSoundFiles(File folderName) {
  numSoundFiles = 0;
  
  while (true) {
    File entry =  folderName.openNextFile();
    
    if (!entry) {
      // no more files
      folderName.rewindDirectory();
      break;
    } else {
      numSoundFiles++;
    }
    entry.close();
  }
    Serial.print("Number of Sound Files in Directory: ");
    Serial.println(numSoundFiles);

    soundList = new String[numSoundFiles];

    for (uint8_t i = 0; i < numSoundFiles; i++) {
      File entry = folderName.openNextFile();
      soundList[i] = entry.name();
      entry.close();
      Serial.println(soundList[i]);
    }
}

float adjustVolume()
{
  potValue = analogRead(potPin);
  speakerVolume = volumeMap(potValue, 0, 4025, 0, 3.5);

  return speakerVolume;
}

void setup()
{  
  Serial.begin(115200);

  // LED
  pinMode(LED, OUTPUT);

  //Configure trigger pins to inputs with internal pull-up resistors enabled
  //pinMode(TRIGGER2, INPUT_PULLUP);
  pinMode(GPIO_PIN_WAKEUP, INPUT);
    
  // SD Card
  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, HIGH);
  SPI.begin(SPI_SCK, SPI_MISO, SPI_MOSI);

  //Create interrupts for each trigger
  //attachInterrupt(digitalPinToInterrupt(TRIGGER2),handleInterrupt,FALLING);
  attachInterrupt(digitalPinToInterrupt(GPIO_PIN_WAKEUP), handleInterrupt, RISING);
  
  out = new AudioOutputI2S();
  mp3 = new AudioGeneratorMP3();

  delay(50);
  Serial.print("Initializing SD card...");
  SD.begin(SD_CS);
  if (!SD.begin(SD_CS))
  {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  delay(100);
  SD_Card_Directory = SD.open("/Deer_Sounds");
  Serial.println("Listing Deer Sounds");
  ListSoundFiles(SD_Card_Directory);
//  SD_Card_Directory = SD.open(rabbitDir);
//  Serial.println("Listing Rabbit Sounds");
//  ListSoundFiles(SD_Card_Directory);
  Serial.println("Done!");
}


void loop() {
    potValue = analogRead(potPin);
    //Serial.println(potValue);
    speakerVolume = volumeMap(potValue, 0, 4025, 0, 3.5);
    //Serial.println(speakerVolume);
    out -> SetGain(speakerVolume);
    //Serial.println(state);

    if (loadTrack) {
      if(playing && mp3->isRunning()) mp3->stop();

      if (loadTrack == 4) file = new AudioFileSourceSD("/Deer_Sounds/1_Deer.mp3");
      mp3 -> begin(file, out);
      loadTrack = 0;
    }

    if (loadTrack == 3) {
      if (playing && mp3->isRunning()) {
        mp3->stop();
        playing = 0;
        state = 0;
        digitalWrite(LED, LOW);
      }
    }

    if(playing && mp3->isRunning()) {
      if (!mp3->loop())
      {
        mp3->stop();
        playing = 0;
        state = 0;
        Serial.println("Stopped");
        digitalWrite(LED, LOW);
        goToSleep();
      }
    }
}