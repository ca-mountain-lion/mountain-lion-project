# Updates
The board for the project has been replaced. This is the first version of the Sound Generator device that uses ESP32 (replaced the Raspberry Pi Zero W board). Currently only interfacing the motion sensor and the speaker.

Summary:
* Since I am unfamiliar with the ESP32, I have to spend more time reading about which features are available and get used to the new programming environment. I plan on using Atom as the primary text editor and the PlatformIO package. As such, the main language used will then be from Python to C++
    * Main test file is SoundGeneratorTest, while the WIP file is in src
* The purpose of the video demo is to show that it is possible to move the Sound Generator project from the Raspberry Pi to ESP32
* The speaker used rated at 3W and 4-Ohm, and the I2S amplifier is a MAX98357 module
* The potentiometer can be used to control the volume level
* Planned features are the following:
    * OLED Menu with a rotary encoder (currently WIP)
    * Make the device run on four AA lithium batteries
    * Enable sleep mode after it has not detected any motion for a certain amount of time (about a few seconds after it has stopped playing the sound file)
    * Interface SD card module - this will contain all of the deer / rabbit distress sound files
      * Add a function that will search for the contents of the SD card (.mp3 or .wav files), return the titles of the sound files and store them into an array, and display the String array onto the OLED menu
    * Add several buttons for select or resetting the device. Note that the rotary encoder also has a built-in push button

# Planned Enclosure Design
![Link for Enclosure](https://github.com/aaron-nanas/MountainLion_Senior_Project/blob/main/Screenshots/EnclosureUpdate.png?raw=true)

# Video Demo
Click the image below to view the video demo:

[![First Video Demo for ESP32](https://github.com/aaron-nanas/MountainLion_Senior_Project/blob/main/Screenshots/ESP32_FirstPhoto.png?raw=true)](https://youtu.be/0Zs3gWum7l8)

# Development Tools
* Microcontroller: ESP32
* Text Editor: Atom
* Environment: PlatformIO
* Languages: C++, C
