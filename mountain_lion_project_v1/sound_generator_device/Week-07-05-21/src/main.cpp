#include "SoundGenerator_Lib/OLED_RotaryEnc_Lib.h"
#include "SoundGenerator_Lib/SD_I2S_Speaker_Lib.h"

void setup() {
  Serial.begin(115200);
  Serial.println("Starting");
  delay(200);

  // Setup Functions
  WiFi.mode(WIFI_OFF);
  btStop();
  init_buzzer_LED();
  OLED_Setup();
  Encoder_Setup();
  SD_Speaker_Setup();

  delay(500);

  Serial.println("Setup Complete");

}

void loop() {
    main_loop();
}
