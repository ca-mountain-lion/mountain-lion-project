#include "OLED_RotaryEnc_Lib.h"
#include "Menu_Config_Lib.h"
#include "ArrowBitmap.h"
#include "BabyLionBitmap.h"
#include "SD_I2S_Speaker_Lib.h"

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
NewEncoder encoder(encoderA_Pin, encoderB_Pin, 0, 10, 0, FULL_PULSE);

int switchEnableStatus;

// Rotary Encoder Variables
int buttonState;                     // the current reading from the input pin
int lastButtonState = HIGH;          // the previous reading from the input pin
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 20;    // the debounce time
int prevEncoderValue;
int encoderValue;
int currentValue;                // global value for the rotary encoder for menu navigation and screen arrows

// Piezo Variables
//unsigned long previousBeepTime = 0;

// Menu Variables
int prevDirectionValue = 0;
int arrowStatus;
bool goingUp;
bool aboutMenuStatus;
bool playingSoundMenuStatus;
bool displayBabyLionStatus;
bool isMenuEnabled;

//char const* mainMenuLines[];

void Encoder_Setup () {
    if (!encoder.begin()) {

    Serial.println(F("Encoder Failed to Start. Check pin assignments and available interrupts. Aborting."));
    while (1) {
    }
  } else {
    encoderValue = encoder;
    Serial.print(F("Encoder Successfully Started at value = "));
    Serial.println(encoderValue);
  }
}

void OLED_Setup() {
  Wire.begin(I2C_SDA, I2C_SCL);

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {     //Initialize the display with address 0x3C,
    Serial.println(F("SSD1306 allocation failed"));     //Done in a for loop so if it fails it would continue
    for (;;);                                           // Don't proceed, loop forever to try and initialize
  }

  display.clearDisplay();          // clears buffer
  display.setTextColor(WHITE);     // Set color of the text
  display.setRotation(0);          // Set orientation. Goes from 0, 1, 2 or 3
  display.setTextWrap(false);      // By default, long lines of text are set to automatically “wrap” back to the leftmost column.

}

void init_buzzer_LED() {
    pinMode(buttonPin, INPUT);
    pinMode(LED_PIN, OUTPUT);
    //pinMode(switchEnablePin, INPUT_PULLUP);

    //pinMode(piezoPin, OUTPUT);

}

bool displayBabyLion() {
  Serial.println("Printing Case 2");
  rotaryEncoder();
  digitalWrite(LED_PIN, HIGH);

  display.clearDisplay();
  display.drawBitmap(0, 0, image_data_lion, 128, 64, 1);
  display.display();

  if (buttonPress()) {
    return true;
  } else {
    return false;
  }
}

void printMessage1() {
    Serial.println("Printing Case 1");
    digitalWrite(LED_PIN, LOW);
    display.clearDisplay();
    printSoundFiles(deerDir);
}

void printMessage3() {
    loadTrack = 1;
    isPlaying = 1;
    Serial.println("Printing Case 3");
    //Serial.println("Load Track Value: " + loadTrack);
}

void printMessage4() {
    Serial.println("Printing Case 4");
}

void main_loop() {
  int loopEncoderValue = mainMenu();

  if (buttonPress()) {

    switch (loopEncoderValue) {

      case 0:
        printMessage1();
        break;

      case 1:
        do {
          displayBabyLionStatus = displayBabyLion();
        } while (displayBabyLionStatus == false);
        break;

      case 2:
        printMessage3();
        do {
            playingSoundMenuStatus = music_loop();
            //playMusic();
        } while (playingSoundMenuStatus == false);
        break;

      case 3:
        Serial.println("case 3");
        do {
            aboutMenuStatus = aboutMenu();
        } while (aboutMenuStatus == false);
        break;
      case 4:
        printMessage4();
        break;

      case 5:
        Serial.println("case 5");
        break;

    }
  }
}

int rotaryEncoder() {
  bool up, down;
  up = encoder.upClick();
  down = encoder.downClick();

  if (up || down) {

    currentValue = encoder;

    if (currentValue != prevEncoderValue) {
      //Serial.print(F("Encoder: "));
      //Serial.println(currentValue);
      prevEncoderValue = currentValue;

      rotaryDirection();
      arrowStatusFunct();

    } else if (up) {
      Serial.println(F("Rotary Encoder reached the upper limit."));
    } else {
      Serial.println(F("Rotary Encoder reached the lower limit."));
      }
    }
    return (currentValue);
}

void rotaryDirection() {

  int directionValue = currentValue;

  if (directionValue > prevDirectionValue) {
    //Serial.println("Rotary Encoder is going up");
    goingUp = true;
  } else if (directionValue < prevDirectionValue) {
    //Serial.println("Rotary Encoder is going down");
    goingUp = false;
  }
  prevDirectionValue = directionValue;
}

int buttonPress() {

  int reading = digitalRead(buttonPin);

  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {

    if (reading != buttonState) {
      buttonState = reading;

      if (buttonState == LOW) {
        Serial.println("");
        Serial.println("Encoder Button has been pressed!");
        //piezoBeep();
        return(true);
      }
    }
  }
  lastButtonState = reading;
  return(false);

}

void arrowStatusFunct() {
  if (goingUp == true && arrowStatus != 32) {
    arrowStatus = arrowStatus + 8;
  } else if (goingUp == false && arrowStatus != 0) {
    arrowStatus = arrowStatus - 8;
  } else if (goingUp == true && arrowStatus == 32) {
    arrowStatus = 32;
  } else if (goingUp == false && arrowStatus == 0) {
    arrowStatus = 0;
  }
}

int mainMenu() {

  int8_t encoderValue = rotaryEncoder();

  int mainMenuArraySize = sizeof(mainMenuLines);          //sizeof returns a number that is 4x the number of lines in the array (4 bytes each entry)
  int mainMenuEncoderMax = (mainMenuArraySize / 4) - 1 ;  // this gives us a number max for the encoder, otherwise the ESP was overflowing


  if (encoderValue >= mainMenuEncoderMax) {               //if the encoder value goes over our max
    encoder.setValue(mainMenuEncoderMax);                 //this resets it to our max and prevents overflow
    encoderValue = mainMenuEncoderMax;
  }

  display.clearDisplay();
  //display.setTextSize(2, 2);
  display.setCursor(0, 0);
  display.fillRoundRect(0, 0, 127, 8, 3, WHITE); //X, Y, Width, Height, Corner Fillet
  display.drawRect(0, 0, 127, 60, WHITE);
  display.setCursor(20, 0);
  display.setTextColor(BLACK);
  display.println("Sound Generator");
  display.setTextColor(WHITE);
  display.setCursor(0, 8);

  if (goingUp == true) {
    if (arrowStatus == 0) {
      for (int i = encoderValue; i <= encoderValue + 4; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    } else if (arrowStatus == 8) {
      for (int i = encoderValue - 1; i <= encoderValue + 3; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    } else if (arrowStatus == 16) {
      for (int i = encoderValue - 2; i <= encoderValue + 2; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    } else if (arrowStatus == 24) {
      for (int i = encoderValue - 3 ; i <= encoderValue + 1; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    } else if (arrowStatus == 32) {
      for (int i = encoderValue - 4; i <= encoderValue; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    } else if (arrowStatus == 40) {
        arrowStatus = 32;
        for (int i = encoderValue - 4; i <= encoderValue; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    }

  } else if (goingUp == false) {
    if (arrowStatus == 0) {
      for (int i = encoderValue; i <= encoderValue + 4; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    } else if (arrowStatus == 8) {
      for (int i = encoderValue - 1 ; i <= encoderValue + 3; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    } else if (arrowStatus == 16) {
      for (int i = encoderValue - 2 ; i <= encoderValue + 2; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    } else if (arrowStatus == 24) {
      for (int i = encoderValue - 3 ; i <= encoderValue + 1; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    } else if (arrowStatus == 32) {
      for (int i = encoderValue - 4; i <= encoderValue; i++) {
        display.print(" ");
        display.println(mainMenuLines[i]);
      }
    }
  }
  display.drawBitmap(0, arrowStatus + 8, arrowBitmap, 10, 8, WHITE); // display.drawBitmap(x position, y position, bitmap data, bitmap width, bitmap height, colo
  display.display();
  return (encoderValue);
}

bool aboutMenu() {

  rotaryEncoder();

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextColor(WHITE);
  display.println("     Bapy");
  display.println("     Alexei");
  display.println("     Rasputin");
  display.println("     David");
  display.display();

  if (buttonPress()) {
    return true;                          //returning true breaks us out of the about menu
  } else {
    return false;
  }
}

bool music_loop()
{
  rotaryEncoder();

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextColor(WHITE);
  display.println("     Playing Music");
  display.display();
  playMusic();

  if (buttonPress()) {
    mp3->stop();
    isPlaying = 0;
    return true;                          //returning true breaks us out of the about menu
  } else {
    return false;
  }
}

void playMusic() {
  //speakerVolume = adjustVolume();
  //Serial.println(speakerVolume);
  //out -> SetGain(speakerVolume);

  if (loadTrack) {
    if (isPlaying && mp3->isRunning()) mp3->stop();

    if (loadTrack == 1) file = new AudioFileSourceSD("/Deer_Sounds/1_Deer.mp3");

    out -> SetGain(0.2);
    mp3-> begin(file, out);
    loadTrack = 0;
  }

    if(isPlaying && mp3->isRunning())
    {
      if (!mp3->loop())
      {
        mp3->stop();
        isPlaying = 0;
        Serial.println("Stopped");
      }
    }
}

// void piezoBeep() {
//
//   previousBeepTime = millis();
//
//   do {
//     int x = millis();
//     if (x % 2 == 0) {
//       digitalWrite(piezoPin, HIGH);           //piezo fluctuates a tone based off of whether or not millis is even or odd
//     } else {
//       digitalWrite(piezoPin, LOW);
//     }
//
//   } while (millis() - previousBeepTime < 100);
//
//   digitalWrite(piezoPin, LOW);               //this ensures the speaker is silent afterwards
//
// }
