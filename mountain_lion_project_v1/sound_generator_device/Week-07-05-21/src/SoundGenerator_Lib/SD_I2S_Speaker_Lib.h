#include "SD.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"
#include "AudioFileSourceSD.h"
#include "driver/i2s.h"
#include "HTTPClient.h"
#include "SPIFFS.h"

// Sound File Directories
//#define deerDir "/Deer_Sounds"
//#define rabbitDir "/Rabbit_Sounds"

const int SD_CS = 27;
const int SPI_SCK = 18;
const int SPI_MISO = 19;
const int SPI_MOSI = 23;
const int potPin = 34;

/* I2S Speaker Pin Definitions
*  DIN    -    GPIO 22
*  LRCKL  -    GPIO 25
*  BCLK   -    GPIO 26
*/


extern File SD_Card_Directory;
extern float potValue;
extern float speakerVolume;
extern int numSoundFiles; // Variable to keep track of number of sound files in a directory
extern String *soundList; // String array that will hold the sound file names
extern String *newSoundList;
//extern String directoryName;
extern String deerDir;
extern String rabbitDir;
extern int dirStringLen;

//Initialize ESP8266 Audio Library classes
extern AudioGeneratorMP3 *mp3;
extern AudioFileSourceSD *file;
extern AudioOutputI2S *out;

extern volatile bool isPlaying;
extern volatile byte loadTrack;
extern volatile byte state;

void SD_Speaker_Setup();
void printSoundFiles(String directoryName);
int getStringLen(String directoryName);
float volumeMap(float x, float in_min, float in_max, float out_min, float out_max);
void ListSoundFiles(File folderName, String directoryName);
float adjustVolume();
