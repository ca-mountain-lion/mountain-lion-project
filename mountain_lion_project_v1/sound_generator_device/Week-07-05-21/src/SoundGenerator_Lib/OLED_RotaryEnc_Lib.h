#ifndef __OLED_ROTARYENC_LIB_H__
#define __OLED_ROTARYENC_LIB_H__

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_I2CDevice.h>
#include "Arduino.h"
#include "NewEncoder.h"

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET -1

// Pin Definitions
const int I2C_SDA = 4;
const int I2C_SCL = 5;
const int encoderA_Pin = 13;
const int encoderB_Pin = 14;
const int buttonPin = 15;
//const int switchEnablePin = 21;
const int LED_PIN = 2;
//const int piezoPin = 21;

extern int switchEnableStatus;

// Rotary Encoder Variables
extern int buttonState;                     // the current reading from the input pin
extern int lastButtonState;          // the previous reading from the input pin
extern unsigned long lastDebounceTime;  // the last time the output pin was toggled
extern unsigned long debounceDelay;    // the debounce time
extern int prevEncoderValue;
extern int encoderValue;
extern int currentValue;                // global value for the rotary encoder for menu navigation and screen arrows

// Piezo Variables
//extern unsigned long previousBeepTime;

// Menu Variables
extern int prevDirectionValue;
extern int arrowStatus;
extern bool goingUp;
extern bool aboutMenuStatus;
extern bool playingSoundMenuStatus;
extern bool displayBabyLionStatus;
extern bool isMenuEnabled;

//extern static const uint8_t image_data_lion[1024];
extern const unsigned char arrowBitmap [] PROGMEM;
extern char const* mainMenuLines[];

// Encoder and OLED Initialization
extern Adafruit_SSD1306 display;
extern NewEncoder encoder;
//extern TaskHandle_t AudioTask;

// Function Prototypes
void main_loop();
void printMessage1();
void printMessage3();
void printMessage4();
int rotaryEncoder();
void rotaryDirection();
int buttonPress();
void arrowStatusFunct();
int mainMenu();
bool aboutMenu();
bool music_loop();
//void piezoBeep();
void init_buzzer_LED();
void OLED_Setup();
void Encoder_Setup();
bool displayBabyLion();
void playMusic();

#endif // __OLED_ROTARYENC_LIB_H not defined
