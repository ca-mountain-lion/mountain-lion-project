#include "SD_I2S_Speaker_Lib.h"

float potValue = 0.0;
float speakerVolume;
int numSoundFiles; // Variable to keep track of number of sound files in a directory
int dirStringLen;
String *soundList;
String *newSoundList;
//String directoryName;
String deerDir = "/Deer_Sounds";
String rabbitDir = "/Rabbit_Sounds";
volatile bool isPlaying = 0;
volatile byte loadTrack = 0;
volatile byte state = LOW;

File SD_Card_Directory;
AudioGeneratorMP3 *mp3;
AudioFileSourceSD *file;
AudioOutputI2S *out;

void SD_Speaker_Setup() {
  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, HIGH);
  SPI.begin(SPI_SCK, SPI_MISO, SPI_MOSI);

  out = new AudioOutputI2S();
  mp3 = new AudioGeneratorMP3();
  delay(50);
  Serial.print("Initializing SD card...");
  SD.begin(SD_CS);
  if (!SD.begin(SD_CS))
  {
    Serial.println("Initialization Failed!");
    return;
  }
  Serial.println("SD Card Initialization Done!");
  delay(100);

}

void printSoundFiles(String directoryName) {
  SD_Card_Directory = SD.open(directoryName);
  Serial.println("Listing Directory's Sounds");
  ListSoundFiles(SD_Card_Directory, directoryName);
}

int getStringLen(String directoryName) {
  dirStringLen = directoryName.length() + 2;
  // Uncomment for debugging
  //Serial.println(dirStringLen);
  return dirStringLen;
}

// Function to map potentiometer values to volume (from 0.0 to max of 4.0)
float volumeMap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void ListSoundFiles(File folderName, String directoryName) {
  numSoundFiles = 0;

  while (true) {
    File entry =  folderName.openNextFile();
    if (!entry) {
      // no more files
      folderName.rewindDirectory();
      break;
    } else {
      numSoundFiles++;
    }
    entry.close();
  }
    Serial.print("Number of Sound Files in Directory: ");
    Serial.println(numSoundFiles);

    soundList = new String[numSoundFiles];
    newSoundList = new String[numSoundFiles];

    for (int i = 0; i < numSoundFiles; i++) {
      File entry = folderName.openNextFile();
      soundList[i] = entry.name();
      entry.close();
      Serial.println(soundList[i]);
      newSoundList[i] = soundList[i].substring(getStringLen(directoryName));

    }

    Serial.println("Printing out new array");
    for (int i = 0; i < numSoundFiles; i++)
    {
      if (i == numSoundFiles - 1) {
        Serial.print((String)(i+1) + newSoundList[i] + "");
      }

      else
      {
        Serial.print((String)(i+1) + newSoundList[i] + ", ");
      }
    }

}

float adjustVolume()
{
  potValue = analogRead(potPin);
  speakerVolume = volumeMap(potValue, 0, 4025, 0, 3.5);

  return speakerVolume;
}
