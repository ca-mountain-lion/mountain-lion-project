# Updates
Project is almost finished. There are still some bugs that need to be fixed, and I am hoping to fix those by the end of the week.

**Note**: Currently working on migrating project over to use ESP-IDF

Weekly Summary:
* Replaced the previous I2S audio library with a different one. 
* Fixed timing issue since audio was a bit choppy while the OLED menu was on
* Some bugs to fix are the following:
  * ~~Tone Generator feature not working properly - does not play after inputting frequency value~~ ~~Just need to make the frequency value dynamically programmable (i.e. respond to frequency input from OLED)~~ Done!
  * ~~Storing the file names in a String array. Need to be able to return the String array and display contents instead of initializing file names in the function~~ Done!

# Development Tools
* Microcontroller: ESP32 (FireBeetle ESP32 IoT Microcontroller)
* Text Editor: Atom
* Environment: PlatformIO
* Languages: C++, C
