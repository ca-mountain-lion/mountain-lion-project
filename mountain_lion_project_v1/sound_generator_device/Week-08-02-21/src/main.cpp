/*
  Description: This is the main.cpp file for the Sound Generator project.
  It will wait when motion is detected to play either a rabbit or deer
  distress sound file. The sound files from the SD card are in .mp3 format.

  ESP32 Pin Definitions:
    const int SD_CS = 5;
    const int I2C_SDA = 9;
    const int I2C_SCL = 10;
    #define SWITCH_ENABLE_PIN GPIO_NUM_12
    const int DT_PIN = 13;  // DT of Rotary Encoder
    const int CLK_PIN = 14;  // CLK of Rotary Encoder
    const int ENC_BUTTON_PIN = 15; // Encoder Push Button
    const int BUTTON_PIN = 16; // Removed for now
    const int LED_PIN = 17;
    const int SPI_SCK = 18;
    const int SPI_MISO = 19;
    const int I2S_DOUT = 22;
    const int SPI_MOSI = 23;
    const int I2S_LRC = 25;
    const int I2S_BCLK = 26;
    const int POT_VOLUME_PIN = 34;
    const int MOTION_SENSOR_PIN = 35;
 */
#include "OLED_RotaryEnc_Lib.h"
#include "SD_Speaker_Lib.h"
#include "MotionSensor_Lib.h"

void setup() {
  Serial.begin(115200);
  Serial.println("Starting Menu");

  GPIO_Setup();
  WiFi.mode(WIFI_OFF);
  btStop();
  RotaryEncoder_Setup();
  SD_Speaker_Setup();
  OLED_Screen_Setup();
  touch_pad_intr_disable();

  readEnableSwitch = digitalRead(SWITCH_ENABLE_PIN);

  if (readEnableSwitch == 0)
  {
    clearMenu();
  }
  else
  {
  splashScreen();         // Initial Splash Screen Display
  delay(2000);            // Displays splash screen for two seconds
  AuthorFacultyScreen();  // Contributors Screen
  delay(2000);            // Displays for two seconds
  displayBabyLion();      // Black/white image of baby mountain lion
  delay(1000);            // Displays for one second
  }
  mainMenu();             // Starts the menu
  blinkLED();             // LED indicator: To indicate that it has finished setting up
  isMotionDetected = 0;   // Ensure this variable is 0 when setting up
  Timer_Setup_Init();
}

void loop()
{
  OLED_Menu_Loop();
  Play_Sound_Loop();
  runToneGenerator();
  enableActivateSleepMode();
  enableBootSleepMode();
}
