/*
    Interfaces OLED menu system with a rotary encoder.
    The CLK and DT pins of the rotary encoder are used to
    navigate through the menu, while its push button is
    used to select the item.

    Based on: https://github.com/alanesq/BasicOLEDMenu
 */
#include "OLED_RotaryEnc_Lib.h"
#include "SD_Speaker_Lib.h"
#include "MotionSensor_Lib.h"

bool serialDebug = 1;                     // This enables debug on Serial Port (Optional)
int itemTrigger = 2;                      // Rotary Encoder Counts Per Click

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// OLED Menu Variables
const byte menuMax = 5;                   // Sets the maximum number of menu items
const byte lineSpace1 = 9;                // Line spacing (6 lines) // 9
const byte lineSpace2 = 16;               // Line spacing (4 lines)
String menuOption[menuMax];               // Displays the available menu options
byte menuCount = 0;                       // Tracks which menu option is highlighted
String menuTitle = "";                    // Current menu ID number (if blank string, then none)
byte menuItemClicked = 100;               // Menu item has been clicked flag (100 = None)
uint32_t lastEncoderStatus = 0;           // Tracks the last time the rotary encoder had activity
int OLEDDisplayTimeout = 10;              // OLED menu display timeout (in seconds)

// Rotary Encoder Variables
volatile int16_t CLK_Position = 0;        // Tracks the current value of the CLK pin (which gets updated in the ISR)
volatile bool CLK_prevPosition = 0;       // Tracks previous position (to see if it has changed -- also for debouncing)
volatile bool DT_prevPosition = 0;        // Tracks previous position (to see if it has changed -- also for debouncing)
bool buttonState = 0;                     // Reads state of the encoder's push button
uint32_t reButtonTimer = millis();        // Tracks the last time the encoder's push button had activity
int reButtonMinTime = 500;                // Minimum milliseconds between allowed button status changes

// SD Card File Variables
volatile bool isDeerSelected;
volatile bool isRabbitSelected;
volatile bool ledState;

// Timer Variables
hw_timer_t * timer = NULL; // This creates a hardware timer
hw_timer_t * BootUpTimer = NULL;
volatile bool isSleepActivated = 0;
volatile bool readStopButtonStatus;

// Load Song Variables
int soundIndex;
float hertzValue;

void RotaryEncoder_Setup()
{
  // Configuring GPIO Pins
  pinMode(ENC_BUTTON_PIN, INPUT_PULLUP);
  pinMode(CLK_PIN, INPUT);
  pinMode(DT_PIN, INPUT);

  // Interrupt for reading Rotary Encoder values
  attachInterrupt(digitalPinToInterrupt(CLK_PIN), readEncoderValues, CHANGE);
}

void OLED_Screen_Setup()
{
  // Initializing OLED Display
  Wire.begin(I2C_SDA,I2C_SCL);
  if(!display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR))
  {
    Serial.println("\nError Initializing the OLED Display!\n");
    for(;;);
  }
}

// Timer ISR to activate sleep mode after it has triggered
// Will be called after the time specified for sleepTimeInSeconds has passed
void IRAM_ATTR onTimer()
{
  isSleepActivated = 1;
}

void IRAM_ATTR bootOnTimer()
{
  isDeviceIdle = 1;
}

// Timer used to activate sleep mode
void Timer_Setup_Enable()
{
  // Timer 0, (1/(80 MHz / 80)), True = repeat the timer)
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, (sleepTimeInSeconds*1000000), true); // After 5 seconds, it activates sleep mode
  timerAlarmEnable(timer);
}

// Timer used to activate sleep mode if no motion is detected at device bootup after 15 seconds
void Timer_Setup_Init()
{
  // Timer 1, (1/(80 MHz / 80)), True = repeat the timer)
  BootUpTimer = timerBegin(1, 80, true);
  timerAttachInterrupt(BootUpTimer, &bootOnTimer, true);
  timerAlarmWrite(BootUpTimer, (bootSleepTimeInSeconds*1000000), true);
  timerAlarmEnable(BootUpTimer);
}

void displayBabyLion()
{
  display.clearDisplay();
  display.drawBitmap(0, 0, image_data_lion, 128, 64, 1);
  display.display();
}

void clearMenu()
{
  turnOffLED();
  menuItemClicked = 100;
  menuTitle = ""; // Turning Menu OFF
  display.clearDisplay();
  display.display();
}

void Play_Sound_Loop()
{
  if (millis() - playSoundTime > 1000)
  {
    if (loadSoundFile)
    {
      speakerVolume = adjustVolume();
      out -> SetGain(speakerVolume); // Volume level is based on potentiometer value
      Serial.println(speakerVolume); // Comment this out
      isDeviceIdle = 0;

      if (isPlaying && mp3->isRunning()) mp3->stop();

      if (loadSoundFile == 1) file = new AudioFileSourceSD(returnRandomSoundFile(deerRabbitDir).c_str());
      mp3->begin(file, out);
      loadSoundFile = 0;
      playSoundTime = millis();
    }

    if (isTestActive)
    {
      speakerVolume = adjustVolume();
      out -> SetGain(speakerVolume); // Volume level is based on potentiometer value
      Serial.println(speakerVolume); // Comment this out
      isDeviceIdle = 0;

      if (isPlaying && mp3->isRunning()) mp3->stop();

      if (isTestActive == 1) file = new AudioFileSourceSD(returnRandomSoundFile(deerRabbitDir).c_str());
      mp3->begin(file, out);
      isTestActive = 0;
      mainMenu();
      playSoundTime = millis();
    }

    if (isLoadSongActive == 1 && isDeerSelected == 1)
    {
      speakerVolume = adjustVolume();
      out -> SetGain(speakerVolume); // Volume level is based on potentiometer value
      Serial.println(speakerVolume); // Comment this out
      isDeviceIdle = 0;

      if (isPlaying && mp3->isRunning()) mp3->stop();

      if (soundIndex == 0) file = new AudioFileSourceSD("/Deer_Sounds/1_Deer.mp3");
      if (soundIndex == 1) file = new AudioFileSourceSD("/Deer_Sounds/2_Deer.mp3");
      if (soundIndex == 2) file = new AudioFileSourceSD("/Deer_Sounds/3_Deer.mp3");
      if (soundIndex == 3) file = new AudioFileSourceSD("/Deer_Sounds/4_Deer.mp3");
      if (soundIndex == 4) file = new AudioFileSourceSD("/Deer_Sounds/5_Deer.mp3");
      if (soundIndex == 5) file = new AudioFileSourceSD("/Deer_Sounds/6_Deer.mp3");
      if (soundIndex == 6) file = new AudioFileSourceSD("/Deer_Sounds/7_Deer.mp3");
      if (soundIndex == 7) file = new AudioFileSourceSD("/Deer_Sounds/8_Deer.mp3");
      if (soundIndex == 8) file = new AudioFileSourceSD("/Deer_Sounds/9_Deer.mp3");
      if (soundIndex == 9) file = new AudioFileSourceSD("/Deer_Sounds/10_Deer.mp3");
      mp3->begin(file, out);
      isLoadSongActive = 0;
      isDeerSelected = 0;
      handleLoadSongMenu();
      playSoundTime = millis();
    }

    if (isLoadSongActive == 1 && isRabbitSelected == 1)
    {
      speakerVolume = adjustVolume();
      out -> SetGain(speakerVolume); // Volume level is based on potentiometer value
      Serial.println(speakerVolume); // Comment this out
      isDeviceIdle = 0;

      if (isPlaying && mp3->isRunning()) mp3->stop();

      if (soundIndex == 0) file = new AudioFileSourceSD("/Rabbit_Sounds/1_Rabbit.mp3");
      if (soundIndex == 1) file = new AudioFileSourceSD("/Rabbit_Sounds/2_Rabbit.mp3");
      if (soundIndex == 2) file = new AudioFileSourceSD("/Rabbit_Sounds/3_Rabbit.mp3");
      if (soundIndex == 3) file = new AudioFileSourceSD("/Rabbit_Sounds/4_Rabbit.mp3");
      if (soundIndex == 4) file = new AudioFileSourceSD("/Rabbit_Sounds/5_Rabbit.mp3");
      if (soundIndex == 5) file = new AudioFileSourceSD("/Rabbit_Sounds/6_Rabbit.mp3");
      if (soundIndex == 6) file = new AudioFileSourceSD("/Rabbit_Sounds/7_Rabbit.mp3");
      if (soundIndex == 7) file = new AudioFileSourceSD("/Rabbit_Sounds/8_Rabbit.mp3");
      if (soundIndex == 8) file = new AudioFileSourceSD("/Rabbit_Sounds/9_Rabbit.mp3");
      if (soundIndex == 9) file = new AudioFileSourceSD("/Rabbit_Sounds/10_Rabbit.mp3");
      mp3->begin(file, out);
      isLoadSongActive = 0;
      isRabbitSelected = 0;
      handleLoadSongMenu();
      playSoundTime = millis();
    }

    if (isPlaying && mp3->isRunning())
    {
      if (!mp3->loop())
      {
        mp3->stop();
        Serial.println("Stopped");
        clearMenu();
        isPlaying = 0;
        Timer_Setup_Enable();
      }
    }
  }
}

void runToneGenerator()
{
  if (isToneGeneratorActive)
  {
    speakerVolume = adjustVolume();
    out -> SetGain(speakerVolume);
    isDeviceIdle = 0;

    if (isPlaying && wav->isRunning()) wav->stop();
    // Parameters: (Seconds, Number of Channels, Hz, Bits/Sample)
    //if (isToneGeneratorActive == 1) toneGen = new AudioFileSourceFunction(8., 1, 38000, 16);
    if (isToneGeneratorActive == 1) toneGen = new AudioFileSourceFunction(1., 1, 35000, 16);
    toneGen -> addAudioGenerators(generateSineWave);
    wav->begin(toneGen, out);
    isToneGeneratorActive = 0;
  }

  if (isPlaying && wav->isRunning())
  {
    if (!wav->loop())
    {
      wav->stop();
      isPlaying = 0;
    }
  }
}

// Rotary Encoder Interrupt Service Routine (ISR)
IRAM_ATTR void readEncoderValues() {
  bool readCLK_PIN = digitalRead(CLK_PIN);
  bool readDT_PIN = digitalRead(DT_PIN);

  if ( (CLK_prevPosition == readCLK_PIN && DT_prevPosition == readDT_PIN) ) return;  // Tracks if there is no change since last activity

  // Same direction (alternating between 0 and 1 & 1 and 0 in one direction OR both 1 and 1 & 0 and 0 in the opposite direction)
  if (CLK_prevPosition == 1 && DT_prevPosition == 0 && readCLK_PIN == 0 && readDT_PIN == 1) CLK_Position -= 1;
  else if (CLK_prevPosition == 0 && DT_prevPosition == 1 && readCLK_PIN == 1 && readDT_PIN == 0) CLK_Position -= 1;
  else if (CLK_prevPosition == 0 && DT_prevPosition == 0 && readCLK_PIN == 1 && readDT_PIN == 1) CLK_Position += 1;
  else if (CLK_prevPosition == 1 && DT_prevPosition == 1 && readCLK_PIN == 0 && readDT_PIN == 0) CLK_Position += 1;

  // Checks if there is a change in direction
  else if (CLK_prevPosition == 1 && DT_prevPosition == 0 && readCLK_PIN == 0 && readDT_PIN == 0) CLK_Position += 1;
  else if (CLK_prevPosition == 0 && DT_prevPosition == 1 && readCLK_PIN == 1 && readDT_PIN == 1) CLK_Position += 1;
  else if (CLK_prevPosition == 0 && DT_prevPosition == 0 && readCLK_PIN == 1 && readDT_PIN == 0) CLK_Position -= 1;
  else if (CLK_prevPosition == 1 && DT_prevPosition == 1 && readCLK_PIN == 0 && readDT_PIN == 1) CLK_Position -= 1;

  // Update the previous encoder readings
  CLK_prevPosition = readCLK_PIN;
  DT_prevPosition = readDT_PIN;
}

void exitMenu()
{
  buttonState = digitalRead(ENC_BUTTON_PIN);    // Tracks the status of the encoder's push button
  lastEncoderStatus = 0;                        // Clear time for previous encoder's activity
  noInterrupts();
  CLK_Position = 0;                             // Clear the encoder's current position value
  interrupts();
}

// Display static menu on OLED
void staticMenu() {
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(20, 0);
  display.fillRoundRect(0, 0, 127, 8, 3, WHITE);
  display.drawRect(0, 0, 127, 64, WHITE);
  display.setTextColor(BLACK);
  display.println(menuTitle);
  display.setTextColor(WHITE);
  display.setCursor(0, 8);

  // Menu Options; Displays menu if it is not blank
  int i = 0;
  while (i < menuMax && menuOption[i] != "")
	{
    if (i == menuItemClicked) display.setTextColor(BLACK,WHITE); // Checks if item is selected
    else display.setTextColor(WHITE,BLACK);
    display.setCursor(10, 18 + (i*lineSpace1));
    display.print(menuOption[i]);
    i++;
  }

  // Highlight the menu item if not yet selected
    if (menuItemClicked == 100) {
      display.setCursor(2, 18 + (menuCount * lineSpace1));
      display.print(">");
    }
  display.display();          // Update the display
}

// Returns 1 if the button status has changed since last time
bool menuCheck() {

  if (digitalRead(ENC_BUTTON_PIN) == buttonState) return 0;    // Tracks if there is no change
  delay(40);
  if (digitalRead(ENC_BUTTON_PIN) == buttonState) return 0;    // Debounce encoder's push button
  if (millis() - reButtonTimer  < reButtonMinTime) return 0;    // Used for debouncing

  // Tracks if encoder's push button status has changed, then updates the timer
  buttonState = !buttonState;
  reButtonTimer = millis();
  if (serialDebug) Serial.println("Button Pressed!");

  // Handle OLED Menu when button is pressed
  // This logs the time when it was last pressed - not counting button release
  if (buttonState==LOW)
  {
    lastEncoderStatus = millis();
    if (menuItemClicked != 100 || menuTitle == "") return 1;    // If menu item is already highlighted/selected
    if (serialDebug) Serial.println("Menu '" + menuTitle + "' Item " + String(menuCount) + " Selected");
    menuItemClicked = menuCount;                                // Flags when menu item is selected
  }
  return 1;
}

void waitForButtonPress(int timeout)
{
  uint32_t tTimer = millis();
  // Waits for the button to be released after pressing
  while ( (digitalRead(ENC_BUTTON_PIN) == LOW) && (millis() - tTimer < timeout) )
  {
    delay(20);
  }
  // Reset Rotary Encoder Position Value
  noInterrupts();
  CLK_Position = 0;
  interrupts();

  // Waits for button to be pressed or the encoder to be turned
  while ( (digitalRead(ENC_BUTTON_PIN) == HIGH) && (CLK_Position == 0) && (millis() - tTimer < timeout) )
  {
    delay(20);
  }
  exitMenu();
}

void setMenu(byte inum, String iname)
{
  if (inum >= menuMax) return;    // Invalid number

  if (iname == "") {              // Clears the menu items
    for (int i; i < menuMax; i++)  menuOption[i] = "";
    menuCount = 0;                // The top menu item becomes highlighted
  } else {
    menuOption[inum] = iname;
    menuItemClicked = 100;        // Flag item selected as None
  }
}
// Handle Menu Item Selection
void menuItemSelection() {
    if (CLK_Position >= itemTrigger) {
      noInterrupts();
      CLK_Position = 0;
      interrupts();
      lastEncoderStatus = millis();                   // Tracks the time for the encoder's previous activity
      if (menuCount+1 < menuMax) menuCount++;         // Allows to navigate the menu if it has not reached the limit
      if (menuOption[menuCount] == "") menuCount--;   // Prevents from scrolling if the following menu item is empty
    }
    if (CLK_Position <= -itemTrigger) {
      noInterrupts();
        CLK_Position = 0;
      interrupts();
      lastEncoderStatus = millis();
      if (menuCount > 0) menuCount--;
    }
}
// enterValue - Function to input a value using the rotary encoder
// Format: Value Name, Initial Value, Step Size, Lower Limit, Upper Limit
// It will then return the specified value as an int
int enterValue(String title, int start, int stepSize, int low, int high)
{
  uint32_t tTimer = millis(); // Log the time when the function is called
  display.clearDisplay();

  if (title.length() > 8)
  {
    display.setTextSize(1);  // If the title is longer than 8 chars make the text smaller
  }
  else
  {
    display.setTextSize(2);
  }

  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.print(title);
  display.display(); // Updates the display
  int tvalue = start;
  while ( (digitalRead(ENC_BUTTON_PIN) == LOW) && (millis() - tTimer < (OLEDDisplayTimeout * 1000)) ) delay(5);    // Waits for when button is released
  tTimer = millis();
  while ( (digitalRead(ENC_BUTTON_PIN) == HIGH) && (millis() - tTimer < (OLEDDisplayTimeout * 1000)) ) {   // Tracks when the button is pressed and is still within the OLED timeout
    if (CLK_Position >= itemTrigger) { // CLK_Position is updated in the ISR
      tvalue -= stepSize;
      noInterrupts(); // Disables the ISR while the value is getting updated
        CLK_Position -= itemTrigger;
      interrupts();
      tTimer = millis();
    }
    else if (CLK_Position <= -itemTrigger) {
      tvalue += stepSize;
      noInterrupts();
        CLK_Position += itemTrigger;
      interrupts();
      tTimer = millis();
    }
      // Sets the lower and upper limits
      if (tvalue > high) tvalue = high;
      if (tvalue < low) tvalue = low;
    display.setTextSize(3);
    const int textPos = 20; // height of number on display
    display.fillRect(0, textPos, SCREEN_WIDTH, SCREEN_HEIGHT - textPos, BLACK);   // Clears the bottom half of the OLED
    display.setCursor(0, textPos);
    display.print(tvalue);

    // Shows a bar graph (to visualize value level) at the bottom of the OLED
    int tmag=map(tvalue, low, high, 0 ,SCREEN_WIDTH);
    display.fillRect(0, SCREEN_HEIGHT - 10, tmag, 10, WHITE);
    display.display(); // Updates the display for any changes
  }
  exitMenu();
  return tvalue;
}
// Function that allows to scroll through a list
// Can set maximum number of list -- as of now, it is 10
int chooseFromList(byte noOfElements, String listTitle, String list[]) {

  const byte noList = 10; // Sets the maximum number of items allowed in the list
  uint32_t tTimer = millis(); // Logs the time when function is called
  int highlightedItem = 0; // Shows which item is currently highlighted
  int xpos, ypos;

  // Display Title
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE, BLACK);
  display.setCursor(10, 0);
  display.print(listTitle);
  display.drawLine(0, lineSpace1, display.width(), lineSpace1, WHITE);

  // Scrolling through list
  while ( (digitalRead(ENC_BUTTON_PIN) == LOW) && (millis() - tTimer < (OLEDDisplayTimeout * 1000)) ) delay(5);
    tTimer = millis();

  while ( (digitalRead(ENC_BUTTON_PIN) == HIGH) && (millis() - tTimer < (OLEDDisplayTimeout * 1000)) ) {
    if (CLK_Position >= itemTrigger) // CLK_Position is updated in the ISR
    {
      noInterrupts();
      CLK_Position = 0;
      interrupts();
      highlightedItem++;
      tTimer = millis();
    }

    if (CLK_Position <= -itemTrigger)
    {
      noInterrupts();
      CLK_Position = 0;
      interrupts();
      highlightedItem--;
      tTimer = millis();
    }

    // Limit values
    if (highlightedItem > noOfElements - 1)
    {
      highlightedItem = noOfElements - 1;
    }

    if (highlightedItem < 0)
    {
      highlightedItem = 0;
    }

    // Displays the list by iterating through the elements
    for (int i=0; i < noOfElements; i++)
    {
      if (i < (noList/2))
      {
        xpos = 0;
        ypos = lineSpace1 * (i+1) + 7;
      }
      else
      {
        xpos = display.width() / 2;
        ypos = lineSpace1 * (i-((noList/2)-1)) + 7;
      }

      display.setCursor(xpos, ypos);

      if (i == highlightedItem)
      {
        display.setTextColor(BLACK,WHITE);
      }
      else
      {
        display.setTextColor(WHITE,BLACK);
      }
            display.print(list[i]);
    }

    display.display(); // Update display
    }

  // If it has exceeded the OLED timeout, then set selection to cancel (Back to Item 0)
  if (millis() - tTimer >= (OLEDDisplayTimeout * 1000)) highlightedItem = 0;
  exitMenu(); // Closes the current menu

  return highlightedItem;
}

// Main Menu Function
void mainMenu()
{
  menuTitle = "Sound Generator";  // Initialize Menu Title
  setMenu(0, "");                 // Clearing Menu Items
  setMenu(0, "Start Device");     // Start Device -> Goes to sleep and waits for motion
  setMenu(1, "Set Frequency");    // Change to Tone Generator -> Set Frequency
  setMenu(2, "Load Sound");       // Second Menu -> Deer/Rabbit -> List Sound Files
  setMenu(3, "Sub Menu");
}

void goBackMainMenu(String titleName, int numItemClicked)
{
  if (menuTitle == titleName && menuItemClicked == numItemClicked)
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked = 100;
    waitForButtonPress(20000);
    mainMenu(); // Go back to Main Menu
  }
}

// Main Menu Item 2
void loadSongMenu()
{
  menuTitle = "Sound File Menu";
  setMenu(0, "");
  setMenu(0, "Deer Cry Sounds");
  setMenu(1, "Rabbit Cry Sounds");
  setMenu(2, "Return to Main Menu");
}

// Main Menu: Item 3
void subMenu()
{
  menuTitle = "Sub Menu";
  setMenu(0, "");
  setMenu(0, "Turn off Menu"); // Exit out of menu
  setMenu(1, "Return to Main Menu");
}

void startDeviceMenu()
{
  menuTitle = "Start Device";
  setMenu(0, "");
  setMenu(0, "Run Test");
  setMenu(1, "Return to Main Menu");
}

void handleLoadSongMenu()
{
  if (menuTitle == "Sound Generator" && menuItemClicked==2)
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked=100;
    isPlaying = 0;
    waitForButtonPress(20000);
    loadSongMenu();
  }

  // Handle Sound File Menu
  if (menuTitle == "Sound File Menu" && menuItemClicked == 0) // If Deer was selected
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked = 100;
    waitForButtonPress(20000);
    isDeerSelected = 1;
    isRabbitSelected = 0;
    isLoadSongActive = 1;
    isPlaying = 1;

    String* deerSoundFiles = printSoundFiles(deerDir);

    int selectDeerFile = chooseFromList(10, "Deer Sound List", deerSoundFiles);
    Serial.println("Sound File: " + String(selectDeerFile) + " selected!");
    soundIndex = selectDeerFile;
    //waitForButtonPress(20000);
  }

  if (menuTitle == "Sound File Menu" && menuItemClicked == 1) // If Rabbit was selected
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked = 100;
    waitForButtonPress(20000);
    isDeerSelected = 0;
    isRabbitSelected = 1;
    isLoadSongActive = 1;
    isPlaying = 1;

    String* rabbitSoundFiles = printSoundFiles(rabbitDir);

    int selectRabbitFile = chooseFromList(10, "Rabbit Sound List", rabbitSoundFiles);
    soundIndex = selectRabbitFile;
    Serial.println("Sound File: " + String(selectRabbitFile) + " selected!");
    //waitForButtonPress(20000);
  }

  if (menuTitle == "Sound File Menu" && menuItemClicked == 2)
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked=100;
    mainMenu();
    waitForButtonPress(20000);
    isLoadSongActive = 0;
  }
}

int getSoundChoiceIndex(int soundIndex)
{
  return soundIndex;
}

void handleSubMenu()
{
  if (menuTitle == "Sound Generator" && menuItemClicked == 3)
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked = 100;
    isPlaying = 0;
    waitForButtonPress(20000);
    subMenu();
  }

  // Configuring Sub Menu
  if (menuTitle == "Sub Menu" && menuItemClicked==0)
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked=100;
    menuTitle = ""; // Turning Menu OFF
    display.clearDisplay();
    display.display();
  }

  // Return to Main Menu
  goBackMainMenu("Sub Menu", 1);
}

int returnFrequencyValue()
{
  // enterValue(String title, Start Value, Step Size, Lower Limit, Upper Limit);
  int frequencyValue = enterValue("Frequency Value (Hz):", 1000, 100, 0, 25000);

  return frequencyValue;
}

void handleStartDeviceMenu()
{
  if (menuTitle == "Sound Generator" && menuItemClicked == 0)
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked = 100;
    waitForButtonPress(20000);
    startDeviceMenu();
  }

  if (menuTitle == "Start Device" && menuItemClicked == 0)
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked = 100;
    turnOnLED();
    isTestActive = 1;
    isPlaying = 1;
    waitForButtonPress(20000);
  }

  goBackMainMenu("Start Device", 1);

}

void menuItemActions()
{
  if (menuItemClicked == 100) return; // Exit when no item has been selected

  // Main Menu Actions
  handleStartDeviceMenu();

  // Enter Frequency Value
  if (menuTitle == "Sound Generator" && menuItemClicked==1)
  {
    if (isPlaying && mp3->isRunning()) mp3->stop();
    if (isPlaying && wav->isRunning()) wav->stop();
    menuItemClicked=100;
    waitForButtonPress(20000);
    int frequencyValue = returnFrequencyValue();
    Serial.println("Menu: Value Set = " + String(frequencyValue)); // Optional print statement (for debugging)
    toneGeneratorValue = frequencyValue;
    isToneGeneratorActive = 1;
    isPlaying = 1;
  }

  handleLoadSongMenu();
  handleSubMenu();
}

void splashScreen()
{
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.setTextSize(1);
  display.print("CA Mountain Lion");
  display.setCursor(0, lineSpace1 * 2);
  display.print("Project Device");
  display.setCursor(0, lineSpace1 * 4);
  display.print("Sound Generator");
  display.display();
}

void AuthorFacultyScreen()
{
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.setTextSize(1);
  display.print("Project Contributors:");
  display.setCursor(0, lineSpace1 * 2);
  display.print("Dr. Winston Vickers");
  display.setCursor(0, lineSpace1 * 4);
  display.print("Aaron Nanas");
  display.setCursor(0, lineSpace1 * 6);
  display.print("Dr. Shahnam Mirzaei");
  display.display();
}

void OLED_Menu_Loop()
{
  if (millis() - runTime > 200)
  {
    runTime = millis();
    readEnableSwitch = digitalRead(SWITCH_ENABLE_PIN);

    if (menuTitle != "" && readEnableSwitch == 1) {   // Start if menu is active
      isMenuDisplayActive = 1;
      menuCheck();                                    // Checks if encoder button is pressed
      menuItemSelection();                            // Checks for change in highlighted items
      staticMenu();                                   // Displays the menu
      menuItemActions();                              // Processes item selection
    }

    else if (readEnableSwitch == 0)
    {
      isMenuDisplayActive = 0;
      menuItemClicked = 100;
      display.clearDisplay();
      display.display();
    }
  }
}

void activateSleepMode()
{
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_35, HIGH);
  Serial.println("Entering Deep Sleep Mode");
  esp_deep_sleep_start();
}

void enableActivateSleepMode()
{
  if (isSleepActivated == 1)
  {
    activateSleepMode();
  }
}

void enableBootSleepMode()
{
  if (isDeviceIdle == 1 && isMotionDetected == 0 && isMenuDisplayActive == 0 && isPlaying == 0)
  {
    clearMenu();
    activateSleepMode(); // Device goes to sleep after 10 seconds of inactivity after booting up
  }
}

float generateSineWave(const float time)
{
  hertzValue = float(toneGeneratorValue);
  float returnSineValue = sin(TWO_PI * hertzValue * time);
  returnSineValue = returnSineValue * fmod(time, 1.f);
  returnSineValue = returnSineValue * 0.5;

  return returnSineValue;
}

