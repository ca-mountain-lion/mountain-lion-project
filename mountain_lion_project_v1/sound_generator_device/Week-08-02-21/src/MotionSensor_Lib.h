#ifndef __MOTIONSENSOR_LIB_H__
#define __MOTIONSENSOR_LIB_H__

#include "Arduino.h"
#define SWITCH_ENABLE_PIN GPIO_NUM_12

const int sleepTimer = 2000;
const int MOTION_SENSOR_PIN = 35;
const int LED_PIN = 17;

extern volatile bool isMotionDetected;
extern volatile bool readEnableSwitch;
extern volatile bool isPlaying;
extern volatile bool isStarted;
extern volatile byte loadSoundFile;
extern volatile bool isMenuDisplayActive;
extern volatile bool isDeviceIdle;
extern volatile bool isLoadSongActive;
extern volatile bool isTestActive;
extern volatile bool isToneGeneratorActive;
extern volatile bool readButtonStatus;

void GPIO_Setup();
void turnOnLED();
void turnOffLED();
void blinkLED();
IRAM_ATTR void handleMotionSensor();

#endif
