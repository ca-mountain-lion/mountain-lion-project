#ifndef __OLED_ROTARYENC_LIB_H__
#define __OLED_ROTARYENC_LIB_H__

#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_I2CDevice.h>
#include "BabyLionBitmap.h"

extern bool serialDebug;
extern int itemTrigger;

// OLED Definitions
#define OLED_ADDR 0x3C
const int I2C_SDA = 9;
const int I2C_SCL = 10;
const int DT_PIN = 13;  // DT of Rotary Encoder
const int CLK_PIN = 14;  // CLK of Rotary Encoder
const int ENC_BUTTON_PIN = 15; // Encoder Push Button
//const int BUTTON_PIN = 16;

const int SCREEN_WIDTH = 128;                  // OLED display width, in pixels
const int SCREEN_HEIGHT = 64;                  // OLED display height, in pixels
const int OLED_RESET = -1;                     // Reset pin # (or -1 if sharing Arduino reset pin)
extern Adafruit_SSD1306 display;

// OLED Menu Variables
extern const byte menuMax;
extern const byte lineSpace1;
extern const byte lineSpace2;
extern String menuOption[];
extern byte menuCount;
extern String menuTitle;
extern byte menuItemClicked;
extern uint32_t lastEncoderStatus;
extern int OLEDDisplayTimeout;

// Rotary Encoder Variables
extern volatile int16_t CLK_Position;
extern volatile bool CLK_prevPosition;
extern volatile bool DT_prevPosition;
extern bool buttonState;
extern uint32_t reButtonTimer;
extern int reButtonMinTime;

extern volatile bool isDeerSelected;
extern volatile bool isRabbitSelected;

// Variables for sleep timer
const int sleepTimeInSeconds = 5;   // Amount of time that should pass before going to sleep after device is triggered
const int bootSleepTimeInSeconds = 15;   // Amount of time that should pass before going to sleep after device has booted up
extern hw_timer_t * timer;          // This creates a hardware timer
extern hw_timer_t * BootUpTimer;
extern volatile bool isSleepActivated;
extern volatile bool readStopButtonStatus;
extern volatile bool ledState;

// Load Song Variables
extern int soundIndex;

extern float hertzValue;

// Function Prototypes

// Setup Functions
void RotaryEncoder_Setup();
void OLED_Screen_Setup();
void displayBabyLion();
void splashScreen();
void AuthorFacultyScreen();

// Menu Functions
IRAM_ATTR void readEncoderValues();
void exitMenu();
void staticMenu();
bool menuCheck();
void waitForButtonPress(int timeout);
void setMenu(byte inum, String iname);
void menuItemSelection();
int enterValue(String title, int start, int stepSize, int low, int high);
int chooseFromList(byte noOfElements, String listTitle, String list[]);
void mainMenu();
void goBackMainMenu(String titleName, int numItemClicked);
void startDeviceMenu();
void loadSongMenu();
int getSoundChoiceIndex(int soundIndex);
void subMenu();
//void testPeripheralMenu();
void handleStartDeviceMenu();
void handleLoadSongMenu();
//void handleTestPeripheralsMenu();
void handleSubMenu();
int returnFrequencyValue();
void menuItemActions();
void clearMenu();

// Main Loop Functions
void Play_Sound_Loop();
void OLED_Menu_Loop();

// Timer Functions
void IRAM_ATTR onTimer();
void IRAM_ATTR bootOnTimer();
void Timer_Setup_Enable();
void Timer_Setup_Init();

// Sleep Functions
void activateSleepMode();
void enableActivateSleepMode();
void enableBootSleepMode();
float generateSineWave(const float time);
void runToneGenerator();
// IRAM_ATTR void ledButton();

#endif
