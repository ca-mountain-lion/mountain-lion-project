#ifndef __SD_SPEAKER_LIB_H__
#define __SD_SPEAKER_LIB_H__

#include "AudioGeneratorMP3.h"
#include "AudioOutputI2S.h"
#include "AudioFileSourceSD.h"
#include "AudioGeneratorWAV.h"
#include "AudioFileSourceFunction.h"
#include "driver/i2s.h"
#include <SD.h>
#include "HTTPClient.h"
#include "WiFi.h"
#include "SPIFFS.h"

//SD Card Pin Definitions
const int SD_CS = 5;
const int SPI_MOSI = 23;
const int SPI_MISO = 19;
const int SPI_SCK = 18;

// Potentiometer Pin Definition
const int POT_VOLUME_PIN = 34;

extern AudioGeneratorMP3 *mp3;
extern AudioFileSourceSD *file;
extern AudioOutputI2S *out;

extern File SD_Card_Directory;

// Tone Generator Initialization
extern int toneGeneratorValue;
extern AudioGeneratorWAV *wav;
extern AudioFileSourceFunction *toneGen;

// Volume Variables
extern float potValue;
extern float speakerVolume;

// SD Card and Sound File Variables
extern int numSoundFiles;
extern int numAllSoundFiles;
extern int dirStringLen;
extern String *soundList;
extern String *newSoundList;
extern String directoryName;
extern String deerDir;
extern String rabbitDir;
extern String deerRabbitDir;
extern String randomSoundFile;

// Run Time Variables for Main Loop Functions
extern uint runTime;
extern uint playSoundTime;

void SD_Speaker_Setup();
float volumeMap(float x, float in_min, float in_max, float out_min, float out_max);
float adjustVolume();
int getStringLen(String directoryName);
int returnNumSoundFiles(File folderName);
String* ListSoundFiles(File folderName, String directoryName);
String* printSoundFiles(String directoryName);
String returnRandomSoundFile(String directoryName);

#endif
