/*
    This contains all the pin definitions for the LED indicator,
    the motion sensor, and the push button.

    It will also contain ISRs for the push button and the motion sensor,
    and functions for ESP32's sleep feature.
 */
#include "MotionSensor_Lib.h"

volatile bool isMotionDetected = 0;
volatile bool readEnableSwitch;
volatile bool isPlaying;
volatile bool isStarted = 0;
volatile byte loadSoundFile = 0;
volatile bool isMenuDisplayActive;
volatile bool isDeviceIdle = 0;
volatile bool isLoadSongActive;
volatile bool isTestActive;
volatile bool isToneGeneratorActive;
volatile bool readButtonStatus;

void GPIO_Setup()
{
  pinMode(MOTION_SENSOR_PIN, INPUT_PULLUP);
  pinMode(SWITCH_ENABLE_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(MOTION_SENSOR_PIN), handleMotionSensor, RISING);
}

void turnOnLED()
{
  digitalWrite(LED_PIN, HIGH);
}

void turnOffLED()
{
  digitalWrite(LED_PIN, LOW);
}

void blinkLED()
{
  turnOffLED();
  delay(200);
  turnOnLED();
  delay(200);
  turnOffLED();
  delay(200);
  turnOnLED();
  delay(200);
  turnOffLED();
}

IRAM_ATTR void handleMotionSensor()
{
  unsigned long motionInterruptTime = millis();
  static unsigned long lastMotionInterruptTime = 0;
  isMotionDetected = digitalRead(MOTION_SENSOR_PIN);
  readEnableSwitch = digitalRead(SWITCH_ENABLE_PIN);

  if (motionInterruptTime - lastMotionInterruptTime > 200)
  {
    if (isMotionDetected == 1 && isMenuDisplayActive == 0)
    {
      Serial.println("Motion Detected!");
      isMotionDetected = 0;
      loadSoundFile = 1;
      isPlaying = 1;
      isDeviceIdle = 0;
      isLoadSongActive = 0;
      isTestActive = 0;
      digitalWrite(LED_PIN, HIGH);
    }
    else if (isMotionDetected == 1 && isMenuDisplayActive == 1)
    {
      isMotionDetected = 0;
    }
  }
  lastMotionInterruptTime = motionInterruptTime;
  Serial.println(isMotionDetected);
}
