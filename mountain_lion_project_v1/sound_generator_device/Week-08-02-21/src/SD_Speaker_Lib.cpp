#include "SD_Speaker_Lib.h"

AudioGeneratorMP3 *mp3;
AudioFileSourceSD *file;
AudioOutputI2S *out;

File SD_Card_Directory;

// Tone Generator Initialization
int toneGeneratorValue;
AudioGeneratorWAV *wav;
AudioFileSourceFunction *toneGen;

uint runTime;
uint playSoundTime;
float potValue = 0.0;  // Tracks potentiometer value for volume control
float speakerVolume;   // Tracks volume from mapping function

int numSoundFiles; // Variable to keep track of the number of sound files in a directory
int numAllSoundFiles; // Variable to keep track of the number of all sound files (should be 20)
int dirStringLen;
String *soundList;
String *newSoundList;
String directoryName;
String deerDir = "/Deer_Sounds";
String rabbitDir = "/Rabbit_Sounds";
String deerRabbitDir = "/DeerRabbit_Sounds";
String randomSoundFile;

void SD_Speaker_Setup()
{
  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, HIGH);
  SPI.begin(SPI_SCK, SPI_MISO, SPI_MOSI);

  out = new AudioOutputI2S();
  mp3 = new AudioGeneratorMP3();
  wav = new AudioGeneratorWAV();

  Serial.print("Initializing SD card...");
  SD.begin(SD_CS);
  if (!SD.begin(SD_CS))
  {
    Serial.println("initialization Failed!");
    return;
  }
  Serial.println("SD Card Initialization Done!");
  delay(100);
}

// Function to map potentiometer values to volume (from 0 to a maximum of 4)
float volumeMap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float adjustVolume()
{
  potValue = analogRead(POT_VOLUME_PIN);
  speakerVolume = volumeMap(potValue, 0, 4025, 0, 3.9);

  return speakerVolume;
}

int getStringLen(String directoryName) {
  dirStringLen = directoryName.length() + 2;
  // Uncomment for debugging
  //Serial.println(dirStringLen);
  return dirStringLen;
}

int returnNumSoundFiles(File folderName)
{
  numSoundFiles = 0;

  while (true)
  {
    File entry =  folderName.openNextFile();
    if (!entry) {
      folderName.rewindDirectory();
      break;
    } else {
      numSoundFiles++;
    }
    entry.close();
  }
  return numSoundFiles;
}

String* ListSoundFiles(File folderName, String directoryName)
{
    numSoundFiles = returnNumSoundFiles(folderName);
    Serial.print("Number of Sound Files in Directory: ");
    Serial.println(numSoundFiles);
    if (directoryName == deerRabbitDir)
    {
      numAllSoundFiles = numSoundFiles;
    }

    soundList = new String[numSoundFiles];
    newSoundList = new String[numSoundFiles];

    for (int i = 0; i < numSoundFiles; i++)
    {
      File entry = folderName.openNextFile();
      soundList[i] = entry.name();
      entry.close();
      Serial.println(soundList[i]);
    }

    if (directoryName == deerDir || directoryName == rabbitDir)
    {
      for (int i = 0; i < numSoundFiles; i++)
      {
        if (i == numSoundFiles - 1)
        {
          newSoundList[numSoundFiles - 1] = String(String(i+1) + soundList[0].substring(getStringLen(directoryName) + 1));
          if (directoryName == deerDir) newSoundList[numSoundFiles - 1].remove(7, 10);
          else if (directoryName == rabbitDir) newSoundList[numSoundFiles - 1].remove(9, 12);
        }
        else
        {
          newSoundList[i] = String(String(i+1) + soundList[i + 1].substring(getStringLen(directoryName)));
          if (directoryName == deerDir) newSoundList[i].remove(6, 10);
          else if (directoryName == rabbitDir) newSoundList[i].remove(8, 12);
        }
      }
    }
    else if (directoryName == deerRabbitDir)
    {
      for (int i = 0; i < numSoundFiles; i++)
      {
        newSoundList[i] = soundList[i];
      }
    }

    return newSoundList;
}

String* printSoundFiles(String directoryName)
{
  SD_Card_Directory = SD.open(directoryName);
  Serial.println("Listing Directory's Sounds");
  newSoundList = ListSoundFiles(SD_Card_Directory, directoryName);

  return newSoundList;
}

/*
    This function will open the specified directory in the
    SD card, and then return the number of files located
    inside that directory.
    The sound files will get stored in an array, and then
    the function will return a random string from that array
    This return value gets used in the Play_Sound_Loop function
 */

String returnRandomSoundFile(String directoryName)
{
  int soundArrayIndex;
  SD_Card_Directory = SD.open(directoryName);
  newSoundList = ListSoundFiles(SD_Card_Directory, directoryName);
  soundArrayIndex = random(numAllSoundFiles);
  randomSoundFile = newSoundList[soundArrayIndex];

  return randomSoundFile;
}
