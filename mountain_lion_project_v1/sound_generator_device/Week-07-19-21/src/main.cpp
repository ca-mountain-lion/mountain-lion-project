/*
  Description: This is the main.cpp file for the Sound Generator project.
  It will wait when motion is detected to play either a rabbit or deer
  distress sound file. Currently, the sound files are in mp3 format.

  ESP32 Pin Definitions:
    const int SD_CS = 5;
    const int I2C_SDA = 9;
    const int I2C_SCL = 10;
    #define SWITCH_ENABLE_PIN GPIO_NUM_12
    const int DT_PIN = 13;  // DT of Rotary Encoder
    const int CLK_PIN = 14;  // CLK of Rotary Encoder
    const int ENC_BUTTON_PIN = 15; // Encoder Push Button
    const int SPI_SCK = 18;
    const int SPI_MISO = 19;
    const int BUTTON_PIN = 22;
    const int SPI_MOSI = 23;
    const int I2S_LRC = 25;
    const int I2S_BCLK = 26;
    const int I2S_DOUT = 27;
    const int MOTION_SENSOR_PIN = 35;

  To-do:
    -- Add motion sensor as another external wake-up source
    -- Fix Tone Generator feature
    -- Initialize device to be in deep-sleep
    -- Add another switch for menu control?
      -- This way, the menu doesn't have to be active when switch enable is active (less power consumption)
    -- Add battery level indicator
 */
#include "OLED_RotaryEnc_Lib.h"
#include "SD_Speaker_Lib.h"
#define SWITCH_ENABLE_PIN GPIO_NUM_12

const int sleepTimer = 2000;
const int MOTION_SENSOR_PIN = 35;
const int BUTTON_PIN = 22;
const int LED_PIN = 17;

volatile bool isMotionDetected;
volatile bool readEnableSwitch;

IRAM_ATTR void testButton()
{
  Serial.println("Button is working!");
}

IRAM_ATTR void handleMotionSensor()
{
  unsigned long motionInterruptTime = millis();
  unsigned long lastMotionInterruptTime = 0;
  isMotionDetected = digitalRead(MOTION_SENSOR_PIN);
  readEnableSwitch = digitalRead(SWITCH_ENABLE_PIN);

  if (motionInterruptTime - lastMotionInterruptTime > 200)
  {
    if ((isMotionDetected == 1) && (readEnableSwitch == 1))
    {
      Serial.println("Motion Detected!");
      isPlaying = 1;
      isMotionDetected = 0;
      digitalWrite(LED_PIN, HIGH);
    }
    else if ((isMotionDetected == 1) && (readEnableSwitch == 0))
    {
      Serial.println("Switch not enabled");
      isPlaying = 0;
      isMotionDetected = 0;
      digitalWrite(LED_PIN, LOW);
    }
  }
  lastMotionInterruptTime = motionInterruptTime;
  Serial.println(isMotionDetected);
}

void goToSleep()
{
  //unsigned long sleepTimer = millis();
  esp_sleep_enable_ext0_wakeup(SWITCH_ENABLE_PIN, HIGH);
  delay(sleepTimer);
  Serial.println("Entering Deep Sleep Mode");
  esp_deep_sleep_start();

}

void setup() {
  Serial.begin(115200);
  Serial.println("Starting Menu");
  pinMode(MOTION_SENSOR_PIN, INPUT_PULLUP);
  pinMode(SWITCH_ENABLE_PIN, INPUT_PULLUP);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(LED_PIN, OUTPUT);

  WiFi.mode(WIFI_OFF);
  btStop();

  RotaryEncoder_Setup();
	Speaker_SD_Setup();
  OLED_Screen_Setup();
  touch_pad_intr_disable();
  attachInterrupt(digitalPinToInterrupt(MOTION_SENSOR_PIN), handleMotionSensor, RISING);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), testButton, FALLING);

  splashScreen(); // Initial Splash Screen Display
  delay(1000); // Displays splash screen for 2.5 seconds

  mainMenu(); // Starts the menu
}

void loop() {
		audio.loop();

    if (millis() - run_time > 100)
    {
      run_time = millis();
      readEnableSwitch = digitalRead(SWITCH_ENABLE_PIN);
      // speakerVolume = adjustVolume();
      // audio.setVolume(speakerVolume);
      if (menuTitle != "" && readEnableSwitch == 1) { // Start if menu is active
        menuCheck(); // Checks if encoder button is pressed
        menuItemSelection(); // Checks for change in highlighted items
        staticMenu(); // Displays the menu
        menuItemActions(); // Processes item selection
      }
      else if (readEnableSwitch == 0)
      {
        Serial.println("Switch OFF");
        menuItemClicked=100;
        menuTitle = ""; // Turning Menu OFF
        display.clearDisplay();
        display.display();
        goToSleep();
      }
    }

    playSong();

}
