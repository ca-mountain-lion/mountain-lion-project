/*
    Interfaces OLED menu system with a rotary encoder.
    The CLK and DT pins of the rotary encoder are used to
    navigate through the menu, while its push button is
    used to select the item.

    Based on: https://github.com/alanesq/BasicOLEDMenu

 */

#include "OLED_RotaryEnc_Lib.h"
#include "SD_Speaker_Lib.h"

bool serialDebug = 1; // This enables debug on Serial Port (Optional)
int itemTrigger = 2;  // Rotary Encoder Counts Per Click

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// OLED Menu Variables
const byte menuMax = 5;                   // Sets the maximum number of menu items
const byte lineSpace1 = 9;                // Line spacing (6 lines)
const byte lineSpace2 = 16;               // Line spacing (4 lines)
String menuOption[menuMax];               // Displays the available menu options
byte menuCount = 0;                       // Tracks which menu option is highlighted
String menuTitle = "";                    // Current menu ID number (if blank string, then none)
byte menuItemClicked = 100;               // Menu item has been clicked flag (100 = None)
uint32_t lastEncoderStatus = 0;           // Tracks the last time the rotary encoder had activity
int OLEDDisplayTimeout = 10;              // OLED menu display timeout (in seconds)

// Rotary Encoder Variables
volatile int16_t CLK_Position = 0;        // Tracks the current value of the CLK pin (which gets updated in the ISR)
volatile bool CLK_prevPosition = 0;       // Tracks previous position (to see if it has changed -- also for debouncing)
volatile bool DT_prevPosition = 0;        // Tracks previous position (to see if it has changed -- also for debouncing)
bool buttonState = 0;                     // Reads state of the encoder's push button
uint32_t reButtonTimer = millis();        // Tracks the last time the encoder's push button had activity
int reButtonMinTime = 500;                // Minimum milliseconds between allowed button status changes
volatile bool isPlaying;

int potValue = 0;                         // Tracks potentiometer value for volume control
int speakerVolume;                        // Tracks volume from mapping function

// Function to map potentiometer values to volume (from 0 to a maximum of 21)
int volumeMap(int x, int in_min, int in_max, int out_min, int out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

int adjustVolume()
{
  potValue = analogRead(POT_VOLUME_PIN);
  speakerVolume = volumeMap(potValue, 0, 4025, 0, 21);

  return speakerVolume;
}

void playSong()
{
  if (millis() - button_time > 200)
  {
    if (isPlaying == 1)
    {
      isPlaying = 0;
      Serial.println("Playing song");
      if (file_index < file_num - 1)
      {
        file_index++;
      }
      else
      {
        file_index = 0;
      }
      speakerVolume = adjustVolume();
      audio.setVolume(speakerVolume);
      open_new_song(file_list[file_index]);
      button_time = millis();
    }
  }
}

// Rotary Encoder Interrupt Service Routine (ISR)
IRAM_ATTR void readEncoderValues() {
  bool readCLK_PIN = digitalRead(CLK_PIN);
  bool readDT_PIN = digitalRead(DT_PIN);

  if ( (CLK_prevPosition == readCLK_PIN && DT_prevPosition == readDT_PIN) ) return;  // Tracks if there is no change since last activity

  // same direction (alternating between 0,1 and 1,0 in one direction or 1,1 and 0,0 in the other direction)
  if (CLK_prevPosition == 1 && DT_prevPosition == 0 && readCLK_PIN == 0 && readDT_PIN == 1) CLK_Position -= 1;
  else if (CLK_prevPosition == 0 && DT_prevPosition == 1 && readCLK_PIN == 1 && readDT_PIN == 0) CLK_Position -= 1;
  else if (CLK_prevPosition == 0 && DT_prevPosition == 0 && readCLK_PIN == 1 && readDT_PIN == 1) CLK_Position += 1;
  else if (CLK_prevPosition == 1 && DT_prevPosition == 1 && readCLK_PIN == 0 && readDT_PIN == 0) CLK_Position += 1;

  // change of direction
  else if (CLK_prevPosition == 1 && DT_prevPosition == 0 && readCLK_PIN == 0 && readDT_PIN == 0) CLK_Position += 1;
  else if (CLK_prevPosition == 0 && DT_prevPosition == 1 && readCLK_PIN == 1 && readDT_PIN == 1) CLK_Position += 1;
  else if (CLK_prevPosition == 0 && DT_prevPosition == 0 && readCLK_PIN == 1 && readDT_PIN == 0) CLK_Position -= 1;
  else if (CLK_prevPosition == 1 && DT_prevPosition == 1 && readCLK_PIN == 0 && readDT_PIN == 1) CLK_Position -= 1;

  // Update previous readings
  CLK_prevPosition = readCLK_PIN;
  DT_prevPosition = readDT_PIN;
}

void exitMenu()
{
  buttonState = digitalRead(ENC_BUTTON_PIN);    // Tracks the status of the encoder's push button
  lastEncoderStatus = 0;                        // Clear time for previous encoder's activity
  noInterrupts();
  CLK_Position = 0;                              // Clear the encoder's current position value
  interrupts();
}

// bool confirmActionRequired() {
//   display.clearDisplay();
//   display.setTextSize(2);
//   display.setTextColor(WHITE,BLACK);
//   display.setCursor(0, lineSpace2 * 0);
//   display.print("HOLD THE");
//   display.setCursor(0, lineSpace2 * 1);
//   display.print("BUTTON TO");
//   display.setCursor(0, lineSpace2 * 2);
//   display.print("CONFIRM ACTION");
//   display.display();
//
//   delay(2000);
//   display.clearDisplay();
//   display.display();
//
//   exitMenu(); // Close Menu
//
//   if (digitalRead(ENC_BUTTON_PIN) == LOW) return 1;        // Tracks if button is still pressed
//
//   return 0;
// }

// Display static menu on OLED
void staticMenu() {
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(20, 0);
  display.fillRoundRect(0, 0, 127, 8, 3, WHITE);
  display.drawRect(0, 0, 127, 64, WHITE);
  display.setTextColor(BLACK);
  display.println(menuTitle);
  display.setTextColor(WHITE);
  display.setCursor(0, 8);

  // Menu Options; Displays menu if it is not blank
  int i = 0;
  while (i < menuMax && menuOption[i] != "")
	{
    if (i == menuItemClicked) display.setTextColor(BLACK,WHITE); // Checks if item is selected
    else display.setTextColor(WHITE,BLACK);
    display.setCursor(10, 18 + (i*lineSpace1));
    display.print(menuOption[i]);
    i++;
  }

  // Highlight the menu item if not yet selected
    if (menuItemClicked == 100) {
      display.setCursor(2, 18 + (menuCount * lineSpace1));
      display.print(">");
    }
  display.display();          // update display
}

// Returns 1 if the button status has changed since last time
bool menuCheck() {

  if (digitalRead(ENC_BUTTON_PIN) == buttonState) return 0;    // Tracks if there is no change
  delay(40);
  if (digitalRead(ENC_BUTTON_PIN) == buttonState) return 0;    // Debounce encoder's push button
  if (millis() - reButtonTimer  < reButtonMinTime) return 0;    // Used for debouncing

  // Tracks if encoder's push button status has changed, then updates the timer
  buttonState = !buttonState;
  reButtonTimer = millis();
  if (serialDebug) Serial.println("Button Pressed!");

  // Handle OLED Menu when button is pressed
  // This logs the time when it was last pressed - not counting button release
  if (buttonState==LOW)
  {
    lastEncoderStatus = millis();
    if (menuItemClicked != 100 || menuTitle == "") return 1;    // If menu item is already highlighted/selected
    if (serialDebug) Serial.println("menu '" + menuTitle + "' item " + String(menuCount) + " selected");
    menuItemClicked = menuCount;                                // Flags when menu item is selected
  }

  return 1;
}

void waitForButtonPress(int timeout)
{
  uint32_t tTimer = millis();
  // Waits for the button to be released after pressing
  while ( (digitalRead(ENC_BUTTON_PIN) == LOW) && (millis() - tTimer < timeout) )
  {
    delay(20);
  }

  // Reset Rotary Encoder Position Value
  noInterrupts();
  CLK_Position = 0;
  interrupts();

  // Waits for button to be pressed or the encoder to be turned
  while ( (digitalRead(ENC_BUTTON_PIN) == HIGH) && (CLK_Position == 0) && (millis() - tTimer < timeout) )
  {
    delay(20);
  }
  exitMenu();
}

void setMenu(byte inum, String iname)
{
  if (inum >= menuMax) return;    // invalid number

  if (iname == "") {              // clear all menu items
    for (int i; i < menuMax; i++)  menuOption[i] = "";
    menuCount = 0;                // move highlight to top menu item
  } else {
    menuOption[inum] = iname;
    menuItemClicked = 100;        // set item selected flag as none
  }
}

// Handle Menu Item Selection
void menuItemSelection() {
    if (CLK_Position >= itemTrigger) {
      noInterrupts();
      CLK_Position = 0;
      interrupts();
      lastEncoderStatus = millis();                            // log time last activity seen
      if (menuCount+1 < menuMax) menuCount++;               // if not past max menu items move
      if (menuOption[menuCount] == "") menuCount--;         // if menu item is blank move back
    }
    if (CLK_Position <= -itemTrigger) {
      noInterrupts();
        CLK_Position = 0;
      interrupts();
      lastEncoderStatus = millis();                            // log time last activity seen
      if (menuCount > 0) menuCount--;
    }
}

// Function to input a value using the rotary encoder
// Format: Value Name, Initial Value, Step Size, Lower Limit, Upper Limit
// It will then return the specified value as an int
int enterValue(String title, int start, int stepSize, int low, int high)
{
  uint32_t tTimer = millis(); // Log the time when the function is called
  display.clearDisplay();

  if (title.length() > 8)
  {
    display.setTextSize(1);  // if title is longer than 8 chars make text smaller
  }
  else
  {
    display.setTextSize(2);
  }

  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.print(title);
  display.display(); // Updates the display
  int tvalue = start;
  while ( (digitalRead(ENC_BUTTON_PIN) == LOW) && (millis() - tTimer < (OLEDDisplayTimeout * 1000)) ) delay(5);    // Waits for when button is released
  tTimer = millis();
  while ( (digitalRead(ENC_BUTTON_PIN) == HIGH) && (millis() - tTimer < (OLEDDisplayTimeout * 1000)) ) {   // Tracks when the button is pressed and is still within the OLED timeout
    if (CLK_Position >= itemTrigger) { // CLK_Position is updated in the ISR
      tvalue -= stepSize;
      noInterrupts(); // Disables the ISR while the value is getting updated
        CLK_Position -= itemTrigger;
      interrupts();
      tTimer = millis();
    }
    else if (CLK_Position <= -itemTrigger) {
      tvalue += stepSize;
      noInterrupts();
        CLK_Position += itemTrigger;
      interrupts();
      tTimer = millis();
    }
      // Sets the lower and upper limits
      if (tvalue > high) tvalue = high;
      if (tvalue < low) tvalue = low;
    display.setTextSize(3);
    const int textPos = 20; // height of number on display
    display.fillRect(0, textPos, SCREEN_WIDTH, SCREEN_HEIGHT - textPos, BLACK);   // Clears the bottom half of the OLED
    display.setCursor(0, textPos);
    display.print(tvalue);

    // bar graph at bottom of display
    int tmag=map(tvalue, low, high, 0 ,SCREEN_WIDTH);
    display.fillRect(0, SCREEN_HEIGHT - 10, tmag, 10, WHITE);
    display.display(); // Updates the display for any changes
  }
  exitMenu();
  return tvalue;
}
// Function that allows to scroll through a list
// Can set maximum number of list -- as of now, it is 10
int chooseFromList(byte noOfElements, String listTitle, String list[]) {

  const byte noList = 10; // Sets the maximum number of items allowed in the list
  uint32_t tTimer = millis(); // Logs the time when function is called
  int highlightedItem = 0; // Shows which item is currently highlighted
  int xpos, ypos;

  // Display Title
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.setCursor(10, 0);
  display.print(listTitle);
  display.drawLine(0, lineSpace1, display.width(), lineSpace1, WHITE);

  // Scrolling through list
  while ( (digitalRead(ENC_BUTTON_PIN) == LOW) && (millis() - tTimer < (OLEDDisplayTimeout * 1000)) ) delay(5);
    tTimer = millis();

  while ( (digitalRead(ENC_BUTTON_PIN) == HIGH) && (millis() - tTimer < (OLEDDisplayTimeout * 1000)) ) {
    if (CLK_Position >= itemTrigger) // CLK_Position is updated in the ISR
    {
      noInterrupts();
      CLK_Position = 0;
      interrupts();
      highlightedItem++;
      tTimer = millis();
    }

    if (CLK_Position <= -itemTrigger)
    {
      noInterrupts();
      CLK_Position = 0;
      interrupts();
      highlightedItem--;
      tTimer = millis();
    }

    // Limit values
    if (highlightedItem > noOfElements - 1)
    {
      highlightedItem = noOfElements - 1;
    }

    if (highlightedItem < 0)
    {
      highlightedItem = 0;
    }

    // Displays the list by iterating through the elements
    for (int i=0; i < noOfElements; i++)
    {
      if (i < (noList/2))
      {
        xpos = 0;
        ypos = lineSpace1 * (i+1) + 7;
      }
      else
      {
        xpos = display.width() / 2;
        ypos = lineSpace1 * (i-((noList/2)-1)) + 7;
      }

      display.setCursor(xpos, ypos);

      if (i == highlightedItem)
      {
        display.setTextColor(BLACK,WHITE);
      }
      else
      {
        display.setTextColor(WHITE,BLACK);
      }
            display.print(list[i]);
    }

    display.display(); // Update display
    }

  // If it has exceeded the OLED timeout, then set selection to cancel (Back to Item 0)
  if (millis() - tTimer >= (OLEDDisplayTimeout * 1000)) highlightedItem = 0;
  exitMenu(); // Closes the current menu

  return highlightedItem;
}

// Main Menu Function
void mainMenu()
{
  menuTitle = "Sound Generator";  // Initialize Menu Title
  setMenu(0, "");                 // Clearing Menu Items
  setMenu(0, "Start Device");     // Start Device -> Goes to sleep and waits for motion
  setMenu(1, "Set Frequency");    // Change to Tone Generator -> Set Frequency
  setMenu(2, "Load Song");        // Second Menu -> Deer/Rabbit -> List Sound Files
  setMenu(3, "Test Peripherals"); // Second Menu -> Test Peripherals
  setMenu(4, "Sub Menu");
}

void goBackMainMenu(String titleName, int numItemClicked)
{
  if (menuTitle == titleName && menuItemClicked == numItemClicked)
  {
    menuItemClicked = 100;
    waitForButtonPress(20000);
    mainMenu(); // Go back to Main Menu
  }
}

// Main Menu Item 2
void loadSongMenu()
{
  menuTitle = "Sound File Menu";
  setMenu(0, "");
  setMenu(0, "Deer Cry Sounds");
  setMenu(1, "Rabbit Cry Sounds");
  setMenu(2, "Return to Main Menu");
}

// Main Menu: Item 3
void subMenu() {
  menuTitle = "Sub Menu";
  setMenu(0, "");
  setMenu(0, "Turn off Menu"); // Exit
  setMenu(1, "Return to Main Menu");
  //setMenu(2, "Settings");
}
// Main Menu: Item 4
void testPeripheralMenu()
{
  menuTitle = "Testing Peripherals";
  setMenu(0, "");
  setMenu(0, "Test Speaker");
  setMenu(1, "Test Motion Sensor");
  setMenu(2, "Test Buzzer");
  setMenu(3, "Return to Main Menu");
}

void handleLoadSongMenu()
{
  if (menuTitle == "Sound Generator" && menuItemClicked==2)
  {
    menuItemClicked=100;
    isPlaying = 0;
    waitForButtonPress(20000);
    loadSongMenu();
  }

  // Handle Sound File Menu
  if (menuTitle == "Sound File Menu" && menuItemClicked == 0) // If Deer was selected
  {
    menuItemClicked = 100;
    waitForButtonPress(20000);
    String deerSoundFiles[] = {"Deer 1", "Deer 2", "Deer 3", "Deer 4", "Deer 5",
                                "Deer 6", "Deer 7", "Deer 8", "Deer 9", "Deer 10"};

    int selectDeerFile = chooseFromList(10, "Deer List", deerSoundFiles);
    Serial.println("Sound File: " + String(selectDeerFile) + " selected!");
    //waitForButtonPress(20000);
  }

  if (menuTitle == "Sound File Menu" && menuItemClicked == 1) // If Rabbit was selected
  {
    menuItemClicked = 100;
    waitForButtonPress(20000);
    String rabbitSoundFiles[] = {"Rabbit 1", "Rabbit 2", "Rabbit 3", "Rabbit 4", "Rabbit 5",
                                 "Rabbit 6", "Rabbit 7", "Rabbit 8", "Rabbit 9", "Rabbit 10"};

    int selectRabbitFile = chooseFromList(10, "Deer List", rabbitSoundFiles);
    Serial.println("Sound File: " + String(selectRabbitFile) + " selected!");
    //waitForButtonPress(20000);
  }

  if (menuTitle == "Sound File Menu" && menuItemClicked == 2)
  {
    menuItemClicked=100;
    mainMenu();
    waitForButtonPress(20000);
  }
}

void handleTestPeripheralsMenu()
{
  // Test Peripherals Menu
  if (menuTitle == "Sound Generator" && menuItemClicked == 3)
  {
    menuItemClicked=100;
    isPlaying = 0;
    waitForButtonPress(20000);
    testPeripheralMenu(); // Title: "Testing Peripherals"
  }

  goBackMainMenu("Testing Peripherals", 3);

}

void handleSubMenu()
{
  if (menuTitle == "Sound Generator" && menuItemClicked == 4)
  {
    menuItemClicked = 100;
    isPlaying = 0;
    waitForButtonPress(20000);
    subMenu();
  }

  // Configuring Sub Menu
  if (menuTitle == "Sub Menu" && menuItemClicked==0)
  {
    menuItemClicked=100;
    menuTitle = ""; // Turning Menu OFF
    display.clearDisplay();
    display.display();
  }

  // Return to Main Menu
  goBackMainMenu("Sub Menu", 1);

}

int returnFrequencyValue()
{
  // enterValue(String title, Start Value, Step Size, Lower Limit, Upper Limit);
  int frequencyValue = enterValue("Frequency:", 440, 1, 0, 1000);

  return frequencyValue;
}

void menuItemActions()
{
  if (menuItemClicked == 100) return; // Exit when no item has been selected

  // Main Menu Actions
  if (menuTitle == "Sound Generator" && menuItemClicked==0)
  {
    menuItemClicked=100;
    isPlaying = 1;
    waitForButtonPress(20000);
    //playSong();
    displayBabyLion();
    waitForButtonPress(20000);
  }
  // Enter frequency value
  if (menuTitle == "Sound Generator" && menuItemClicked==1)
  {
    menuItemClicked=100;
    waitForButtonPress(20000);
    int frequencyValue = returnFrequencyValue();
    Serial.println("Menu: Value set = " + String(frequencyValue));
  }

  handleLoadSongMenu();
  handleTestPeripheralsMenu();
  handleSubMenu();
}

void splashScreen()
{
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.setTextSize(1);
  display.print("CA Mountain Lion");
  display.setCursor(0, lineSpace1 * 2);
  display.print("Senior Design");
  display.setCursor(0, lineSpace1 * 4);
  display.print("Sound Generator");
  display.display();
}

void RotaryEncoder_Setup()
{
  // Configuring GPIO Pins
  pinMode(ENC_BUTTON_PIN, INPUT_PULLUP);
  pinMode(CLK_PIN, INPUT);
  pinMode(DT_PIN, INPUT);

  // Interrupt for reading Rotary Encoder values
  attachInterrupt(digitalPinToInterrupt(CLK_PIN), readEncoderValues, CHANGE);
}

void OLED_Screen_Setup()
{
  // Initializing OLED Display
  Wire.begin(I2C_SDA,I2C_SCL);
  if(!display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR))
  {
    Serial.println("\nError Initializing the OLED Display!\n");
    for(;;);
  }
}

void displayBabyLion()
{
  display.clearDisplay();
  display.drawBitmap(0, 0, image_data_lion, 128, 64, 1);
  display.display();
}
