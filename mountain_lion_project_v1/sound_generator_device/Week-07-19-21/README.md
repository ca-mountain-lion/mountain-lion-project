# Updates
* An OLED Menu has been implemented for the device. The user can navigate through the menu with the use of a rotary encoder. The CLK and DT pins of the rotary encoder will allow to scroll up or down, while the encoder's push button acts as a select button. The device is powered with four AA batteries
* This uses an I2S 3W Amplifier Module (MAX98357). The speaker is rated at 3W and 4-ohm
* An enable switch allows the user to disable or enable the menu. This is done to preserve battery power
* The buck-boost converter takes in the baterry input and outputs a stable 3.3V to the ESP32 board.

# Materials Used
| Part | QTY | Price ($) | Link |
| --- | --- | --- | --- |
| FireBeetle ESP32 Microcontroller | 1 | 6.90 per unit | [Product Link](https://www.dfrobot.com/product-1590.html)
| Adafruit MicroSD Card SPI Module | 1 | 2.95 per unit | [Product Link](https://www.mouser.com/ProductDetail/adafruit/4682/?qs=hWgE7mdIu5TtvwzYJhYD8g%3D%3D&countrycode=US&currencycode=USD)
| MAX98357 I2S Amplifier | 1 | 5.33 per unit | [Product Link](https://www.amazon.com/gp/product/B0912CWB7Z/)
| 3W 4-Ohm Speaker | 1 | 3.95 per unit | [Product Link](https://www.adafruit.com/product/4445)
| OLED Module (SSD1306) | 1 | 3.50 per unit | [Product Link](https://www.amazon.com/SSD1306-Self-Luminous-Display-Compatible-Raspberry/dp/B08FD643VZ/)
| Rotary Encoder Module | 1 | 2.00 per unit | [Product Link](https://www.amazon.com/gp/product/B07T3672VK/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
| Momentary Push Button Switch | 2 | 0.66 per unit | [Product Link](https://www.amazon.com/gp/product/B07F24Y1TB/)
| 2 Position DPDT Toggle Switch | 2 | 0.75 per unit | [Product Link](https://www.amazon.com/gp/product/B013DZB6CO/)
| AM312 Mini PIR Motion Sensor | 1 | 2.50 per unit | [Product Link](https://www.amazon.com/gp/product/B07NPKMH58)
| 6.2 x 3.54 x 2.3 Electronic Enclosure Box | 1 | 5.99 per unit | [Product Link](https://www.amazon.com/gp/product/B07TS6RY85)
| 100K Potentiometer | 1 | -- | [Product Link](https://www.amazon.com/gp/product/B07ZKJVZ53/)

# Video Demo
Click the image below to view the video demo:

[![ESP32 OLED Menu Demo 1](https://github.com/aaron-nanas/MountainLion_Senior_Project/blob/main/Screenshots/DevicePhoto-07-21-21.JPG?raw=true)](https://youtu.be/_COLPNzaOiU)

# Development Tools
* Microcontroller: ESP32 (FireBeetle ESP32 IoT Microcontroller)
* Text Editor: Atom
* Environment: PlatformIO
* Languages: C++, C
