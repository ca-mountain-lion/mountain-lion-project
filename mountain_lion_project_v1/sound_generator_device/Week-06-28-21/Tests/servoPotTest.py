from gpiozero import AngularServo, LED, MCP3008
import time

pot = MCP3008(channel=0)
servo = AngularServo(16, min_angle=-90, max_angle=180)
led1 = LED(17)
led2 = LED(27)

MIN_SERVO=500
MAX_SERVO=2500

MIN_POT_CAP=0
MAX_POT_CAP=1


def map(val, in_min, in_max, out_min, out_max):
   return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min



# def moveServoLeft():
#     if pot.value > 0.2 and pot.value < 0.4:
#         servo.angle = -90
#         led1.on()
#         led2.off()
#     elif pot.value > 0.6:
#         servo.angle = 180
#         led2.on()
#         led1.off()

while True:
    micros = map(pot.value, MIN_POT_CAP, MAX_POT_CAP, MIN_SERVO, MAX_SERVO)

    print(micros)