from flask import Flask, render_template
import RPi.GPIO as GPIO

app = Flask(__name__)

pins = 4

GPIO.setmode(GPIO.BCM)
GPIO.setup(pins, GPIO.OUT)
GPIO.output(pins, GPIO.LOW)

@app.route('/on')
def on():
    GPIO.output(pins, GPIO.HIGH)
    return render_template('on.html')

@app.route('/off')
def off():
    GPIO.output(pins, GPIO.LOW)
    return render_template('off.html')