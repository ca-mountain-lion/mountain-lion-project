import PySimpleGUIWeb as sg
import time
import pygame
import json
import _thread
from gpiozero import MotionSensor
from signal import pause
from ToneClass import GenerateTone

def adjust_volume(volume):
    actual_volume = (float(volume))/10.0
    pygame.mixer.music.set_volume(actual_volume)

def get_settings():
    settings_read = open("Configuration.json", 'r')
    dict__ = json.loads(settings_read.read())
    return dict__

def append_new_settings(new_settings):
    settings = open("Configuration.json", 'w')
    settings.truncate(0)
    settings.write(json.dumps(new_settings))

def settings_menu():
        
    settings_layout = [
        [sg.Text("settings")],
        [sg.Combo(sg.theme_list(), key='-THEME-')],
        [sg.Text("default Volume :-"),sg.Slider(range=(0, 10), default_value=5, orientation='horizontal', key='-VOLUME-')],
        [sg.OK(), sg.Cancel()]
    ]
    window = sg.Window("settings", layout=settings_layout)
    while True:
        event, values = window.read()
        if event in ("OK"):
            window.close()
            theme = values['-THEME-']
            default_volume = values['-VOLUME-']
            return {
                'theme' : theme,
                'volume' : default_volume
            }

def initializeSound():
    freq = 44100  # audio CD quality
    bitsize = -16  # unsigned 16 bit
    channels = 2  # 1 is mono, 2 is stereo
    buffer = 2048  # number of samples (experiment to get right sound)

    pygame.mixer.init(freq, bitsize, channels, buffer)

def loadSound(soundName):
    pygame.mixer.music.load(soundName)

def playSoundFile():
    pygame.mixer.music.play()

def unpauseSound():
    pygame.mixer.music.unpause()

def resetSound():
    # This function plays the sound again from the beginning
    pygame.mixer.music.stop()
    pygame.mixer.music.play()

def pauseSound():
    pygame.mixer.music.pause()

def exitSoundControl():
    pygame.mixer.stop()

def clearAddList(soundList, SOUND_NAME):
    soundList = []
    soundList.clear()
    soundList.append(SOUND_NAME)
    print(soundList)

def automaticTrigger():
    global isAutomaticOn
    while isAutomaticOn:
        if motionSensor.value == 1:
            #time.sleep(2)
            print(motionSensor.value)
            playSoundFile()
            time.sleep(2)
        else:
            print(motionSensor.value)
            time.sleep(0.2)

def printNoMotion():
    print("No Motion Detected!")

def playTone(frequency):
    pygame.mixer.pre_init(44100, -16, 1, 1024)
    pygame.init()
    GenerateTone(frequency).play(-1)
    time.sleep(2)
    pygame.mixer.stop()

motionSensor = MotionSensor(4, queue_len=4)
settings = get_settings()

try:
    sg.theme(settings['theme'])
except TypeError:
    sg.popup("settings loader failed")
    exit()

print = sg.Print
fontType = ("Arial", 20, "bold")
layout = [  [sg.Text("Select Mode", font=fontType)],
            [sg.Button("Sound File"), sg.Button("Generate Tone")],
            [sg.Text('_' * 40)],
            [sg.Text("Rabbit Sounds Selection", font=fontType)],
            [sg.Combo(["Rabbit Cry 1", "Cottontail Rabbit Cry 2", "Jackrabbit Cry 3", "Sound 4", "Sound 5", "Sound 6", "Sound 7", 
                        "Sound 8", "Sound 9", "Sound 10"], enable_events=True, key="Rabbit")],
            [sg.Button("Select Rabbit Sound")],
            [sg.Text('_' * 40)],
            [sg.Text("Deer Sounds Selection", font=fontType)],
            [sg.Combo(["Doe Bleat 1", "Fawn Distress 2", "Fawn Distress 3", "Sound 4", "Sound 5", "Sound 6", "Sound 7", 
                        "Sound 8", "Sound 9", "Sound 10"], enable_events=True, key="Deer")],
            [sg.Button("Select Deer Sound")],
            [sg.Text('_' * 40)],
            [sg.Text("Enter Frequency Value (in Hz)", font=fontType)],
            [sg.Input(key="freqval")],
            [sg.Button("Select Frequency")],
            [sg.Text('_' * 40)],
            [sg.Text("Sound Control", font=fontType)],
            [sg.Text("Please restart the player to enable new settings", visible=False, key='-SETTINGS_WARN-')],
            #[sg.Text("Tone Frequency :-"),sg.Slider(range=(0, 700), default_value=settings['frequency'], orientation='horizontal', key='-FREQUENCY-', enable_events=True)],
            [sg.Text("Volume :-", font=("Arial", 15)), sg.Slider(range=(0, 10), default_value=settings['volume'], orientation='horizontal', key='-VOLUME-', enable_events=True)],
            [sg.Button("Play"), sg.Button("Resume"), sg.Button("Pause"), sg.Button("Reset"), sg.Button("Stop"), sg.Button("Settings"), sg.Exit()],
            [sg.Text('_' * 40)],
            [sg.T("Set Automatic or Manual", font=fontType)],
            [sg.Button("Automatic"), sg.Button("Manual")]
        ]

window = sg.Window("Song Selection Test", layout, element_justification='center', 
                    auto_size_buttons=True, resizable=True, web_port=2222, web_start_browser=False)

initializeSound()
isLoaded = False
isAutomaticOn = False
isGenerateTone = False
isPlaySoundFile = True
isFrequencySel = False

while True:
    event, values = window.read(timeout=10)
    soundList = list()
    if event is None or event == "Exit":
        exitSoundControl()
        break
    
    elif event == "Sound File":
        isPlaySoundFile = True
        isGenerateTone = False
        print("Sound File Mode: ", isPlaySoundFile)
    
    elif event == "Generate Tone":
        isPlaySoundFile = False
        isGenerateTone = True
        print("Generate Tone Mode: ", isGenerateTone)

    elif event == "Select Rabbit Sound":
        isLoaded = True
        SOUND_NAME = values["Rabbit"]
        soundName = "/home/pi/GUI/SoundFiles/Rabbit/" + SOUND_NAME + ".mp3"
        print("Loaded: ", SOUND_NAME + ".mp3")
        print("Load Status: ", isLoaded)
        clearAddList(soundList, SOUND_NAME)
        loadSound(soundName)
    
    elif event == "Select Deer Sound":
        isLoaded = True
        SOUND_NAME = values["Deer"]
        soundName = "/home/pi/GUI/SoundFiles/Deer/" + SOUND_NAME + ".mp3"
        print("Loaded: ", SOUND_NAME + ".mp3")
        print("Load Status: ", isLoaded)
        clearAddList(soundList, SOUND_NAME)
        loadSound(soundName)

    elif event == "Play":
        if isPlaySoundFile:
            if not isLoaded:
                print("Select a sound first!")
                print("Load Status: ", isLoaded)
            else:
                print("Playing...")
                print("Load Status: ", isLoaded)
                playSoundFile()
        elif isGenerateTone:
            frequency = (values["freqval"])
            if frequency == '':
                print("Empty input!")
            else:
                try:
                    freqValue = int(frequency)
                    print(f'Frequency: {freqValue}')
                    if freqValue < 0:
                        print("Please enter a positive frequency value")
                    else:
                        playTone(freqValue)
                except:
                    print("Not a valid value")
    
    elif event == "Resume":
        print("Unpausing")
        unpauseSound()
    
    elif event == "Pause":
        print("Pausing...")
        pauseSound()
    
    elif event == "Reset":
        print("Resetting...")
        resetSound()
    
    elif event == "Stop":
        print("Stopping...")
        pygame.mixer.music.stop()

    elif event in ('-VOLUME-'):
        volume = values['-VOLUME-']
        adjust_volume(volume)

    elif event == "Settings":
        new_settings = settings_menu()
        append_new_settings(new_settings)
        window['-SETTINGS_WARN-'].Update(visible=True)
    
    if event == "Automatic":
        if not isLoaded:
            print("Load a sound first!")
            print("Load Status: ", isLoaded)
        else:
            isAutomaticOn = True
            print("Automatic Trigger ACTIVE")
            print("Automatic Status: ", isAutomaticOn)
            _thread.start_new_thread(automaticTrigger, ())
    
    if event == "Manual":
        isAutomaticOn = False
        print("Automatic Status: ", isAutomaticOn)
        print("Setting to manual...")


window.Close()