# Updates
First version of the Sound Generator device that is based on a Raspberry Pi Zero W. This board was chosen due to its relatively inexpensive price ($10). The device can be controlled using a web page GUI.

Summary:
* The main file is SoundGUI.py, and it imports Configuration.json and ToneClass.py
* The web page GUI can set the device to be automatically triggered, or manually control it
* Two modes can be set: the device can either play a sound file or play a tone at the specified frequency
* The frequency value can be manually entered in the text box (in Hz)
* The volume can be adjusted while the sound or tone is playing in real time
* Default settings for the device such as the GUI's color theme and the volume level are located in a JSON file (called Configuration.json)
* Before starting the device, the sound file must be loaded first. The sound files are divided into two categories: Deer and Rabbit. There is a drop-down menu that lists the available sound files
* Controls such as Play, Pause, Reset, or Stop are included. The settings menu can allow the user to update the GUI's color theme or the default volume level.

Problems:
* Since the Raspberry Pi consumes relatively more power and does not have a sleep mode, an alternative solution would be considered (i.e. using a different microcontroller that is both low-power and has a sleep mode feature)
* A microcontroller that can run on AA lithium batteries for weeks or months is much more preferable

# Video Demo
Click the image below to view the video demo:

[![Video Demo for Sound Generator Web Server](https://github.com/aaron-nanas/MountainLion_Senior_Project/blob/main/Screenshots/DeviceControlWeb.png?raw=true)](https://youtu.be/Lu3atJ6JMMY)

The device's address and the specified port can also be entered in a phone's browser, then control the device from there, as shown in this very short demo:

[![Video Demo for Sound Generator Phone Web Server](https://github.com/aaron-nanas/MountainLion_Senior_Project/blob/main/Screenshots/PhoneDemo.png?raw=true)](https://youtu.be/V_JyFtU0nG8)

# Development Tools
* Board Used: Raspberry Pi Zero W
* Text Editor: Visual Studio Code (Remote SSH)
* Languages: Python
