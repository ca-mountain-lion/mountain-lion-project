# Python script to test the speaker + web GUI

import pygame
import PySimpleGUIWeb as sg

def getFile():
    layout =    [[sg.Text("Please find the file to play")],
                [sg.Input(), sg.FileBrowse(button_text="Browse File")],
                [sg.OK()]]

    window = sg.Window("Select File", layout, web_port=2222, web_start_browser=False)

    while True:
        event, values = window.read()
        if event in (None, "OK"):
            window.close()
            file_name = values[0]
            return file_name

def initializeSound():
    freq = 44100  # audio CD quality
    bitsize = -16  # unsigned 16 bit
    channels = 2  # 1 is mono, 2 is stereo
    buffer = 2048  # number of samples (experiment to get right sound)
    x = getFile()

    pygame.mixer.init(freq, bitsize, channels, buffer)

    # Loading the song
    pygame.mixer.music.load(x)
    
    # Setting the volume
    pygame.mixer.music.set_volume(0.7)

def playSoundFile():
    initializeSound()
    # Start playing the song
    pygame.mixer.music.play()
  
# infinite loop
while True:
    playSoundFile()
    print("Type 'play' to play music")
    print("Press 'p' to pause, 'r' to resume")
    print("Press 'e' to exit the program")
    query = input("  ")
      
    if query == 'p':
  
        # Pausing the music
        pygame.mixer.music.pause()

    elif query == 'r':
  
        # Resuming the music
        pygame.mixer.music.unpause()
    elif query == 'e':
  
        # Stop the mixer
        pygame.mixer.music.stop()
        break