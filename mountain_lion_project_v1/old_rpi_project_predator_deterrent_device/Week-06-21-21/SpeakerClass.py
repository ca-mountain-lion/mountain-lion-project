'''
    Author: Aaron Nanas
    ECE 492: Senior Design
    Faculty: Dr. Shahnam Mirzaei
    Project: Predator Deterrent Device

    File: SpeakerClass.py
    Purpose: This script stores finds .mp3 files
    and stores them in an array called "mp3s". It will
    then play the mp3 files from the array until
    it reaches the end or reaches KeyboardInterrupt.
'''

import sys
import pygame as pg
import os
import time

class SpeakerModule:
    def speakerPlay(self, volumeLevel):
        freq = 44100  # audio CD quality
        bitsize = -16  # unsigned 16 bit
        channels = 2  # 1 is mono, 2 is stereo
        buffer = 2048  # number of samples (experiment to get right sound)

        pg.mixer.init(freq, bitsize, channels, buffer)
        self.playSoundFile(volumeLevel)

    def setup_MP3(self, music_file):
        clock = pg.time.Clock()
        try:
            pg.mixer.music.load(music_file)
            print("Music file {} loaded!".format(music_file))
        except pygame.error:
            print("File {} not found! {}".format(music_file, pg.get_error()))
            return

        pg.mixer.music.play()

        # Check if playback has finished
        while pg.mixer.music.get_busy():
            clock.tick(30)

    def playSoundFile(self, volumeLevel):
        print("Playing at volume: " + str(volumeLevel)+ "\n")
        pg.mixer.music.set_volume(volumeLevel)
        mp3s = []
        for file in os.listdir("."):
            if file.endswith(".mp3"):
                mp3s.append(file)

        print(mp3s)

        for x in mp3s:
            try:
                self.setup_MP3(x)
                time.sleep(.25)
            except KeyboardInterrupt:
                pg.mixer.music.fadeout(1000)
                pg.mixer.music.stop()
                raise SystemExit

