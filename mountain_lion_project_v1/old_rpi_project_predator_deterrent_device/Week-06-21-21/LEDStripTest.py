# Python script used to test LEDStripClass.py

from LEDStripClass import LEDStrip

GPIO.setmode(GPIO.BCM)

ledStrip = LEDStrip(board.D12, 60, 0.2)

def main():
    ledStrip.turnOff()
    ledStrip.turnRedOn()
    time.sleep(2)
    ledStrip.turnGreenOn()
    time.sleep(2)
    ledStrip.turnBlueOn()
    time.sleep(2)
    ledStrip.flashRed()
    time.sleep(2)
    ledStrip.turnOff()
    
if __name__ == "__main__":
    main()
    