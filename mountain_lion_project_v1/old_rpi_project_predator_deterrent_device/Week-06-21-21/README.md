# Updates
This update showcases the LCD Menu. A 16x2 I2C LCD is connected to one Raspberry Pi, while the second Raspberry Pi device is connected to the other peripherals. Also created Python classes for all of the peripheral devices. This provides reusability in the future and arranges the files in a more organized manner.

Summary:
* Created the following classes: CameraClass, LEDStripClass, I2CLCD, MenuClass, MotionSensorClass, MotorClass, NoteClass, SpeakerClass
* Each class comes with a separate Python script to test its functionality - this helped detect bugs earlier on
* Replaced the Piezo buzzer entirely, and specific tones can be played via I2S. This feature is added via the new NoteClass
* As for playing sound files, it searches for .mp3 files in the SD card, lists the titles, and plays them.
    * Planned feature: Specify which sound file to play
    
# Video Demo
Click the image below to view the video demo:

[![Video Demo for LCD Menu](https://github.com/aaron-nanas/MountainLion_Senior_Project/blob/main/Screenshots/DeviceLCDMenu.png?raw=true)](https://youtu.be/m31EgpWTCzU)

# Diagram
![Project Diagram](https://github.com/aaron-nanas/MountainLion_Senior_Project/blob/main/Screenshots/Project_Diagram_v2.png?raw=true)

# Development Tools
* Board Used: Raspberry Pi 4
* Text Editor: Visual Studio Code (Remote SSH)
* Languages: Python
