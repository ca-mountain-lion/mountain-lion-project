'''
    Author: Aaron Nanas
    ECE 492: Senior Design
    Faculty: Dr. Shahnam Mirzaei
    Project: Predator Deterrent Device

    File: CameraClass.py
    Purpose: The camera class for the LCD menu. Includes
    functions to start recording, to take a still photo,
    to record a video for the specified duration, and to stop
    the current recording.
'''

from picamera import PiCamera
import time
from time import sleep
import I2CLCD
import pigpio

pi = pigpio.pi('192.168.1.3')
if not pi.connected:
    exit(0)

class CameraModule:
    def __init__(self, manualRecordTime, automaticWaitTime):
        self.manualRecordTime = manualRecordTime
        self.automaticWaitTime = automaticWaitTime
        self.camera = PiCamera()
        self.lcd = I2CLCD.lcd(pi, width=16)
        self.timeFormat = (time.strftime("%Y-%b-%d_%H:%M:%S"))
        self.photoFileName = "/home/pi/Main/CameraFiles/Photos/" + "Photo_" + self.timeFormat + ".jpg"
        self.videoFileName = "/home/pi/Main/CameraFiles/Videos/" + "Video_" + self.timeFormat + ".h264"

    def manualStillCapture(self):
        print("Taking a still photo")
        self.camera.resolution = (1024, 768)
        self.camera.start_preview()
        sleep(2) # Warm-up time for camera
        self.lcd.put_line(0, "CAM CTRL Menu   ")
        self.lcd.put_line(1, "Taking photo... ")
        self.camera.capture(self.photoFileName)
        sleep(1) # Wait 1 second after capturing photo
        print("Finished capturing a photo")
        self.lcd.put_line(0, "CAM CTRL Menu   ")
        self.lcd.put_line(1, "Photo captured! ")
        sleep(3) # Wait 3 seconds for message to display
        self.lcd.put_line(0, "CAM CTRL Menu   ")
        self.lcd.put_line(1, "<  STILL PHOTO >")

    def manualVideoRecord(self, manualRecordTime):
        print("Recording a short video")
        self.camera.resolution = (640, 480)
        self.lcd.put_line(0, "CAM CTRL Menu   ")
        self.lcd.put_line(1, "Recording vid...")
        self.camera.start_recording(self.videoFileName)
        self.camera.wait_recording(self.manualRecordTime)
        self.camera.stop_recording()
        sleep(1)
        self.lcd.put_line(0, "CAM CTRL Menu   ")
        self.lcd.put_line(1, "Recording done! ")
        print("Finished recording a short video")
        sleep(3) # Wait 3 seconds for message to display
        self.lcd.put_line(0, "CAM CTRL Menu   ")
        self.lcd.put_line(1, "<  RECORD VID   ")
    
    def waitVideoRecord(self, automaticWaitTime):
        self.camera.wait_recording(self.automaticWaitTime)
    
    def beginRecording(self):
        print("Camera is recording")
        self.camera.start_recording(self.videoFileName)
        
    def stopRecording(self):
        self.camera.stop_recording()
        print("Footage recorded")

