'''
    Author: Aaron Nanas
    ECE 492: Senior Design
    Faculty: Dr. Shahnam Mirzaei
    Project: Predator Deterrent Device

    File: NoteClass.py
    Purpose: Used with the I2S amplifier and 3W, 4-Ohm Speaker.
    This will allow the speaker to play a tone at the
    specified frequency.
'''

from array import array
from time import sleep

import pygame
from pygame.mixer import Sound, get_init, pre_init

class Note(Sound):

    def __init__(self, frequency, volume=.1):
        self.frequency = frequency
        Sound.__init__(self, self.build_samples())
        self.set_volume(volume)

    def build_samples(self):
        period = int(round(get_init()[0] / self.frequency))
        samples = array("h", [0] * period)
        amplitude = 2 ** (abs(get_init()[1]) - 1) - 1
        for time in range(period):
            if time < period / 2:
                samples[time] = amplitude
            else:
                samples[time] = -amplitude
        return samples

    def playTone(self, freq_value, volume_level):
        pre_init(44100, -16, 1, 1024)
        pygame.init()
        Note(freq_value, volume_level).play(-1)
        sleep(3)
        return self

    def runTone(self):
        freq_value = 440
        volume_level = 0.1
        playTone(freq_value, volume_level)
        return self
