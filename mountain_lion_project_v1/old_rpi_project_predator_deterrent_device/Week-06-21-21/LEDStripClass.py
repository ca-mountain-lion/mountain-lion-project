'''
    Author: Aaron Nanas
    ECE 492: Senior Design
    Faculty: Dr. Shahnam Mirzaei
    Project: Predator Deterrent Device

    File: LEDStripClass.py
    Purpose: The LED Strip class for the LCD Menu. This includes
    functions that will turn on the LED strip for a single color,
    flash lights, or specific patterns. More patterns will be added.
'''
import time
import board
import neopixel

class LEDStrip:
    def __init__(self, pin, numPixels, brightnessValue):
        self.numPixels = numPixels
        self.brightnessValue = brightnessValue
        self.pixels = neopixel.NeoPixel(pin, self.numPixels, brightness=self.brightnessValue)

    def turnOff(self):
        print("Turning off LED strip")
        self.pixels.fill((0, 0, 0))
        time.sleep(0.3)

    def turnRedOn(self):
        print("Turning RED on")
        self.pixels.fill((255, 0, 0))
        time.sleep(0.3)
    
    def turnGreenOn(self):
        print("Turning GREEN on")
        self.pixels.fill((0, 255, 0))
        time.sleep(0.3)
        
    def turnBlueOn(self):
        print("Turning BLUE on")
        self.pixels.fill((0, 0, 255))
        time.sleep(0.3)
    
    def flashRed(self):
        print("Flashing RED lights")
        for x in range(1, 4, 1):
            self.turnRedOn()
            time.sleep(0.5)
            self.turnOff()
            time.sleep(0.5)
        
    def flashGreen(self):
        print("Flashing GREEN lights")
        for x in range(1, 4, 1):
            self.turnGreenOn()
            time.sleep(0.5)
            self.turnOff()
            time.sleep(0.5)

    def flashBlue(self):
        print("Flashing BLUE lights")
        for x in range(1, 4, 1):
            self.turnBlueOn()
            time.sleep(0.5)
            self.turnOff()
            time.sleep(0.5)

    def turnRainbowOn(self):
        print("Turning RAINBOW on")
        for x in range(0, 9, 1):
            self.pixels[x] = (255, 0, 0)
            time.sleep(0.1)

        for x in range(9, 18, 1):
            self.pixels[x] = (255, 70, 0)
            time.sleep(0.1)

        for x in range(18, 27, 1):
            self.pixels[x] = (255, 255, 0)
            time.sleep(0.1)

        for x in range(27, 36, 1):
            self.pixels[x] = (0, 128, 0)
            time.sleep(0.1)

        for x in range(36, 45, 1):
            self.pixels[x] = (0, 0, 255)
            time.sleep(0.1)

        for x in range(45, 54, 1):
            self.pixels[x] = (75, 0, 130)
            time.sleep(0.1)

        for x in range(54, 60, 1):
            self.pixels[x] = (128, 0, 255)
            time.sleep(0.1)



