from MenuClass import MenuStateMachine
from signal import pause
import time

MenuControl = MenuStateMachine()

try:
    while True:
        MenuControl.handleMenu()
except KeyboardInterrupt:
    MenuControl.StopMenu()
