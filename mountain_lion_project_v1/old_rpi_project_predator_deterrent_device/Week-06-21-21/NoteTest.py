# Generate a 440 Hz square waveform in Pygame by building an array of samples and play
# it for 5 seconds.  Change the hard-coded 440 to another value to generate a different
# pitch.
#
# Run with the following command:
#   python pygame-play-tone.py

from array import array
from time import sleep
import time

import pygame
from pygame.mixer import Sound, get_init, pre_init

class Note(Sound):

    def __init__(self, frequency, volume=.1):
        self.frequency = frequency
        Sound.__init__(self, self.build_samples())
        self.set_volume(volume)

    def build_samples(self):
        period = int(round(get_init()[0] / self.frequency))
        samples = array("h", [0] * period)
        amplitude = 2 ** (abs(get_init()[1]) - 1) - 1
        for time in range(period):
            if time < period / 2:
                samples[time] = amplitude
            else:
                samples[time] = -amplitude
        return samples


def main(freq_value, volume_level):
    pre_init(44100, -16, 1, 1024)
    pygame.init()
    time.sleep(0.5)
    Note(freq_value, volume_level).play(1000)
    time.sleep(2)

if __name__ == "__main__":
    volume_level = 0.8
    freq_value = 440

    # print("Note 1")
    # main(440, volume_level)
    # print("Note 2")
    # main(600, volume_level)
    # print("Note 3")
    # main(700, volume_level)
    # print("Note 4")
    # main(800, volume_level)
    # print("Note 5")
    # main(10000, volume_level)

    i = 0

    for x in range(0, 500, 100):
        i = i + 1
        print("Note", i)
        main(freq_value + x, volume_level)
