'''
    Author: Aaron Nanas
    ECE 492: Senior Design
    Faculty: Dr. Shahnam Mirzaei
    Project: Predator Deterrent Device

    File: MotorClass.py
    Purpose: The Stepper Motor class for the LCD Menu.
    This includes functions to run the motor clockwise,
    counter-clockwise, or stop it entirely.
'''

from RpiMotorLib import RpiMotorLib
import RPi.GPIO as GPIO
import time

class StepperMotor:
    
    def __init__(self, motorDirPin, motorStepPin, stepNum=100):
        self.stepNum = stepNum
        self.StepperMotor_Inst = RpiMotorLib.A4988Nema(motorDirPin, motorStepPin, (21, 21, 21), "DRV8825")
        self.DIRECTION_ARRAY = [False, True]

    def runMotorForward(self):
        for x in range(5):
            self.StepperMotor_Inst.motor_go(self.DIRECTION_ARRAY[0], # Move the motor in clockwise direction
                                    "Full", # Move in "Full" step type
                                    self.stepNum, # Number of steps
                                    0.0005,
                                    False,
                                    0.05)
            #self.stopMotor()
    
    def runMotorBackward(self):
        print("Running the motor in counter-clockwise direction")
        for ii in range(5):
            self.StepperMotor_Inst.motor_go(self.DIRECTION_ARRAY[1], # Move the motor in counter-clockwise direction
                                    "Full", # Move in "Full" step type
                                    self.stepNum, # Number of steps
                                    0.0005,
                                    False,
                                    0.05)
            #time.sleep(1)
    
    def stopMotor(self):
        print("Turning off motor")
        self.StepperMotor_Inst.motor_stop()
        time.sleep(0.5)

