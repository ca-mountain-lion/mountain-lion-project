'''
    Author: Aaron Nanas
    ECE 492: Senior Design
    Faculty: Dr. Shahnam Mirzaei
    Project: Predator Deterrent Device

    File: mainGUI.py
    Purpose: This is the updated version of the GUI from Spring 2021.
    This includes the new LED strip and speaker devices.

    To run, enter the command in the terminal:
    sudo -E python3 mainGUI.py
'''
import PySimpleGUI as sg
import RPi.GPIO as GPIO
import board
import neopixel
import os
from RpiMotorLib import RpiMotorLib
import time
from picamera import PiCamera
from gpiozero import MotionSensor

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# GPIO Pin Locations
PIR_SENSOR = MotionSensor(4)
BUZZER_PIN = 13
LEDSTRIP_PIN = board.D12
SPEAKER_BCLK = 18
SPEAKER_LRCLK = 19
SPEAKER_DIN = 21
MOTOR_DIR_PIN = 22 # Motor Direction (DIR) GPIO Pin
MOTOR_STEP_PIN = 23 # Motor Step GPIO Pin
MOTOR_ENABLE = 24 # Motor Enable Pin (Set LOW to enable)

# Global Variables for Buzzer PWM
DUTY_CYCLE = 50
DUTY_CYCLE2 = 25
DUTY_CYCLE3 = 75
DUTY_STOP = 0
PWM_FREQ = 1000 # Set the Buzzer's frequency (in Hz)

# LED Strip Initialization
num_pixels = 60
pixels = neopixel.NeoPixel(LEDSTRIP_PIN, num_pixels, brightness=0.2)

# Motor and GPIO Initialization
StepperMotor_Inst = RpiMotorLib.A4988Nema(MOTOR_DIR_PIN, MOTOR_STEP_PIN, (21, 21, 21), "DRV8825")
GPIO.setup(MOTOR_ENABLE, GPIO.OUT)
GPIO.setup(BUZZER_PIN, GPIO.OUT)

# Camera Initialization
camera = PiCamera()

# Motor Control
DIRECTION_ARRAY = [False, True]
GPIO.output(MOTOR_ENABLE, GPIO.LOW) # Setting MOTOR_ENABLE low to activate

# LED Strip Functions
def ledsOn():
    print("Turning on LEDs")
    pixels.fill((255, 0, 0))
    time.sleep(1)

def ledsOff():
    print("Turning off LEDs")
    pixels.fill((0, 0, 0))
    time.sleep(1)

def rgbPattern():
    print("Showing RGB Pattern")

    for x in range(0, 20, 1):
        pixels[x] = (255, 0, 0)
        time.sleep(0.05)

    for x in range(20, 40, 1):
        pixels[x] = (0, 255, 0)
        time.sleep(0.05)

    for x in range(40, 60, 1):
        pixels[x] = (0, 0, 255)
        time.sleep(0.05)

def rainbowPattern():
    print("Showing Rainbow Pattern")

    for x in range(0, 9, 1):
        pixels[x] = (255, 0, 0)
        time.sleep(0.1)

    for x in range(9, 18, 1):
        pixels[x] = (255, 70, 0)
        time.sleep(0.1)

    for x in range(18, 27, 1):
        pixels[x] = (255, 255, 0)
        time.sleep(0.1)

    for x in range(27, 36, 1):
        pixels[x] = (0, 128, 0)
        time.sleep(0.1)

    for x in range(36, 45, 1):
        pixels[x] = (0, 0, 255)
        time.sleep(0.1)

    for x in range(45, 54, 1):
        pixels[x] = (75, 0, 130)
        time.sleep(0.1)

    for x in range(54, 60, 1):
        pixels[x] = (128, 0, 255)
        time.sleep(0.1)

# Alarm Functions
def turnOnAlarm():
    print("Alarm Activated!")
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE)
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE2) # Change to 25
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE3) # Change to 75
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE) # Change back to 50

def turnOffAlarm():
    print("Alarm Disabled")
    BUZZER_PWM.ChangeDutyCycle(DUTY_STOP)

def turnOnBuzzer():
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE)
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE2)
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE3)
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE)
    print("Buzzer ON")

def turnOffBuzzer():
    BUZZER_PWM.ChangeDutyCycle(DUTY_STOP)
    print("Buzzer OFF")

# Motor Control Functions:
# runMotorForward() will run the stepper motor in a clockwise direction
# runMotorBackward() will run the stepper motor in a counter-clockwise direction
def runMotorForward():
    print("Running the motor in clockwise direction")
    for ii in range(5):
        StepperMotor_Inst.motor_go(DIRECTION_ARRAY[0], # Move the motor in clockwise direction
                                   "Full", # Move in "Full" step type
                                   400, # Number of steps
                                   0.0005,
                                   False,
                                   0.05)
        time.sleep(1)

def runMotorBackward():
    print("Running the motor in counter-clockwise direction")
    for ii in range(5):
        StepperMotor_Inst.motor_go(DIRECTION_ARRAY[1], # Move the motor in counter-clockwise direction
                                   "Full", # Move in "Full" step type
                                   400, # Number of steps
                                   0.0005,
                                   False,
                                   0.05)
        time.sleep(1)

def stopMotor():
    print("Stopping the motor")
    StepperMotor_Inst.motor_stop()
    time.sleep(0.5)

# Camera manual control functions
def manualStillCapture():
    print("Taking a still photo")
    camera.resolution = (1024, 768)
    camera.start_preview()
    sleep(2) # Warm-up time for camera
    camera.capture('DevicePhoto.jpg')
    sleep(1)
    print("Finished capturing a photo")

def manualVideoRecord():
    print("Recording a short video")
    camera.resolution = (640, 480)
    camera.start_recording('Device_ManualFootage.h264')
    camera.wait_recording(15) # Record video for about 15 seconds
    camera.stop_recording()
    sleep(1)
    print("Finished recording a short video")

# Automatic and Manual Trigger Functions
def automaticTrigger():
    print("Automatic Trigger ON")
    camera.start_recording('Device_AutoFootage.h264')
    print("Alarm is active and camera is recording")
    for ii in range(4):
        StepperMotor_Inst.motor_go(DIRECTION_ARRAY[ii%2], # If 0, then clockwise; If 1, then counter-clockwise
                              "Full", # This indicates the step type (Full, 1/2, 1/4, 1/8, 1/16, 1/32)
                              400, # The number of steps
                              0.0005, # The amount of step delay (in seconds)
                              False, # If true, print output
                              0.05) # The amount of initial delay (in seconds)
        ledsOn()
        turnOnBuzzer()
    turnOffBuzzer()
    camera.wait_recording(5) # Record for 5 seconds more after the alarm is triggered
    camera.stop_recording() # Stop recording after 5 more seconds have passed
    print("Footage recorded")
    time.sleep(2) # Wait for 2 seconds

def manualTrigger():

    print("Manual Trigger ON")

# Function to turn off all active peripherals
def turnOffActiveDevices():
    print("Turning OFF all active peripherals")
    ledsOff()
    BUZZER_PWM.ChangeDutyCycle(DUTY_STOP)
    StepperMotor_Inst.motor_stop()
    time.sleep(0.5)

# Function to call automaticTrigger() when motion is detected
def automaticTriggerHelper():
    print("Waiting for PIR Motion Sensor")
    PIR_SENSOR.wait_for_no_motion()
    print("Sensor is ready")
    PIR_SENSOR.wait_for_motion()
    print("Motion Detected!")
    automaticTrigger()
    time.sleep(2)

# Test for motion
def motionSensorTest():
    print("Waiting for PIR Motion Sensor")
    PIR_SENSOR.wait_for_no_motion()
    print("Sensor is ready")
    PIR_SENSOR.wait_for_motion()
    print("Motion Detected!")
    time.sleep(1)
    PIR_SENSOR.wait_for_no_motion()
    print("No Motion Detected!")

# Initialize the PWM for the Buzzer
BUZZER_PWM = GPIO.PWM(BUZZER_PIN, PWM_FREQ)
BUZZER_PWM.start(0) # Initialize the Buzzer PWM at 0 to turn off


# Variable for font size
fontSize = 10

# Window GUI Setup
layout = [[sg.Text('Device Control Window', size=(30, 1),  justification = 'center', font=("Helvetica", 25))],
          [sg.Text('Mode Selection: Automatic | Manual', font = ("Helvetica", 12))],
          [sg.Button('AUTOMATIC'), sg.Button('MANUAL')],
          [sg.Text('_' * 80)],
          [sg.Text('Manual Control Configuration', font = ("Helvetica", 12))],
          [sg.Text('Click LED ON to turn on LED or LED OFF to turn off LED', font = ("Helvetica", fontSize))],
          [sg.Button('LED ON'), sg.Button('LED OFF'), sg.Button('RGB PATTERN'), sg.Button('RAINBOW PATTERN')],
          [sg.Text('Click PLAY SPEAKER to activate Speaker', font=("Helvetica", fontSize))],
          [sg.Button('PLAY SPEAKER')],
          [sg.Text('Click BUZZER ON to activate Buzzer or BUZZER OFF to disable Buzzer', font = ("Helvetica", fontSize))],
          [sg.Button('BUZZER ON'), sg.Button('BUZZER OFF')],
          [sg.Text('Click ALARM ON to activate Alarm circuit or ALARM OFF to disable it', font = ("Helvetica", fontSize))],
          [sg.Button('ALARM ON'), sg.Button('ALARM OFF'), sg.Button('Test Motion Sensor')],
          [sg.Text('Motor Control', font = ("Helvetica", fontSize))],
          [sg.Button('Move Forward'), sg.Button('Move Backward'), sg.Button('Stop Motor')],
          [sg.Text('Camera Control', font = ("Helvetica", fontSize))],
          [sg.Button('Still Capture'), sg.Button('Record A Short Video')],
          [sg.Text('_' * 80)],
          [sg.Button('Exit', button_color = '#8b0000'), sg.Button('Turn OFF All Active Devices')]]

window = sg.Window('Predator Deterrent Device', layout)

def main():
    while True:
        event, values = window.read()
        print(event, values)
        if event == 'AUTOMATIC':
            automaticTriggerHelper()
        if event == 'MANUAL':
            manualTrigger()
            turnOffActiveDevices() # Turn off all peripherals when manual mode is activated
        if event == 'Turn OFF Devices':
            turnOffActiveDevices()
        if event ==  'LED ON':
            ledsOn()
        if event == 'LED OFF':
            ledsOff()
        if event == 'RGB PATTERN':
            rgbPattern()
        if event == 'RAINBOW PATTERN':
            rainbowPattern()
        if event == 'PLAY SPEAKER':
            os.system("sudo python3 speakerMusicScript.py")
        if event == 'BUZZER ON':
            BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE) # Buzzer will play a sound
        if event == 'BUZZER OFF':
            BUZZER_PWM.ChangeDutyCycle(DUTY_STOP) # Disable buzzer
        if event == 'ALARM ON':
            turnOnAlarm()
        if event == 'ALARM OFF':
            turnOffAlarm()
        if event == 'Test Motion Sensor':
            motionSensorTest()
        if event == 'Move Forward':
            runMotorForward()
        if event == 'Move Backward':
            runMotorBackward()
        if event == 'Stop Motor':
            stopMotor()
        if event == 'Still Capture':
            manualStillCapture()
        if event == 'Record A Short Video':
            manualVideoRecord()
        if event == sg.WIN_CLOSED or event == 'Exit':
            turnOffActiveDevices()
            break

    time.sleep(0.5)
    window.close()

if __name__ == "__main__":
    main()


