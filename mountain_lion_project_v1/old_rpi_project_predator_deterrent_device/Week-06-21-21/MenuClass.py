from gpiozero import Button, LED
from gpiozero.pins.pigpio import PiGPIOFactory
from MotorClass import StepperMotor
from LEDStripClass import LEDStrip
from signal import pause
from CameraClass import CameraModule
from MotionSensorClass import MotionSensorModule
import time
import I2CLCD
import pigpio
import board
import neopixel
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

pi = pigpio.pi('192.168.1.3')
if not pi.connected:
    exit(0)

# led = LED(17)

# Motion Sensor Initialization
motionSensorPin = 4

# LCD Initialization
lcd = I2CLCD.lcd(pi, width=16)

# Motor Initialization
motorDirPin = 22
motorStepPin = 23
motorEnablePin = 24
GPIO.setup(motorEnablePin, GPIO.OUT)
stepperMotor = StepperMotor(motorDirPin, motorStepPin)

# Camera Initiliazation
Camera = CameraModule(15, 5) # Record manually for 15 seconds; wait for 5s when automatic

remote_factory = PiGPIOFactory(host='192.168.1.3')

leftScroll = Button(17, pin_factory=remote_factory)
rightScroll = Button(27, pin_factory=remote_factory)
selectOption = Button(22, pin_factory=remote_factory)
backToSubMenu = Button(26, pin_factory=remote_factory)

ledStrip = LEDStrip(board.D12, 60, 0.2)

motionSensor = MotionSensorModule(motionSensorPin)

class MenuState(object):
    isOn = False
    isEnabled = False

    def __init__(self):
        pass
    
    # Goes to the previous state when first button is pressed (scrolling left)
    def previousState(self):
        return self

    # Goes to the next state when second button is pressed (scrolling right)
    def nextState(self):
        return self

    # Selects the current state when third button is pressed
    def selectState(self):
        return self

    def performAction(self):
        return self
    
    def backSubMenu(self):
        # Put function that will disable all active devices
        return self
    
    def StopMenu(self):
        ledStrip.turnOff()
        lcd.backlight_on = False
        lcd.close()
        return self

    def handleMenu(self):
        firstButton = self.checkButtonInput(leftScroll)
        secondButton = self.checkButtonInput(rightScroll)
        thirdButton = self.checkButtonInput(selectOption)
        fourthButton = self.checkButtonInput(backToSubMenu)

        if firstButton == True:
            return self.previousState()
        elif secondButton == True:
            return self.nextState()
        elif thirdButton == True:
            return self.selectState()
        elif fourthButton == True:
            return self.backSubMenu()

        return self

    # Returns either True or False depending on button input status
    def checkButtonInput(self, btn):
        isPressed = False

        if btn.is_pressed:
            time.sleep(0.1)
            isPressed = True
        else:
            isPressed = False

        return isPressed
    
    def enableMenu(self):
        firstButton = self.checkButtonInput(leftScroll)
        secondButton = self.checkButtonInput(rightScroll)
        thirdButton = self.checkButtonInput(selectOption)
        fourthButton = self.checkButtonInput(backToSubMenu)

        if firstButton | secondButton | thirdButton | fourthButton == True:
            self.isEnabled = True
        else:
            self.isEnabled = False
        
        return self.isEnabled
    
    def automaticTrigger(self):
        lcd.put_line(0, "Auto Trigger ON ")
        lcd.put_line(1, "Alarm is active" )
        Camera.beginRecording()
        stepperMotor.runMotorForward()
        ledStrip.flashRed()
        Camera.waitVideoRecord(5) # Continue recording for 5 seconds more after the alarm is triggered
        Camera.stopRecording()
        lcd.put_line(0, "Recording done! ")
        lcd.put_line(1, "Video is ready " )
        time.sleep(3) # Wait for 3 seconds for message to display
        lcd.put_line(0, "Auto Trigger ON ")
        lcd.put_line(1, "Waiting...     " )

        return self

    # Used to print the current state
    def __repr__(self):
        return "Current State: " + self.getState()

    # Gets the string of the class name for current state
    def getState(self):
        return self.__class__.__name__

class StopMenu(MenuState):
    def __init__(self):
        lcd.close()

class Start(MenuState):
    def __init__(self):
        lcd.put_line(0, "Main Menu       ")
        lcd.put_line(1, "   Start       >")
    def previousState(self):
        return Start()
    def nextState(self):
        return ManualControl()
    def selectState(self):
        return Start()

class ManualControl(MenuState):
    def __init__(self):
        lcd.put_line(0, "Main Menu       ")
        lcd.put_line(1, "<  Manual      >")
    def previousState(self):
        return Start()
    def nextState(self):
        return AutomaticControl()
    def selectState(self):
        return ledControl()

class ledControl(MenuState):
    def __init__(self):
        lcd.put_line(0, "Manual Menu     ")
        lcd.put_line(1, "<  LED CTRL    >")
    def performAction(self):
        print("Selecting LED control")
        return ledOff()
    def previousState(self):
        return ledControl()
    def nextState(self):
        return motorControl()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return ManualControl()

# Motor Control Classes
class motorControl(MenuState):
    def __init__(self):
        lcd.put_line(0, "Manual Menu     ")
        lcd.put_line(1, "<  MOTOR CTRL  >")
    def performAction(self):
        print("Selecting Motor control")
        return motorOff()
    def previousState(self):
        return ledControl()
    def nextState(self):
        return cameraControl()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return ManualControl()

class motorOff(MenuState):
    def __init__(self):
        lcd.put_line(0, "Motor Menu      ")
        lcd.put_line(1, "<  MOTOR OFF   >")
    def performAction(self):
        stepperMotor.stopMotor()
        return self
    def previousState(self):
        return motorOff()
    def nextState(self):
        return motorOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return motorControl()

class motorOn(MenuState):
    def __init__(self):
        lcd.put_line(0, "Motor Menu      ")
        lcd.put_line(1, "<  MOTOR ON    >")
    def performAction(self):
        return moveForward()
    def previousState(self):
        return motorOff()
    def nextState(self):
        return motorOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return motorControl()

class moveForward(MenuState):
    def __init__(self):
        lcd.put_line(0, "Motor ON Menu  ")
        lcd.put_line(1, "<  FORWARD    >")
    def performAction(self):
        stepperMotor.runMotorForward()
        return self
    def previousState(self):
        return moveForward()
    def nextState(self):
        return moveBackward()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        stepperMotor.stopMotor()
        return motorControl()

class moveBackward(MenuState):
    def __init__(self):
        lcd.put_line(0, "Motor ON Menu  ")
        lcd.put_line(1, "<  BACKWARD   >")
    def performAction(self):
        stepperMotor.runMotorBackward()
        return self
    def previousState(self):
        return moveForward()
    def nextState(self):
        return moveBackward()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        stepperMotor.stopMotor()
        return motorControl()

class cameraControl(MenuState):
    def __init__(self):
        lcd.put_line(0, "Manual Menu     ")
        lcd.put_line(1, "<  CAM CTRL    >")
    def performAction(self):
        return stillPhotoCapture()
    def previousState(self):
        return motorControl()
    def nextState(self):
        return motionSensorControl()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return ManualControl()

class motionSensorControl(MenuState):
    def __init__(self):
        lcd.put_line(0, "Manual Menu     ")
        lcd.put_line(1, "<  MOTION TEST >")
    def performAction(self):
        return testForMotion()
    def previousState(self):
        return cameraControl()
    def nextState(self):
        return motionSensorControl()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return ManualControl()

class testForMotion(MenuState):
    def __init__(self):
        lcd.put_line(0, "Motion Test Menu")
        lcd.put_line(1, "Press SEL button")
    def performAction(self):
        motionSensor.motionSensorTest()
        return self
    def previousState(self):
        return motionSensorControl()
    def nextState(self):
        return testForMotion()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return ManualControl()

class stillPhotoCapture(MenuState):
    def __init__(self):
        lcd.put_line(0, "CAM CTRL Menu   ")
        lcd.put_line(1, "   STILL PHOTO >")
    def performAction(self):
        Camera.manualStillCapture()
        return self
    def previousState(self):
        return stillPhotoCapture()
    def nextState(self):
        return recordVideo()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return cameraControl()

class recordVideo(MenuState):
    def __init__(self):
        lcd.put_line(0, "CAM CTRL Menu   ")
        lcd.put_line(1, "<  RECORD VID   ")
    def performAction(self):
        Camera.manualVideoRecord(10) # Record video for 10 seconds
        return self
    def previousState(self):
        return stillPhotoCapture()
    def nextState(self):
        return recordVideo()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return cameraControl()

class ledOff(MenuState):
    def __init__(self):
        lcd.put_line(0, "LED Menu        ")
        lcd.put_line(1, "<  LED OFF     >")
    def performAction(self):
        ledStrip.turnOff()
        return self
    def previousState(self):
        return ledOff()
    def nextState(self):
        return ledOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return ManualControl()

class ledOn(MenuState):
    def __init__(self):
        lcd.put_line(0, "LED Menu        ")
        lcd.put_line(1, "<  LED ON      >")
    def performAction(self):
        return redOn()
    def previousState(self):
        return ledOff()
    def nextState(self):
        return ledOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return ManualControl()

class redOn(MenuState):
    def __init__(self):
        lcd.put_line(0, "LED ON Menu     ")
        lcd.put_line(1, "<  RED         >")
    def performAction(self):
        ledStrip.turnRedOn()
        return self
    def previousState(self):
        return redOn()
    def nextState(self):
        return greenOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        ledStrip.turnOff()
        return ledOn()

class greenOn(MenuState):
    def __init__(self):
        lcd.put_line(0, "LED ON Menu     ")
        lcd.put_line(1, "<  GREEN       >")
    def performAction(self):
        ledStrip.turnGreenOn()
        return self
    def previousState(self):
        return redOn()
    def nextState(self):
        return blueOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        ledStrip.turnOff()
        return ledOn()
    
class blueOn(MenuState):
    def __init__(self):
        lcd.put_line(0, "LED ON Menu     ")
        lcd.put_line(1, "<  BLUE        >")
    def performAction(self):
        ledStrip.turnBlueOn()
        return self
    def previousState(self):
        return greenOn()
    def nextState(self):
        return rainbowOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        ledStrip.turnOff()
        return ledOn()  

class rainbowOn(MenuState):
    def __init__(self):
        lcd.put_line(0, "LED ON Menu     ")
        lcd.put_line(1, "<  RAINBOW     >")
    def performAction(self):
        ledStrip.turnRainbowOn()
        return self
    def previousState(self):
        return blueOn()
    def nextState(self):
        return flashRedOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        ledStrip.turnOff()
        return ledOn()

class flashRedOn(MenuState):
    def __init__(self):
        lcd.put_line(0, "LED ON Menu     ")
        lcd.put_line(1, "<  FLASH RED   >")
    def performAction(self):
        ledStrip.flashRed()
        return self
    def previousState(self):
        return rainbowOn()
    def nextState(self):
        return flashGreenOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        ledStrip.turnOff()
        return ledOn()

class flashGreenOn(MenuState):
    def __init__(self):
        lcd.put_line(0, "LED ON Menu     ")
        lcd.put_line(1, "<  FLASH GREEN >")
    def performAction(self):
        ledStrip.flashGreen()
        return self
    def previousState(self):
        return flashRedOn()
    def nextState(self):
        return flashBlueOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        ledStrip.turnOff()
        return ledOn()   

class flashBlueOn(MenuState):
    def __init__(self):
        lcd.put_line(0, "LED ON Menu     ")
        lcd.put_line(1, "<  FLASH BLUE  >")
    def performAction(self):
        ledStrip.flashBlue()
        return self
    def previousState(self):
        return flashGreenOn()
    def nextState(self):
        return flashBlueOn()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        ledStrip.turnOff()
        return ledOn()                    

class AutomaticControl(MenuState):
    def __init__(self):
        lcd.put_line(0, "Main Menu       ")
        lcd.put_line(1, "<  Automatic   >")
    def performAction(self):
        return AutomaticPrompt()
    def previousState(self):
        return ManualControl()
    def nextState(self):
        return Exit()
    def selectState(self):
        return self.performAction()

class AutomaticPrompt(MenuState):
    def __init__(self):
        lcd.put_line(0, "Enable Auto?    ")
        lcd.put_line(1, "Press SEL if yes")
    def performAction(self):
        return self.automaticTrigger()
    def previousState(self):
        return AutomaticControl()
    def nextState(self):
        return AutomaticPrompt()
    def selectState(self):
        return self.performAction()
    def backSubMenu(self):
        return Start()

class Exit(MenuState):
    def __init__(self):
        lcd.put_line(0, "Main Menu       ")
        lcd.put_line(1, "<  Exit         ")
    def previousState(self):
        return AutomaticControl()
    def nextState(self):
        return Exit()
    def selectState(self):
        return self.StopMenu()

class MenuStateMachine(object):
    def __init__(self):
        self.state = Start()
    def handleMenu(self):
        self.state = self.state.handleMenu()
    def StopMenu(self):
        self.state = self.state.StopMenu()

