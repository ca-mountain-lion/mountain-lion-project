# Simple Python script to test CameraClass.py

from CameraClass import CameraModule

Camera = CameraModule(15, 5) # Record manually for 15 seconds; wait for 5s when automatic

def main():
    Camera.manualStillCapture()
    print("Done! #1")
    Camera.manualVideoRecord(10)
    print("Done! #2")

if __name__ == "__main__":
    main()

