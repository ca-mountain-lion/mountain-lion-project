'''
    Author: Aaron Nanas
    ECE 492: Senior Design
    Faculty: Dr. Shahnam Mirzaei
    Project: Predator Deterrent Device

    File: MotionSensorClass.py
    Purpose: The Motion Sensor class for the LCD Menu.
    Allows messages to print out on the LCD when motion
    is triggered or when it has stopped detecting any motion.
'''

from gpiozero import MotionSensor
import time
import I2CLCD
import pigpio

pi = pigpio.pi('192.168.1.3')
if not pi.connected:
    exit(0)

class MotionSensorModule:
    def __init__(self, motionSensorPin):
        self.motionSensor = MotionSensor(motionSensorPin)
        self.lcd = I2CLCD.lcd(pi, width=16)    

    def motionSensorTest(self):
        print("Waiting for motion sensor")
        self.motionSensor.wait_for_no_motion()
        print("Sensor is ready")
        self.lcd.put_line(0, "Sensor is ready!")
        self.lcd.put_line(1, "Waiting ...     ")
        self.motionSensor.wait_for_motion()
        print("Motion Detected!")
        self.lcd.put_line(0, "Motion detected!")
        time.sleep(1)
        self.motionSensor.wait_for_no_motion()
        print("No motion detected")
        self.lcd.put_line(0, "No active motion")
        self.lcd.put_line(1, "Motion test done")
        time.sleep(3)
        self.lcd.put_line(0, "Press the BACK  ")
        self.lcd.put_line(1, "button for menu ")