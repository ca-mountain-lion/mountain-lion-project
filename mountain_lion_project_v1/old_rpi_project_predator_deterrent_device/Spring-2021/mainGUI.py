'''
    Author: Aaron Nanas
    ECE 492: Senior Design
    Faculty: Dr. Shahnam Mirzaei
    Project: Predator Deterrent Device
    Semester: Spring 2021

    File: mainGUI.py
    Purpose: This contains the first prototype of the
    Predator Deterrent Device. The device's main control
    is a GUI, which can set it to be automatically triggered
    or manually controlled.

    To run, enter the following command:
    sudo python3 mainGUI.py
'''
import PySimpleGUI as sg
import RPi.GPIO as GPIO
from RpiMotorLib import RpiMotorLib
import time
from time import sleep
from picamera import PiCamera
from gpiozero import MotionSensor

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# Variables for automatic or manual control
isManualActivated = False
isAutomaticActivated = False

# GPIO Pin Locations
BUZZER_PIN = 12
MOTOR_DIR_PIN = 22 # Motor Direction (DIR) GPIO Pin
MOTOR_STEP_PIN = 23 # Motor Step GPIO Pin
MOTOR_ENABLE = 24 # Motor Enable Pin (Set LOW to enable)
LED_PIN = 25
LED2_PIN = 27
PIR_SENSOR = MotionSensor(4)

# Global Variables for Buzzer PWM
DUTY_CYCLE = 50
DUTY_CYCLE2 = 25
DUTY_CYCLE3 = 75
DUTY_STOP = 0
PWM_FREQ = 1000 # Set the Buzzer's frequency (in Hz)

# Motor and GPIO Initialization
StepperMotor_Inst = RpiMotorLib.A4988Nema(MOTOR_DIR_PIN, MOTOR_STEP_PIN, (21, 21, 21), "DRV8825")
GPIO.setup(MOTOR_ENABLE, GPIO.OUT)
GPIO.setup(BUZZER_PIN, GPIO.OUT)
GPIO.setup(LED_PIN, GPIO.OUT)
GPIO.setup(LED2_PIN, GPIO.OUT)

# Camera Initialization
camera = PiCamera()

# Motor Control
DIRECTION_ARRAY = [False, True]
GPIO.output(MOTOR_ENABLE, GPIO.LOW) # Setting MOTOR_ENABLE low to activate

# Alarm Functions
def turnOnAlarm():
    print("Alarm Activated!")
    GPIO.output(LED_PIN, GPIO.HIGH)
    GPIO.output(LED2_PIN, GPIO.HIGH)
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE)
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE2) # Change to 25
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE3) # Change to 75
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE) # Change back to 50

def turnOffAlarm():
    print("Alarm Disabled")
    GPIO.output(LED_PIN, GPIO.LOW)
    GPIO.output(LED2_PIN, GPIO.LOW)
    BUZZER_PWM.ChangeDutyCycle(DUTY_STOP)

def turnOnBuzzer():
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE)
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE2)
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE3)
    BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE)
    print("Buzzer ON")

def turnOffBuzzer():
    BUZZER_PWM.ChangeDutyCycle(DUTY_STOP)
    print("Buzzer OFF")

# Alternate LED on and off
def flashLights():
    print("Flashing Lights")
    GPIO.output(LED_PIN, GPIO.LOW)
    GPIO.output(LED2_PIN, GPIO.LOW)
    time.sleep(0.3)
    GPIO.output(LED_PIN, GPIO.HIGH)
    GPIO.output(LED2_PIN, GPIO.HIGH)
    time.sleep(0.3)
    GPIO.output(LED_PIN, GPIO.LOW)
    GPIO.output(LED2_PIN, GPIO.LOW)
    time.sleep(0.3)
    GPIO.output(LED_PIN, GPIO.HIGH)
    GPIO.output(LED2_PIN, GPIO.HIGH)
    time.sleep(0.3)
    GPIO.output(LED_PIN, GPIO.LOW)
    GPIO.output(LED2_PIN, GPIO.LOW)
    time.sleep(0.3)

def ledsOn():
    print("Turning on LEDs")
    GPIO.output(LED_PIN, GPIO.HIGH)
    GPIO.output(LED2_PIN, GPIO.HIGH)

def ledsOff():
    print("Turning off LEDs")
    GPIO.output(LED_PIN, GPIO.LOW)
    GPIO.output(LED2_PIN, GPIO.LOW)

# Motor Control Functions:
# runMotorForward() will run the stepper motor in a clockwise direction
# runMotorBackward() will run the stepper motor in a counter-clockwise direction
def runMotorForward():
    print("Running the motor in clockwise direction")
    for ii in range(5):
        StepperMotor_Inst.motor_go(DIRECTION_ARRAY[0], # Move the motor in clockwise direction
                                   "Full", # Move in "Full" step type
                                   400, # Number of steps
                                   0.0005,
                                   False,
                                   0.05)
        time.sleep(1)

def runMotorBackward():
    print("Running the motor in counter-clockwise direction")
    for ii in range(5):
        StepperMotor_Inst.motor_go(DIRECTION_ARRAY[1], # Move the motor in counter-clockwise direction
                                   "Full", # Move in "Full" step type
                                   400, # Number of steps
                                   0.0005,
                                   False,
                                   0.05)
        time.sleep(1)

def stopMotor():
    print("Stopping the motor")
    StepperMotor_Inst.motor_stop()
    time.sleep(0.5)

# Camera manual control functions
def manualStillCapture():
    global isManualActivated
    global isAutomaticActivated

    isManualActivated = True
    isAutomaticActivated = False

    print("Taking a still photo")
    camera.resolution = (1024, 768)
    camera.start_preview()
    sleep(2) # Warm-up time for camera
    camera.capture('DevicePhoto.jpg')
    sleep(1)
    print("Finished capturing a photo")

def manualVideoRecord():
    global isManualActivated
    global isAutomaticActivated

    isManualActivated = True
    isAutomaticActivated = False

    print("Recording a short video")
    camera.resolution = (640, 480)
    camera.start_recording('Device_ManualFootage.h264')
    camera.wait_recording(15) # Record video for about 15 seconds
    camera.stop_recording()
    sleep(1)
    print("Finished recording a short video")

# Automatic and Manual Trigger Functions
def automaticTrigger():
    print("Automatic Trigger ON")
    camera.start_recording('Device_AutoFootage.h264')
    print("Alarm is active and camera is recording")
    for ii in range(4):
        StepperMotor_Inst.motor_go(DIRECTION_ARRAY[ii%2], # If 0, then clockwise; If 1, then counter-clockwise
                              "Full", # This indicates the step type (Full, 1/2, 1/4, 1/8, 1/16, 1/32)
                              400, # The number of steps
                              0.0005, # The amount of step delay (in seconds)
                              False, # If true, print output
                              0.05) # The amount of initial delay (in seconds)
        turnOnBuzzer()
        flashLights()
    turnOffBuzzer()
    camera.wait_recording(5) # Record for 5 seconds more after the alarm is triggered
    camera.stop_recording() # Stop recording after 5 more seconds have passed
    print("Footage recorded")
    time.sleep(2) # Wait for 2 seconds

def manualTrigger():
    global isManualActivated
    global isAutomaticActivated

    isManualActivated = True
    isAutomaticActivated = False
    print("Manual Trigger ON")

# Function to turn off all active peripherals
def turnOffActiveDevices():
    print("Turning OFF all active peripherals")
    GPIO.output(LED_PIN, GPIO.LOW)
    GPIO.output(LED2_PIN, GPIO.LOW)
    BUZZER_PWM.ChangeDutyCycle(DUTY_STOP)
    StepperMotor_Inst.motor_stop()
    time.sleep(0.5)

# Function to call automaticTrigger() when motion is detected
def automaticTriggerHelper():
    global isManualActivated
    global isAutomaticActivated

    isManualActivated = False
    isAutomaticActivated = True

    print("Waiting for PIR Motion Sensor")
    PIR_SENSOR.wait_for_no_motion()
    print("Sensor is ready")
    PIR_SENSOR.wait_for_motion()
    print("Motion Detected!")
    automaticTrigger()
    time.sleep(2)

# Test for motion
def motionSensorTest():
    print("Waiting for PIR Motion Sensor")
    PIR_SENSOR.wait_for_no_motion()
    print("Sensor is ready")
    PIR_SENSOR.wait_for_motion()
    print("Motion Detected!")
    time.sleep(1)
    PIR_SENSOR.wait_for_no_motion()
    print("No Motion Detected!")

# Initialize the PWM for the Buzzer
BUZZER_PWM = GPIO.PWM(BUZZER_PIN, PWM_FREQ)
BUZZER_PWM.start(0) # Initialize the Buzzer PWM at 0 to turn off

# Variable for font size
fontSize = 10

# Window GUI Setup
layout = [[sg.Text('Device Control Window', size=(30, 1),  justification = 'center', font=("Helvetica", 25))],
          [sg.Text('Mode Selection: Automatic | Manual', font = ("Helvetica", 12))],
          [sg.Button('AUTOMATIC'), sg.Button('MANUAL')],
          [sg.Text('_' * 80)],
          [sg.Text('Manual Control Configuration', font = ("Helvetica", 12))],
          [sg.Text('Click LED ON to turn on LED or LED OFF to turn off LED', font = ("Helvetica", fontSize))],
          [sg.Button('LED ON'), sg.Button('LED OFF')],
          [sg.Text('Click BUZZER ON to activate Buzzer or BUZZER OFF to disable Buzzer', font = ("Helvetica", fontSize))],
          [sg.Button('BUZZER ON'), sg.Button('BUZZER OFF')],
          [sg.Text('Click ALARM ON to activate Alarm circuit or ALARM OFF to disable it', font = ("Helvetica", fontSize))],
          [sg.Button('ALARM ON'), sg.Button('ALARM OFF'), sg.Button('Test Motion Sensor')],
          [sg.Text('Motor Control', font = ("Helvetica", fontSize))],
          [sg.Button('Move Forward'), sg.Button('Move Backward'), sg.Button('Stop Motor')],
          [sg.Text('Camera Control', font = ("Helvetica", fontSize))],
          [sg.Button('Still Capture'), sg.Button('Record A Short Video')],
          [sg.Text('_' * 80)],
          [sg.Button('Exit', button_color = '#8b0000'), sg.Button('Turn OFF All Active Devices'), sg.Button('Mountain Lion')]]

window = sg.Window('Predator Deterrent Device', layout)

def main():
    while True:
        event, values = window.read()
        print(event, values)
        if event == 'AUTOMATIC':
            automaticTriggerHelper()
        if event == 'MANUAL':
            manualTrigger()
            turnOffActiveDevices() # Turn off all peripherals when manual mode is activated
        if event == 'Turn OFF Devices':
            turnOffActiveDevices()
        if event ==  'LED ON':
            ledsOn()
        if event == 'LED OFF':
            ledsOff()
        if event == 'BUZZER ON':
            BUZZER_PWM.ChangeDutyCycle(DUTY_CYCLE) # Buzzer will play a sound
        if event == 'BUZZER OFF':
            BUZZER_PWM.ChangeDutyCycle(DUTY_STOP) # Disable buzzer
        if event == 'ALARM ON':
            turnOnAlarm()
        if event == 'ALARM OFF':
            turnOffAlarm()
        if event == 'Test Motion Sensor':
            motionSensorTest()
        if event == 'Move Forward':
            runMotorForward()
        if event == 'Move Backward':
            runMotorBackward()
        if event == 'Stop Motor':
            stopMotor()
        if event == 'Still Capture':
            manualStillCapture()
        if event == 'Record A Short Video':
            manualVideoRecord()
        if event == sg.WIN_CLOSED or event == 'Exit':
            break

    time.sleep(0.5)
    window.close()

if __name__ == "__main__":
    main()


