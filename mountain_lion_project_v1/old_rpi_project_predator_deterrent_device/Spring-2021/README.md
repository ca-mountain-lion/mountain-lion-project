# Updates
The first version of the Predator Deterrent Device. It is primarily controlled with a GUI (Graphical User Interface), which was created using the PySimpleGUI library. The device can be set to be automatically triggered via the motion sensor or manually controlled.

What the device can do:
* Controls are available using the GUI. Each button press corresponds to a specific function and calls it.
* Can be set to be automatic or manual
* The motor can be moved in either a clockwise or counter-clockwise direction. There is also a button to stop the current movement.
* The Piezo buzzer can play the specified frequency. This is planned to be made configurable by the user.
* Currently, this version has two small LEDs that will later be replaced by larger ones, which would be better suited for an alarm.
* The user can manually take a still photo or capture a video recording for the given duration
* When triggered by motion, the alarm circuit will be activated. The camera will start recording a short footage, the motor will run, the LEDs will start flashing, and the buzzer will continuously play a tone.
* Powered via 5V USB

Notes:
* At the beginning of Spring 2021, I did not know much about Python, and this project has given me an opportunity to learn more about the language

# Video Demo
Click the image below to view the video demo:

[![Video Demo for Predator Deterrent Device](https://github.com/aaron-nanas/MountainLion_Senior_Project/blob/main/Screenshots/PIR_motion_sensor.jpg?raw=true)](https://youtu.be/Lag4H_MNH2s)

# Development Tools
* Board Used: Raspberry Pi 4
* Languages: Python
