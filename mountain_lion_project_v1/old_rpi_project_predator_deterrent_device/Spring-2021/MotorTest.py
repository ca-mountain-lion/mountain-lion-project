# Simple Python script used to test the NEMA-17 Stepper Motor

from MotorClass import StepperMotor
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

# GPIO Pin Definitions
motorDirPin = 22
motorStepPin = 23
motorEnablePin = 24

GPIO.setup(motorEnablePin, GPIO.OUT)
stepperMotor = StepperMotor(motorDirPin, motorStepPin)

def main():
    stepperMotor.runMotorBackward()
    time.sleep(5)
    stepperMotor.runMotorForward()
    time.sleep(5)

if __name__ == "__main__":
    main()


