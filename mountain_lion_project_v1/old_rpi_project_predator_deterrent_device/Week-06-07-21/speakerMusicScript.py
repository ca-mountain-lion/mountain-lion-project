'''
File name: speakerMusicScript.py
Description: This script stores finds .mp3 files
and stores them in an array called "mp3s". It will
then play the mp3 files from the array until
it reaches the end or reaches KeyboardInterrupt.
'''

import sys
import pygame as pg
import os
import time

def play_music(music_file):
    clock = pg.time.Clock()
    try:
        pg.mixer.music.load(music_file)
        print("Music file {} loaded!".format(music_file))
    except pygame.error:
        print("File {} not found! {}".format(music_file, pg.get_error()))
        return

    pg.mixer.music.play()

    # Check if playback has finished
    while pg.mixer.music.get_busy():
        clock.tick(30)

def speakerPlay(user_volume):
    print("Playing at volume: " + str(user_volume)+ "\n")
    pg.mixer.music.set_volume(user_volume)
    mp3s = []
    for file in os.listdir("."):
        if file.endswith(".mp3"):
             mp3s.append(file)

    print(mp3s)

    for x in mp3s:
        try:
            play_music(x)
            time.sleep(.25)
        except KeyboardInterrupt:
            pg.mixer.music.fadeout(1000)
            pg.mixer.music.stop()
            raise SystemExit


def main():
    freq = 44100  # audio CD quality
    bitsize = -16  # unsigned 16 bit
    channels = 2  # 1 is mono, 2 is stereo
    buffer = 2048  # number of samples (experiment to get right sound)
    pg.mixer.init(freq, bitsize, channels, buffer)
    user_volume = 0.2

    speakerPlay(user_volume)

if __name__ == "__main__":
    main()
