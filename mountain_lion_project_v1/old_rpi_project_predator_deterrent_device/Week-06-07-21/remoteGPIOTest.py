# Python file used to test the Raspberry Pi's Remote GPIO feature
from gpiozero import LED
from gpiozero.pins.pigpio import PiGPIOFactory
import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

remote_factory = PiGPIOFactory(host='192.168.1.84')
LED_PIN_2 = LED(27, pin_factory=remote_factory)  # remote pin
LED_PIN_1 = 22
PUSH_PIN = 17
GPIO.setup(PUSH_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # using the internal Pull up resistor
GPIO.setup(LED_PIN_1, GPIO.OUT, initial=GPIO.LOW)

def main():
    try:
        while True:
                   GPIO.output(LED_PIN_1, not GPIO.input(PUSH_PIN))
                   if (not GPIO.input(PUSH_PIN)):
                       LED_PIN_2.on()
                   else:
                       LED_PIN_2.off()
                   sleep(0.2)
    except KeyboardInterrupt:
        LED_PIN_2.off()
        GPIO.output(LED_PIN_1, GPIO.LOW)

if __name__ == "__main__":
    main()