# Updates
Added a 3W, 4-Ohm speaker that uses an I2S amplifier. Replaced the small LED blocks with a NeoPixel LED Strip.

What the device can do:
* Created a script that will find .mp3 files in the current directory, list their titles, and play them sequentially
* Created a Python class for the LED strip. This contains functions that will turn on the lights with the specified color, flash them at an interval, or show a specific pattern such as displaying rainbow colors
* Updated the GUI to include new features. Specifically, buttons to play music on the speaker and to turn on the LED strip were added
* Currently trying to figure out a way to play tones at a certain frequency via I2S and replace the Piezo buzzer entirely
* Also working on controlling two Raspberry Pi devices wirelessly via the Remote GPIO feature. This will be used to create an LCD menu, providing a hardware alternative to control the device rather than the GUI alone
    
# Video Demo
Click the image below to view the video demo:

[![Video Demo for Predator Deterrent Device](https://gitlab.com/ca-mountain-lion/mountain-lion-project/-/blob/main/mountain_lion_project_v1/screenshots/LEDStripPhoto.png)](https://youtu.be/5Y2bAS_y6co)

# Diagram
![Project Diagram](https://gitlab.com/ca-mountain-lion/mountain-lion-project/-/blob/main/mountain_lion_project_v1/screenshots/Project_Diagram_v1.png)

# Development Tools
* Board Used: Raspberry Pi 4
* Text Editor: Visual Studio Code (Remote SSH)
* Languages: Python
