![Baby Mountain Lion](./screenshots/BabyMountainLion.jpg)

# Predator Deterrent Device: Senior Design Project
  * Purpose: To construct a prototype of a deterrent device which can be placed in pathways or at locations utilized by mountain lions for travel, feeding, or where attractants have been placed

# Description
Currently a work-in-progress. This project is expected to be completed by December 2021. This repository will contain all the weekly logs and changes, pictures of the current design, video demos, and the source code. Ultimately, the goal is to move this project over to use ESP-IDF dev tools and to use freeRTOS. OTA programming could also be implemented.

# Design Goals
**Predator Deterrent Device**
* Implemented using an ESP32 SoC microcontroller, the device can be triggered with either a motion sensor or by a timer
* The alarm will have the following features:
    * Emits sound with a volume range of 60 dB - 90 dB. It must be able to play a sound file that is loaded from external memory, or generate a tone according to the frequency value specified by the user. Frequencies outside of normal human hearing can be set if they prove to be capable of dissuading mountain lions
    * Lights with randomized patterns that will be placed along horizontal fences
    * Motors to provide sudden motion which can maximize the deterrent effect
* The user can choose between two modes of control: software (GUI) or hardware (an OLED display menu system)
    * Include a form of communication across several alarm devices to control them simultaneously
* Battery powered with the possibility of solar recharging. When idle, the microcontroller's sleep mode will be enabled to lower power consumption significantly
* Housed in a waterproof enclosure
* Optional goals: Camera and PCB design

**Note**: The project also has a separate sub-device that plays sounds to attract a mountain lion's attention briefly and help with capturing close photos:

**Sound Generator**
* Plays deer or rabbit distress sounds when triggered by motion
* The user can generate tone based on the frequency value set by the user
* Has an OLED display menu system and a rotary encoder used to navigate through the menu
* Battery-powered and housed in a waterproof enclosure

# Table of Contents
1. Predator-Deterrent-Device
   1. This contains all the relevant information regarding the design of the main device
   2. The first iteration of this device utilized a Raspberry Pi; however, this was changed in favor of lower power consumption and the project is currently based on ESP32
2. Sound-Generator-Project
   1. Initially implemented on a Raspberry Pi with a webserver GUI
   2. It now uses an ESP32 in favor of lower power consumption, and it can be powered using AA lithium batteries

# Trello Board
[Click here to view the Trello Board of this project.](https://trello.com/b/0ASiic4X/senior-design-project) This contains short update logs.

# Contributors
**Author:** Aaron Joseph Nanas

**Faculty Advisors:** Dr. Shahnam Mirzaei and Dr. Winston Vickers

# About Me
I am a Computer Engineering student from California State University, Northridge. Expected graduation: December 2021.

Languages that I know: C++, C, Python, Verilog and SystemVerilog, VHDL, MATLAB, ARM Assembly, MIPS
