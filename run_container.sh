#!/bin/sh

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
docker build -t registry.gitlab.com/ca-mountain-lion/mountain-lion-project .
docker push registry.gitlab.com/ca-mountain-lion/mountain-lion-project
