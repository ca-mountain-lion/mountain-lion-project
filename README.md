# CA Mountain Lion Project
## Research Topics of Interest

1. What kind of sensory stimuli do mountain lions react to or avoid?
2. When interacting with the deterrent devices, do mountain lions avoid them by a wide distance?
3. Over time, will mountain lions become acclimated to the deterrent devices and ignore the stimuli?
4. Will these devices also affect other predator species?

# Description
Funded by the UC Davis Wildlife Health Center, the Predator Deterrent Device project started in Spring 2021 with the goal of designing a prototype of a low-cost device that can not only dissuade mountain lions from entering certain pathways or locations, but also be capable of emitting sounds that can attract them towards certain areas with cameras and hold their attention for as long as possible. The device has two modes of operation: Attractant and Deterrent. It aims to improve upon current non-lethal deterrent devices, help reduce animal depredation, and assist with mountain lion research efforts. The work done for the duration of this project includes the development of several hand-made prototype devices and on-field testing to observe and draw conclusions from mountain lion responses.

# Directory Tree
- The `mountain_lion_project_v1` directory contains the project files for the Sound Generator device, which can be found in the path `/mountain_lion_project_v1/sound_generator_device/`. Moreover, this directory also contains the old project files of the old implementation that used a Raspberry Pi.
- The `mountain_lion_project_v2` directory contains the final version of the project files for the Predator Deterrent Device. This serves as the primary directory.
- `Dockerfile` is used to create the Docker container of the project's application and its associated environment and dependencies.

# Project Device Goals
- The device can be triggered with either a motion sensor or by a timer.
- The sound emitted must have a volume range of 60 dB to 90 dB or more. It must be able to play a sound file depending on the mode of operation or generate a tone according to the frequency value specified by the user if they prove to be capable of dissuading mountain lions.
- It must have lights capable of displaying random patterns that can be placed along fences or posts.
- It must have a motor in order to trigger some form of sudden motion.
- The user must be able to control the device with some form of an interface such as a Graphical User Interface (GUI) or a Web User Interface (Web UI).
- The device must be powered using batteries such as AA batteries, with the possibility of upgrading to solar recharging in the future.
- The device must be housed in a waterproof enclosure.

# Device Control Web UI
The ESP32 can serve as an access point without needing to be connected to the internet. It can provide an IP address, and the user can enter the given IP address to a browser in order to access the device's control web UI. Some examples are shown in the following images.

Alarm Control Menu

![Web UI 1](./screenshots/alarm_control_menu.png)

Motor Control Menu:

![Web UI 2](./screenshots/motor_control_menu.png)

LED Strip Pattern Menu:

![Web UI 3](./screenshots/led_strip_menu_1.png)

Sound Control Menu 1:

![Web UI 4](./screenshots/sound_control_menu_1.png)

Sound Control Menu 2:

![Web UI 5](./screenshots/sound_control_menu_2.png)

# Dependencies
The following dependencies are required to install in order to flash the program to the ESP32.

- ESP-IDF and its associated dependencies
- Docker
- PlatformIO
- ESP8266Audio
- Adafruit NeoPixel
- ESPAsyncWebServer
- AsyncTCP

**Note**: To use the container, Docker must be installed first in the user's system. Installation of the dependencies is an optional step when using the project's Docker container, as it already contains the environment and the necessary dependencies.

# Building and Flashing with Docker
The Docker container contains all the project files and the required dependencies.

To get the latest container and to pull it from the Gitlab registry:

```
docker pull registry.gitlab.com/ca-mountain-lion/mountain-lion-project
```

Run the Docker container:
```
docker run --rm -it --device=/dev/ttyUSB0 registry.gitlab.com/ca-mountain-lion/mountain-lion-project
```

Once inside the container, erase, flash the ESP32, and monitor the serial output:
```
pio run --target erase && pio run --target upload && pio device monitor --port=/dev/ttyUSB0 --baud=115200
```

To install a particular library:
```
pio lib install <link/to/library>
```

For example:
```
pio lib install https://github.com/earlephilhower/ESP8266Audio.git
```

# Components

# Development Tools

## Development Board
- FireBeetle ESP32 IoT Microcontroller by DFRobot

## Languages:
- Front-end: HTML, CSS, Javascript
- Back-end: C and C++

## Environment:
- PlatformIO
## DevOps:
- Docker
